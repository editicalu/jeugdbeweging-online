import assert = require("assert");
import "mocha";
import "./ArrayExtend";

describe("Array prototype extension tests", () => {
    describe("Prepend tests", () => {
        describe("#prependEmptyArray", () => {
            it("Should return an array with the prepended element.", () => {
                const emptyArr: string[] = [];
                assert.deepEqual(emptyArr.prepend("test"), ["test"]);
            });
        });

        describe("#prependMultipleItems", () => {
            it("Should return an array with the prepended elements in front of the original element.", () => {
                const emptyArr: string[] = ["orig"];
                assert.deepEqual(emptyArr.prepend("test", "test2", "test3"), ["test", "test2", "test3", "orig"]);
            });
        });
    });

    describe("diff tests", () => {
        describe("#symDiff", () => {
            it("Should return the symmetrical difference between 2 arrays", () => {
                const arr1 = [0, 1, 2, 3];
                const arr2 = [4, 5, 0, 6];
                assert.deepEqual(arr1.diff(arr2), [1, 2, 3, 4, 5, 6]);
            });
        });

        describe("#symDiff", () => {
            it("Should return an empty array as there are no differences in content of the arrays", () => {
                const arr1 = [0, 1, 2];
                const arr2 = [1, 2, 0];
                assert.deepEqual(arr1.diff(arr2), []);
            });
        });
    });

    describe("leftDiff tests", () => {
        describe("#nonSymDiff", () => {
            it("Should return the items which are in arr1, but not in arr2.", () => {
                const arr1 = [0, 1, 2];
                const arr2 = [2, 3, 4];
                assert.deepEqual(arr1.leftDiff(arr2), [0, 1]);
            });
        });

        describe("#nonSymDiff", () => {
            it("Should return an empty array as all items in arr1 are also in arr2", () => {
                const arr1 = [0, 1, 2];
                const arr2 = [0, 1, 2, 3, 4, 5, 6];
                assert.deepEqual(arr1.leftDiff(arr2), []);
            });
        });
    });

    describe("unFlatBy tests", () => {
        describe("#unflatEmptyArray", () => {
            it("Should return a 1D array as there were no items to place in sub-arrays.", () => {
                const emptyArr: string[] = [];
                assert.deepEqual(emptyArr.unflatBy(2), []);
            });
        });

        describe("#unflatMultipleTimes", () => {
            it(`Should return a 3D array with the innermost items being grouped
            by 2, and these tuples (items by 2), grouped by 3 each.`, () => {
                const oneD: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                const twoD: number[][] = oneD.unflatBy(2);
                const threeD: number[][][] = twoD.unflatBy(3);
                assert.deepEqual(threeD, [[[1, 2], [3, 4], [5, 6]], [[7, 8], [9]]]);
            });
        });
    });

    describe("zip tests", () => {
        describe("#zip same-length-arrays", () => {
            it("Should return an array of tuples with the elements of a tuple having the same value.", () => {
                const arr1: number[] = [1, 2, 3];
                const arr2: number[] = [1, 2, 3];

                assert.deepEqual(arr1.zip(arr2), [[1, 1], [2, 2], [3, 3]]);
            });
        });

        describe("#zip diff-length-arrays", () => {
            it(`Should return an array of tuples with the elements of a tuple
            having the same value. However, the last tuple has a singular value
            (no partner).`, () => {
                const arr1: number[] = [1, 2, 3];
                const arr2: number[] = [1, 2];

                assert.deepEqual(arr1.zip(arr2), [[1, 1], [2, 2], [3, null]]);
            });
        });

        describe("#zip differently typed array", () => {
            it(`Should return an array of tuples with the elements of a tuple
            being a digit, once as a number and once as a string.`, () => {
                const arr1: number[] = [1, 2, 3];
                const arr2: string[] = ["1", "2", "3"];

                assert.deepEqual(arr1.zip(arr2), [[1, "1"], [2, "2"], [3, "3"]]);
            });
        });

        describe("#zip differently typed array (falsy)", () => {
            it("Should return an array of tuples with the 2nd item of a tuple being a falsy value.", () => {
                const arr1: number[] = [1, 2, 3];
                const arr2: Array<null | undefined | false> = [null, undefined, false];

                assert.deepEqual(arr1.zip(arr2), [[1, null], [2, undefined], [3, false]]);
            });
        });
    });
});
