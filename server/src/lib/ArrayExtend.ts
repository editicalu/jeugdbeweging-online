// We have to extend the Array type's interface but TSlint
// forces us to prepend interfaces with an "I".
// tslint:disable-next-line
declare interface Array<T> {
    prepend(this: T[], ...newItem: T[]): T[];
    diff(this: T[], other: T[]): T[];
    leftDiff(this: T[], other: T[]): T[];
    unflatBy(this: T[], groupSize: number): T[][];
    zip<Z>(this: T[], other: Z[]): Array<[T, Z]>;
}

// Extend Array with the `prepend()` method.
// The method `Array.unshift()` allows for prepending to an array. However, it
// returns the new length of the array, thus not allowing for chaining.
// This method fixes that.
Object.defineProperty(Array.prototype, "prepend", {
    value<T>(this: T[], ...newItem: T[]): T[] {
        const newArray = this;
        newArray.unshift(...newItem);
        return newArray;
    },
});

// Extend Array with the `diff()` method.
// The method `Array.diff()` allows us to find the items which are not in the
// union of the arrays.
// This is also called the symmetric diff.
Object.defineProperty(Array.prototype, "diff", {
    value<T>(this: T[], other: T[]): T[] {
        return this
      .filter((x) => !other.includes(x))
      .concat(other.filter((x) => !this.includes(x)));
    },
});

// Extend Array with the `leftDiff()` method.
// The method `Array.leftDiff()` allows us to find the items which
// are in the "base" array but not in the "other" array.
// This is **NOT** a symmetric diff!
Object.defineProperty(Array.prototype, "leftDiff", {
    value<T>(this: T[], other: T[]): T[] {
        return this
      .filter((x) => !other.includes(x));
    },
});

// Extend Array with the `unflatBy()` method.
// This is the opposite of `Array.flat()`. It creates, given a 1D Array,
// a 2D array of which each sub-array has a length of maximum `groupSize`.
// e.g.: [1, 2, 3, 4, 5].unflatBy(2) === [[1, 2], [3, 4], [5]]
Object.defineProperty(Array.prototype, "unflatBy", {
    value<T>(this: T[], groupSize: number): T[][] {
        return this.reduce((
      groupedItems: T[][],
      newItem: T,
      index: number,
    ): T[][] => {
            if (index % groupSize === 0) {
                groupedItems.push([]);
            }

            groupedItems[groupedItems.length - 1].push(newItem);
            return groupedItems;
        }, []);
    },
});

// Extend Array with the `zip()` method.
// This merges 2 arrays into 1 array where each element is a tuple
// of the elements at position `n` of the original arrays.
Object.defineProperty(Array.prototype, "zip", {
    value<T, Z>(this: T[], other: Z[]): Array<[T, Z]> {
        return this.map((item, index): [T, Z] => [item, other[index]]);
    },
});
