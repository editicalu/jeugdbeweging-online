import ResponseBill from "../server/responseInterfaces/ResponseBill";

export default class Bill {

    public static fromServerMessage(resp: ResponseBill): Bill {
        return new Bill(
            resp.id,
            resp.description,
            resp.amount,
            resp.creationDate !== undefined ?
                new Date(resp.creationDate) :
                undefined,
        );
    }

    public constructor(
    public id: number|undefined,
    public description: string|undefined,
    public amount: number|undefined,
    public creationDate: Date|undefined,
    ) {
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.creationDate = creationDate;
    }
}
