export enum PersonType {
    Leader,
    Child,
}

export function typeToString(type: PersonType): string {
    if (type === PersonType.Child) {
        return "child";
    } else if (type === PersonType.Leader) {
        return "leader";
    } else {
        return "";
    }
}
