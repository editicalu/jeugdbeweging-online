import ResponseReport from "../server/serverMessages/ResponseReport";

export class ReportMin {
    constructor(
    public id: number = -1,
    public reportName: string = "",
    public writtenById: number = 0,
    public date: Date = new Date(1980, 0, 1),
    public writtenByName: string=  "",
  ) {
        this.id = id;
        this.reportName = reportName;
        this.writtenById = writtenById;
        this.date = date;
        this.writtenByName = writtenByName;
    }
}

export class Report extends ReportMin {

    public static toServerMessage(report: Report): ResponseReport {
        return {
            content: report.content,
            date: report.date.toISOString(),
            id: report.id,
            reportName: report.reportName,
            writtenById: report.writtenById,
            writtenByName: report.writtenByName,
        };
    }

    constructor(
        public id: number = -1,
        public reportName: string = "",
        public writtenById: number = 0,
        public date: Date = new Date(1980, 0, 1),
        public writtenByName: string=  "",
        public content: string = "",
    ) {
        super(id, reportName, writtenById, date, writtenByName);
        this.content = content;
    }
}
