import Grouping from "./Grouping";

export default class Activity extends Grouping {
    public static fromDbRow<T extends Grouping>(
        this: new(
                id: number,
                title: string,
                description: string,
                preRegisterDate: Date | null,
                startDate: Date,
                stopDate: Date,
            ) => T,
        query: {
            id: number,
            title: string,
            description: string | null,
            registration_date: Date | null,
            start_date: Date,
            stop_date: Date,
        }): T {
        return new this(
            query.id,
            query.title,
            query.description || "",
            query.registration_date,
            query.start_date,
            query.stop_date,
        );
    }
    constructor(
        public id: number,
        public title: string,
        public description: string = "",
        public preRegisterDate: Date | null = null,
        public startDate: Date = new Date(),
        public stopDate: Date = new Date(),
    ) {
        super(id, title, description);
        this.preRegisterDate = preRegisterDate;
        this.startDate = startDate;
        this.stopDate = stopDate;
    }
}
