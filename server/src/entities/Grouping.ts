export default class Grouping {
    public static fromDbRow<T extends Grouping>(
        this: new(id: number, title: string, description: string) => T,
        query: { id: number, title: string, description: string | null },
    ): T {
        return new this(query.id, query.title, query.description || "");
    }

    constructor(
        public id: number,
        public title: string,
        public description: string = "",
    ) {
        this.id = id;
        this.title = title;
        this.description = description;
    }
}
