import Address from "./datatypes/Address";
import { PersonType, typeToString } from "./PersonType";

enum Gender {
    Male = "Male",
    Female = "Female",
    Other = "Other",
}

class PersonMin {
    public static fromDbRow(
        row: { id: number, first_name: string, last_name: string },
    ): PersonMin {
        return new PersonMin(row.id, row.first_name, row.last_name);
    }
    protected constructor(
        public id: number | null,
        public firstName: string,
        public lastName: string,
    ) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}

/**
 * A Person is a basic entity in the system. It provides basic details.
 */
abstract class Person extends PersonMin {
    protected constructor(
        id: number | null,
        firstName: string,
        lastName: string,
        private birthplace: string,
        private birthdate: Date,
        private gender: Gender | null = null,
        private phoneNumber: string | null = null,
        private address: Address | null = null,
        private comment: string | null = null,
    ) {
        super(id, firstName, lastName);
        this.birthplace = birthplace;
        this.birthdate = birthdate;
        this.gender = gender;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.comment = comment;
    }

    /**
     * Returns the full name. This can be used for printing the name, but should not be used for
     * data manipulations.
     */
    public getName(): string {
        return this.firstName + " " + this.lastName;
    }

    public getId(): number | null {
        return this.id;
    }

    public getFirstName(): string {
        return this.firstName;
    }

    public setFirstName(firstName: string) {
        this.firstName = firstName;
    }

    public getLastName(): string {
        return this.lastName;
    }

    public setLastName(lastName: string) {
        this.lastName = lastName;
    }

    public getGender(): Gender | null {
        return this.gender;
    }

    public setGender(gender: Gender) {
        this.gender = gender;
    }

    public getBirthplace(): string {
        return this.birthplace;
    }

    public setBirthplace(birthplace: string) {
        this.birthplace = birthplace;
    }

    public getBirthdate(): Date {
        return this.birthdate;
    }

    public setBirthdate(birthdate: Date) {
        this.birthdate = birthdate;
    }

    public getPhoneNumber(): string | null {
        return this.phoneNumber;
    }

    public setPhoneNumber(phoneNumber: string | null) {
        this.phoneNumber = phoneNumber;
    }

    public getAddress(): Address | null {
        return this.address;
    }

    public setAddress(address: Address | null) {
        this.address = address;
    }

    public getComment(): string | null {
        return this.comment;
    }

    public setComment(comment: string | null) {
        this.comment = comment;
    }

    public getBasicInformation(): object {
        return {
            id: this.id,
            type: this.getPersonType().toString(),

            firstName: this.firstName,
            lastName: this.lastName,
        };
    }

    public abstract getExtraInformation(): object;

    public getExtendedInformation(): object {
        return {
            id: this.id,
            type: typeToString(this.getPersonType()),

            firstName: this.getFirstName(),
            lastName: this.getLastName(),

            gender: this.getGender(),

            birthdate: this.getBirthdate(),
            birthplace: this.getBirthplace(),

            address: this.address ? this.address.toObject() : null,
            comment: this.getComment(),
            phoneNumber: this.getPhoneNumber(),

            ...this.getExtraInformation(),
        };
    }

    public abstract getPersonType(): PersonType;
}

export { PersonMin, Person, Gender };
