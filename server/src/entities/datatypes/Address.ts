import assert = require("assert");

class Address {
    private streetAddress: string;
    /**
     * Postal zipcode. In some countries, like The Netherlands, this could include letters.
     * Therefore, it is set to be a string.
     */
    private zipcode: string;
    private city: string;

    constructor(streetAddress: string, zipcode: string, city: string) {
        this.streetAddress = streetAddress;
        this.zipcode = zipcode;
        this.city = city;
    }

    public getStreetAddress(): string {
        return this.streetAddress;
    }

    public setStreetAddress(streetAddress: string) {
        this.streetAddress = streetAddress;
    }

    public getZipcode() {
        return this.zipcode;
    }

    public setZipcode(zipcode: string) {
        this.zipcode = zipcode;
    }

    public getCity() {
        return this.city;
    }

    public setCity(city: string) {
        this.city = city;
    }

    public toObject(): object {
        return {
            city: this.city,
            streetAddress: this.streetAddress,
            zipcode: this.zipcode,
        };
    }
}

export { Address };
export default Address;
