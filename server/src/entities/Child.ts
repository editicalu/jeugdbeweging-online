import Address from "./datatypes/Address";
import { Gender, Person } from "./Person";
import { PersonType } from "./PersonType";

export class Child extends Person {
    public static newChild(
        firstName: string,
        lastName: string,
        birthplace: string,
        birthdate: Date,
        gender: Gender | null = null,
        phoneNumber: string | null = null,
        address: Address | null = null,
        comment: string | null = null,
        registrationPending: boolean = true,
    ): Child {
        return new Child(
            null,
            firstName,
            lastName,
            birthplace,
            birthdate,
            gender,
            phoneNumber,
            address,
            comment,
            registrationPending,
        );
    }

    public static withId(
        id: number,
        firstName: string,
        lastName: string,
        birthplace: string,
        birthdate: Date,
        gender: Gender | null = null,
        phoneNumber: string | null = null,
        address: Address | null = null,
        comment: string | null = null,
        registrationPending: boolean = true,
    ): Child {
        return new Child(
            id,
            firstName,
            lastName,
            birthplace,
            birthdate,
            gender,
            phoneNumber,
            address,
            comment,
            registrationPending,
        );
    }

    private registrationPending: boolean;

    private constructor(
        id: number | null,
        firstName: string,
        lastName: string,
        birthplace: string,
        birthdate: Date,
        gender: Gender | null = null,
        phoneNumber: string | null = null,
        address: Address | null = null,
        comment: string | null = null,
        registrationPending = true) {
        super(id, firstName, lastName, birthplace, birthdate, gender, phoneNumber, address, comment);

        this.registrationPending = registrationPending;
    }

    public confirmRegistration() {
        this.registrationPending = false;
    }

    public getRegistrationPending(): boolean {
        return this.registrationPending;
    }

    public getPersonType(): PersonType {
        return PersonType.Child;
    }

    public getExtraInformation(): object {
        return {
            registrationPending: this.registrationPending,
        };
    }
}
