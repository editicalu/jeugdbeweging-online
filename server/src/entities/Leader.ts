import { Account } from "../authentication/Account";
import { Address } from "./datatypes/Address";
import { Gender, Person } from "./Person";
import { PersonType } from "./PersonType";

class Leader extends Person {

    public static fromData(
        id: number,
        firstName: string,
        lastName: string,
        birthplace: string,
        birthdate: Date,
        gender: Gender | null = null,
        phoneNumber: string | null = null,
        address: Address | null = null,
        comment: string | null = null,
        account: Account): Leader {
        return new Leader(
            id,
            firstName,
            lastName,
            birthplace,
            birthdate,
            gender,
            phoneNumber,
            address,
            comment,
            account,
        );
    }

    public static newLeader(
        firstName: string,
        lastName: string,
        birthplace: string,
        birthdate: Date,
        gender: Gender | null = null,
        phoneNumber: string | null = null,
        address: Address | null = null,
        comment: string | null = null,
        account: Account): Leader {
        return new Leader(
            null,
            firstName,
            lastName,
            birthplace,
            birthdate,
            gender,
            phoneNumber,
            address,
            comment,
            account,
        );
    }

    private account: Account;

    private constructor(
        id: number | null,
        firstName: string,
        lastName: string,
        birthplace: string,
        birthdate: Date,
        gender: Gender | null = null,
        phoneNumber: string | null = null,
        address: Address | null = null,
        comment: string | null = null,
        account: Account) {
        super(id, firstName, lastName, birthplace, birthdate, gender, phoneNumber, address, comment);
        this.account = account;
    }

    public getAccount(): Account {
        return this.account;
    }

    public getPersonType(): PersonType {
        return PersonType.Leader;
    }

    public getExtraInformation(): object {
        return {
            permissions: this.getAccount().getPermissions().getRawNumber(),
            username: this.getAccount().getEmail(),
        };
    }
}

export { Leader };
