interface IPersonDataRequest {
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    placeOfBirth: string;

    phoneNumber: string | null;
    address: [string, string, string] | null;
    comments: string | null;

    groups: string[];
    activities: Array<{
        name: string;
        start: Date;
        end: Date;
    }>;

    bills: Array<{
        name: string;
        date: Date;
        amount: number;
        paid: boolean;
    }>;
}

interface IParentDataRequest {
    email: string;
    children: IPersonDataRequest[];
}

interface ILeaderDataRequest extends IPersonDataRequest {
    email: string;
}

export { IPersonDataRequest, IParentDataRequest, ILeaderDataRequest };
