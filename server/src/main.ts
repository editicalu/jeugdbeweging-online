import { readFileSync } from "fs";
import * as http from "http";
import * as https from "https";

import { app } from "./server";

// Start the server
const PORT = 8000;
if (process.env.VUE_APP_SSL === "true") {
    https
        .createServer({
            cert: readFileSync("/run/secrets/ssl_cert").toString(),
            key: readFileSync("/run/secrets/ssl_key").toString(),
            passphrase: process.env.SSL_PASS,
        },
            app,
        )
        .listen(
            PORT,
            () => console.log(`Web application listening on port ${PORT}!`),
        );
} else {
    http
        .createServer(app)
        .listen(
            PORT,
            () => console.log(
                `Web application listening on port ${PORT}!
WARNING: SSL is not being used.`,
            ),
        );
}
