import Result from "../dataPermissionLayer/Result";
import { Child } from "../entities/Child";
import Address from "../entities/datatypes/Address";
import { Leader } from "../entities/Leader";
import { Gender, Person, PersonMin } from "../entities/Person";
import AccountDao from "./AccountDao";
import Database from "./Database";
import { LeaderDao } from "./LeaderDao";

export default class PersonDao {
    public static async fromIdDatabase(id: number): Promise<Result<Child | Leader>> {
        try {
            const [queryResult, leaderQR, childQR] = await Promise.all([
                Database.oneOrNone(`SELECT * FROM person WHERE id=$1`, [id]),
                Database.oneOrNone(`SELECT * FROM leader WHERE person_id=$1`, [id]),
                Database.oneOrNone(`SELECT * FROM child WHERE person_id=$1`, [id]),
            ]);

            if (queryResult === null || queryResult.length === 0) {
                return Result.error(404, `Not found`);
            } else {
                const person = await PersonDao.fromQuery(
                    queryResult, childQR, leaderQR,
                );
                if (person === null) {
                    return Result.error(500, "Database error");
                }
                return Result.correct(person);
            }
        } catch (err) {
            console.error("Error reading Account from database: " + err);
            return Result.error(500, "Database error");
        }
    }

    /**
     * Returns the person with the given id. Returns null if this Person does not exist.
     * @param id The identifier to look for.
     */
    public static async fromId(id: number): Promise<Child | Leader | null> {
        try {
            const [queryResult, leaderQR, childQR] = await Promise.all([
                Database.oneOrNone(`SELECT * FROM person WHERE id=$1`, [id]),
                Database.oneOrNone(`SELECT * FROM leader WHERE person_id=$1`, [id]),
                Database.oneOrNone(`SELECT * FROM child WHERE person_id=$1`, [id]),
            ]);
            return queryResult ? PersonDao.fromQuery(queryResult, childQR, leaderQR) : null;
        } catch (err) {
            console.error(`Error reading Person from database with id ${id}: ${err}`);
            return null;
        }
    }

    /**
     * Returns all Persons in the database.
     */
    public static async all(): Promise<Array<Child | Leader>> {
        try {
            const result = [];
            const [personResults, leaderResults, childrenResults] =
                await Promise.all([
                    // We sort them, to make sure we can iterate over them
                    Database.any(`SELECT * FROM person ORDER BY id`),
                    Database.any(`SELECT * FROM leader ORDER BY person_id`),
                    Database.any(`SELECT * FROM child ORDER BY person_id`),
                ]);

            // A person can be in either the leader or the child table.
            let leaderCounter = 0;
            let childCounter = 0;
            for (const i of personResults) {
                if (leaderResults[leaderCounter].id === i.id) {
                    const leader = await PersonDao.fromQuery(i, null, leaderResults[leaderCounter]);
                    if (leader) {
                        result.push(leader);
                    }
                    leaderCounter += 1;
                } else if (childrenResults[childCounter].id === i.id) {
                    const child = await PersonDao.fromQuery(i, childrenResults[childCounter], null);
                    if (child) {
                        result.push(child);
                    }
                    childCounter += 1;
                }
            }

            return result;
        } catch (err) {
            console.error("Error reading Account from database: " + err);
        }
        return [];
    }

    /**
     * Returns all MinPersons in the database.
     */
    public static async allMin(): Promise<PersonMin[] | null> {
        try {
            const dbQuery = `
                SELECT person.id,
                    person.first_name,
                    person.last_name
                FROM person
                ORDER BY person.id
            `;
            const queryResult = await Database.any(dbQuery);
            if (queryResult === null) {
                return null;
            }
            for (const i of queryResult) {
                return queryResult.map((person) => PersonMin.fromDbRow(person));
            }
        } catch (err) {
            console.error("Error reading Account from database: " + err);
        }
        return null;
    }

    /**
     * Returns all persons as MinPersons in the database that share a group with given accountId.
     */
    public static async allShareAGroupMin(
        accountId: number,
    ): Promise<PersonMin[] | null> {
        try {
            const dbQuery = `
                SELECT DISTINCT person.id,
                    person.first_name,
                    person.last_name
                FROM person
                    INNER JOIN group_person_relation
                    ON group_person_relation.person_id = person.id
                WHERE group_person_relation.group_id IN
                    (SELECT group_id
                    FROM group_person_relation
                        INNER JOIN leader
                        ON leader.person_id = group_person_relation.person_id
                    WHERE leader.user_account_id = $(accountId))
                ORDER BY person.id
            `;
            const queryResult = await Database.any(
                dbQuery,
                { accountId },
            );
            if (queryResult === null) {
                return null;
            }

            return queryResult.map((person) => PersonMin.fromDbRow(person));
        } catch (err) {
            console.error("Error reading Account from database: " + err);
        }
        return null;
    }

    public static async deletePerson(personId: number): Promise<boolean> {
        try {
            const accountID = await LeaderDao.translatePersonToAccountID(personId);
            const personDeletePromise = Database.none(`DELETE FROM person WHERE id=$(personId)`, { personId });

            // Delete account if this is a leader
            if (accountID !== null) {
                await Database.none(`DELETE FROM user_account WHERE id=$(accountID)`, { accountID });
            }
            await personDeletePromise;
            return true;
        } catch (err) {
            console.error(`Error deleting person: ${err}`);
            return false;
        }
    }

    private static async fromQuery(
        queryResult: {
            id: number, first_name: string, last_name: string, gender: string, birthplace: string,
            address_street: string, address_zip: string, address_place: string,
            birthdate: Date, phone_number: string, comments: string,
        },
        childQR: { person_id: number, registration_pending: boolean } | null,
        leaderQR: { user_account_id: number, person_id: number } | null): Promise<Child | Leader | null> {
        const address: Address | null = PersonDao.addressFromQueryResult(queryResult);
        if (leaderQR !== null) {
            const account = await AccountDao.fromIdDatabase(leaderQR.user_account_id);
            if (!account) { return null; }
            return Leader.fromData(
                queryResult.id,
                queryResult.first_name,
                queryResult.last_name,
                queryResult.birthplace,
                queryResult.birthdate,
                Gender[queryResult.gender as keyof typeof Gender],
                queryResult.phone_number,
                address,
                queryResult.comments,
                account,
            );
        } else if (childQR !== null) {
            return Child.withId(
                queryResult.id,
                queryResult.first_name,
                queryResult.last_name,
                queryResult.birthplace,
                queryResult.birthdate,
                Gender[queryResult.gender as keyof typeof Gender],
                queryResult.phone_number,
                address,
                queryResult.comments,
                childQR.registration_pending,
            );
        } else {
            return null;
        }
    }

    private static addressFromQueryResult(queryResult: {
        address_street: string, address_zip: string, address_place: string,
    }): Address | null {
        return queryResult.address_street && queryResult.address_zip && queryResult.address_place
            ? new Address(
                queryResult.address_street,
                queryResult.address_zip,
                queryResult.address_place)
            : null;
    }

}

export { PersonDao };
