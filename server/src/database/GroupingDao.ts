import Grouping from "../entities/Grouping";
import Database from "./Database";

export default class GroupingDao<T extends Grouping> {
    protected grouping: any;
    protected groupingName: string;

    constructor(
        grouping: new(id: number, title: string, description: string) => T,
    ) {
        this.grouping = grouping;
        this.groupingName = grouping.name.toLowerCase();
    }

    public async fromIdDatabase(gId: number)
        : Promise<T | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                SELECT *
                FROM "${this.groupingName}"
                WHERE "${this.groupingName}".id = $(gId)
            `,
                { gId },
            );
            return queryRes ? this.grouping.fromDbRow(queryRes) as T : null;
        } catch (err) {
            console.error(`Error getting ${this.groupingName} with id ${gId}: ${err}`);
            return null;
        }
    }

    public async insertIntoDatabase(
        gName: string,
        gDescription: string,
    ): Promise<number | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                INSERT INTO "${this.groupingName}" (title, description)
                VALUES ($(gName), $(gDescription))
                RETURNING id
            `,
                { gName, gDescription },
            );
            return queryRes ? queryRes.id : null;
        } catch (err) {
            console.error(`Error inserting ${this.groupingName} in db: ${err}`);
            return null;
        }
    }

    public async updateInDatabase(
        gId: number,
        gName: string,
        gDescription: string,
    ): Promise<boolean | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                UPDATE "${this.groupingName}"
                SET title = $(gName), description = $(gDescription)
                WHERE id = $(gId)
                RETURNING id
            `,
                { gName, gDescription, gId },
            );
            // queryRes.id has type any, this makes the strict comparison fail
            // with a number. Instead of using 'normal' comparison and let JS
            // decide what to cast the data to, we tell JS what to do.
            return Number(queryRes.id) === Number(gId);
        } catch (err) {
            console.error(`Error inserting ${this.groupingName} in db: ${err}`);
            return null;
        }
    }

    public async deleteFromDatabase(gId: number)
        : Promise<boolean | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                DELETE FROM "${this.groupingName}"
                WHERE "${this.groupingName}".id = $(gId)
                RETURNING id
            `,
                { gId },
            );
            return queryRes !== null;
        } catch (err) {
            console.error(`Error deleting ${this.groupingName} with id ${gId}: ${err}`);
            return null;
        }
    }

    public async leaderInGrouping(accId: number, gId: number)
        : Promise<boolean | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                SELECT ${this.groupingName}_person_relation.person_id,
                    ${this.groupingName}_person_relation.${this.groupingName}_id
                FROM ${this.groupingName}_person_relation
                    LEFT JOIN "${this.groupingName}"
                    ON ${this.groupingName}_person_relation.${this.groupingName}_id = "${this.groupingName}".id
                WHERE ${this.groupingName}_person_relation.${this.groupingName}_id = $(gId)
                    AND ${this.groupingName}_person_relation.person_id = (
                        SELECT leader.person_id AS pId
                        FROM user_account
                            INNER JOIN leader
                            ON user_account.id = leader.user_account_id
                        WHERE user_account.id = $(accId)
                    )
            `,
                { gId, accId },
            );
            return queryRes !== null;
        } catch (err) {
            console.error(
                `Error determining whether acc ${accId} is allowed to see
                the data of ${this.groupingName} ${gId}: ${err}`);
            return null;
        }
    }
}
