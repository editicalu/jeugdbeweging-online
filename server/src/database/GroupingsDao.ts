import Grouping from "../entities/Grouping";
import Database from "./Database";

export default class GroupingsDao<T extends Grouping> {
    protected groupingName: string;

    constructor(
        grouping: new (id: number, title: string, description: string) => T,
    ) {
        this.groupingName = grouping.name.toLowerCase();
    }

    public async countAllDatabase()
        : Promise<number | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                SELECT COUNT(*)
                FROM "${this.groupingName}"
            `);
            return queryRes ? queryRes.count : null;
        } catch (err) {
            console.error(`Error counting all ${this.groupingName}s in database: ${err}`);
            return null;
        }
    }

    public async allDatabase(
        limit: number = 0,
        page: number = 0,
    ): Promise<T[] | null> {
        try {
            const baseQuery = `
                SELECT "${this.groupingName}".id, "${this.groupingName}".title
                FROM "${this.groupingName}"
                ORDER BY "${this.groupingName}".id
                OFFSET $(offset)
            `;
            const limitQuery = limit === 0
                ? `LIMIT ALL`
                : `LIMIT $(limit)`;
            const queryRes = await Database.any(
                baseQuery + limitQuery,
                {
                    limit,
                    offset: limit * page,
                },
            );
            return queryRes
                ? queryRes.map((group) => Grouping.fromDbRow(group) as T)
                : null;
        } catch (err) {
            console.error(`Error getting all ${this.groupingName}s from database: ${err}`);
            return null;
        }
    }

    public async countFromPersonIdDatabase(personId: number)
        : Promise<number | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                    SELECT COUNT(*)
                    FROM "${this.groupingName}"
                        INNER JOIN ${this.groupingName}_person_relation
                        ON "${this.groupingName}".id = ${this.groupingName}_person_relation.${this.groupingName}_id
                    WHERE ${this.groupingName}_person_relation.person_id = $(personId)
                `,
                { personId },
            );
            return queryRes ? queryRes.count : null;
        } catch (err) {
            console.error(`Error counting ${this.groupingName}s for person with id ${personId} from database: ${err}`);
            return null;
        }
    }

    public async fromPersonIdDatabase(
        personId: number,
        limit: number = 0,
        page: number = 0,
    ): Promise<T[] | null> {
        try {
            const baseQuery = `
                SELECT "${this.groupingName}".id, "${this.groupingName}".title
                FROM "${this.groupingName}"
                    INNER JOIN ${this.groupingName}_person_relation
                    ON "${this.groupingName}".id = ${this.groupingName}_person_relation.${this.groupingName}_id
                WHERE ${this.groupingName}_person_relation.person_id = $(personId)
                ORDER BY "${this.groupingName}".id
                OFFSET $(offset)
            `;
            const limitQuery = limit === 0
                ? `LIMIT ALL`
                : `LIMIT $(limit)`;
            const queryRes = await Database.any(
                baseQuery + limitQuery,
                {
                    limit,
                    offset: limit * page,
                    personId,
                },
            );
            return queryRes
                ? queryRes.map((group) => Grouping.fromDbRow(group) as T)
                : null;
        } catch (err) {
            console.error(`Error getting ${this.groupingName}s for a person with id ${personId} from database: ${err}`);
            return null;
        }
    }

    // This function returns the groups which are shared by the 2 acc & person.
    // This means that if we ask to check a user and the person coupled to this
    // user for shared groupings, but the person is not in a grouping, it will
    // return an empty array. This is because when you are not in a group,
    // you cannot share it with yourself.
    // However, it will return an array of group ids if the account's person
    // is in a group, as it shares it with itself.
    public async getSharedGroupings(personId: number, accId: number)
        : Promise<number[] | null> {
        try {
            // Get groupings of which either person or acc's person are part of.
            const queryRes = await Database.any(`
            SELECT groupings_of_account.${this.groupingName}_id
            FROM
            (
                SELECT ${this.groupingName}_person_relation.${this.groupingName}_id
                FROM ${this.groupingName}_person_relation
                WHERE ${this.groupingName}_person_relation.person_id = $(personId)
            ) AS groupings_of_person
            INNER JOIN
            (
                SELECT ${this.groupingName}_person_relation.${this.groupingName}_id
                FROM ${this.groupingName}_person_relation
                WHERE ${this.groupingName}_person_relation.person_id =
                    (
                        SELECT leader.person_id AS pId
                        FROM user_account
                            INNER JOIN leader
                            ON user_account.id = leader.user_account_id
                        WHERE user_account.id = $(accId)
                    )
            ) AS groupings_of_account
            ON groupings_of_person.${this.groupingName}_id = groupings_of_account.${this.groupingName}_id
        `,
                { personId, accId },
            );
            return queryRes.map((row) => row.group_id);
        } catch (err) {
            console.error(
                `Error determining whether acc ${accId} is allowed to see all
                 ${this.groupingName}s of ${personId}: ${err} `);
            return null;
        }
    }
}
