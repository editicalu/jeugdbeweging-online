import assert = require("assert");
import { Child } from "../../entities/Child";
import { ChildDao } from "../ChildDao";
import GroupDao from "./GroupDao";
import GroupMemberDao from "./GroupMemberDao";

describe("Database", () => {
    describe("#insertIntoDatabaseThenDeleteAgain", () => {
        it(`tries to add a member to a group, and then remove it again.`,
            async () => {
                try {
                    const personId: number | null = await ChildDao.insertIntoDatabase(
                        Child.newChild("Jan", "Janssen", "qmlkfj", new Date("1998/08/18")),
                    );
                    const groupId: number | null = await new GroupDao().insertIntoDatabase(
                        "testGroupName", "description of testGroupName",
                    );

                    if (groupId !== null && personId !== null) {
                        const GMemberDao = new GroupMemberDao();

                        let memberId = await GMemberDao.addMember(
                            groupId, personId,
                        );

                        assert.strictEqual("boolean", typeof memberId);

                        memberId = await GMemberDao.deleteMember(
                            groupId, personId,
                        );

                        assert.strictEqual("boolean", typeof memberId);
                    } else {
                        return assert.fail("inserting person or group failed");
                    }

                } catch (err) {
                    return assert.fail(err);
                }
            });
    });
});
