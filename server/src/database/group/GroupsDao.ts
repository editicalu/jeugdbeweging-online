import Group from "../../entities/Group";
import GroupingsDao from "../GroupingsDao";

export default class GroupsDao extends GroupingsDao<Group> {
    constructor() {
        super(Group);
    }
}
