import assert = require("assert");
import Database from "../Database";
import GroupDao from "./GroupDao";

describe("GroupDao", () => {
    const groupName = "name";
    const groupDescr = "descr";
    const adminId = 1;

    async function cleanPreviousGroups() {
        await Database.none(`DELETE FROM "group"`);
    }

    describe("#insertIntoDatabase", () => {
        it(`tries to insert a group in the database, returning its id.`,
            async () => {
                try {
                    await cleanPreviousGroups();

                    const groupId = await new GroupDao().insertIntoDatabase(
                        groupName, groupDescr,
                    );

                    assert.strictEqual("number", typeof groupId);
                } catch (err) {
                    return assert.fail(err);
                }
            });
    });

    describe("#fromIdDatabase", () => {
        it(`tries to insert a group in the database, then get it again
         with its id.`,
            async () => {
                try {
                    await cleanPreviousGroups();
                    const GDao = new GroupDao();

                    const groupId = await GDao.insertIntoDatabase(
                        groupName, groupDescr,
                    );
                    const group = await GDao.fromIdDatabase(groupId || 0);

                    assert.strictEqual(groupName, group!.title);
                    assert.strictEqual(groupDescr, group!.description);
                } catch (err) {
                    return assert.fail(err);
                }
            });
    });

    describe("#deleteFromDatabase", () => {
        it(`inserts a group in the database, then deletes it again.`,
            async () => {
                try {
                    await cleanPreviousGroups();
                    const GDao = new GroupDao();

                    const groupId = await GDao.insertIntoDatabase(
                        groupName, groupDescr,
                    );
                    const groupDeleted =
                        await GDao.deleteFromDatabase(groupId || 0);

                    assert(groupDeleted);
                } catch (err) {
                    return assert.fail(err);
                }
            });
    });

    describe("#leaderInGrouping", () => {
        it(`inserts a group in the database, then adds person with id = 1
         (which should already be bound to the admin, who has account id 1)
         , to it, and queries whether the admin is allowed to see this
         group.`,
            async () => {
                try {
                    await cleanPreviousGroups();
                    const GDao = new GroupDao();

                    const groupId = await GDao.insertIntoDatabase(
                        groupName, groupDescr,
                    );
                    await Database.none(`
                        INSERT INTO group_person_relation (person_id, group_id)
                        VALUES (1, $(groupId))
                    `, { groupId });

                    assert(
                        await GDao.leaderInGrouping(adminId, groupId || 0),
                    );
                } catch (err) {
                    return assert.fail(err);
                }
            });
    });
});
