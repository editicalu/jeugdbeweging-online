import assert = require("assert");
import { Child } from "../../entities/Child";
import Address from "../../entities/datatypes/Address";
import { Gender } from "../../entities/Person";
import ChildDao from "../ChildDao";
import Database from "../Database";
import GroupDao from "./GroupDao";
import GroupsDao from "./GroupsDao";

describe("Database", () => {
    const gName = "name";
    const gDesc = "desc";
    // from previous tests. Other people were lazy to clean up after
    // themselves. Not nice...
    const adminPersonId = 1;

    async function cleanPreviousGroups() {
        await Database.none(`DELETE FROM "group"`);
    }

    describe("#countAllDatabase", () => {
        it(`deletes all groups from the database, adds 2, counts them, and
         expects 2 groups to be listed.`,
            async () => {
                try {
                    await cleanPreviousGroups();
                    const GDao = new GroupDao();

                    const gName1 = "name1";
                    const gDesc1 = "desc1";
                    const gName2 = "name2";
                    const gDesc2 = "desc2";
                    await Promise.all([
                        GDao.insertIntoDatabase(gName1, gDesc1),
                        GDao.insertIntoDatabase(gName2, gDesc2),
                    ]);

                    const counted = await new GroupsDao().countAllDatabase();
                    assert.equal(2, counted);
                } catch (err) {
                    return assert.fail(err);
                }
            });
    });

    describe("#allDatabase, using pagination", () => {
        it(`deletes all groups, adds 2, and gets max 1 group back from the
         database.`,
            async () => {
                try {
                    await cleanPreviousGroups();
                    const GDao = new GroupDao();

                    const gName1 = "name1";
                    const gDesc1 = "desc1";
                    const gName2 = "name2";
                    const gDesc2 = "desc2";
                    // Make sure group1 is inserted first such that its id
                    // is lower, and thus it will be the selected group
                    // with our pagination.
                    await GDao.insertIntoDatabase(gName1, gDesc1);
                    await GDao.insertIntoDatabase(gName2, gDesc2);

                    const groups = await new GroupsDao().allDatabase(1);
                    if (groups === null) {
                        throw new Error("groups is null");
                    }
                    // We don't care about the id as these are serialised.
                    const nameGroups = groups.map((group) => group.title);
                    assert.deepStrictEqual([gName1], nameGroups);
                } catch (err) {
                    return assert.fail(err);
                }
            });
    });

    describe("group adding and querying for a person", () => {
        describe("#countFromPersonIdDatabase", () => {
            it(`deletes all old groups, adds a new group and adds a person
            with id = 1 to the group. It then counts the groups which person
            with id = 1 is in which must be 1.`,
                async () => {
                    try {
                        await cleanPreviousGroups();

                        const groupId =
                            await new GroupDao().insertIntoDatabase(gName, gDesc);
                        await Database.none(`
                            INSERT INTO
                                group_person_relation (person_id, group_id)
                            VALUES ($(adminPersonId), $(groupId))
                        `, { adminPersonId, groupId },
                        );

                        const amtGroupsForPerson = await new GroupsDao().countFromPersonIdDatabase(adminPersonId);

                        assert.equal(1, amtGroupsForPerson);
                    } catch (err) {
                        return assert.fail(err);
                    }
                });
        });

        describe("#fromPersonIdDatabase", () => {
            it(`creates a group and adds person with id = 1 to it. Then tries
             to get all groups of person1, thus returning the created group.`,
                async () => {
                    try {
                        await cleanPreviousGroups();

                        const groupId =
                            await new GroupDao().insertIntoDatabase(gName, gDesc);
                        await Database.none(`
                            INSERT INTO
                                group_person_relation (person_id, group_id)
                            VALUES ($(adminPersonId), $(groupId))
                        `, { adminPersonId, groupId },
                        );
                        const groups =
                            await new GroupsDao().fromPersonIdDatabase(adminPersonId);
                        if (groups === null) {
                            throw new Error("groups is null");
                        }
                        // we want to see whether the created group is in the
                        // list of groups for the person.
                        const groupIds: number[] =
                            groups.map((group) => group.id);

                        assert(groupIds.includes(groupId || 0));
                    } catch (err) {
                        return assert.fail(err);
                    }
                });
        });
    });

    describe("check whether account is allowed to see groups of a person", () => {
        const adminAccId = 1;
        const TEST_FIRSTNAME = "Hello";
        const TEST_LASTNAME = "World!";
        const TEST_GENDER = Gender["Other" as keyof typeof Gender];
        const TEST_BIRTHDATE = new Date("0001-01-01");
        const TEST_PHONE_NUMBER = "1";
        const TEST_BIRTHPLACE = "rainbow";
        const TEST_ADDRESS = new Address("somewhere 5", "3500", "Hasselt");
        const TEST_COMMENTS = "test";
        const person: Child = Child.newChild(
            TEST_FIRSTNAME,
            TEST_LASTNAME,
            TEST_BIRTHPLACE,
            TEST_BIRTHDATE,
            TEST_GENDER,
            TEST_PHONE_NUMBER,
            TEST_ADDRESS,
            TEST_COMMENTS,
        );

        describe("account wants to see its own groups", () => {
            it(`checks whether the admin is allowed to see the groups for
             person with id = 1, which based on past tests, should be the admin
             itself.`,
                async () => {
                    await cleanPreviousGroups();

                    const groupId =
                        await new GroupDao().insertIntoDatabase(gName, gDesc);
                    await Database.none(`
                            INSERT INTO
                                group_person_relation (person_id, group_id)
                            VALUES ($(adminPersonId), $(groupId))
                        `, { adminPersonId, groupId },
                    );

                    assert(
                        (await new GroupsDao()
                            .getSharedGroupings(adminPersonId, adminAccId))!.length > 0,
                    );
                });
        });

        describe("account wants to see other person's groups (allowed)", () => {
            it(`checks whether the account with id = 1 is allowed to see the
             groups for a newly created person. This person is added to
             a group together with the account.`,
                async () => {
                    await cleanPreviousGroups();

                    const [personId, groupId] = await Promise.all([
                        ChildDao.insertIntoDatabase(person),
                        new GroupDao().insertIntoDatabase(
                            gName, gDesc,
                        ),
                    ]);

                    await Promise.all([
                        Database.none(`
                            INSERT INTO
                                group_person_relation (person_id, group_id)
                            VALUES ($(personId), $(groupId))
                            `, { personId, groupId },
                        ),
                        Database.none(`
                            INSERT INTO
                                group_person_relation (person_id, group_id)
                            VALUES ($(adminPersonId), $(groupId))
                            `, { adminPersonId, groupId },
                        ),
                    ]);

                    assert(
                        (await new GroupsDao().getSharedGroupings(
                            personId || 0, adminAccId,
                        ))!.length > 0,
                    );
                });
        });

        describe("account wants to see other person's groups (not allowed)", () => {
            it(`checks whether the account with id = 1 is allowed to see the
            groups for a newly created person. This person is not added to
            a group together with the account.`,
                async () => {
                    await cleanPreviousGroups();

                    const [personId, groupId] = await Promise.all([
                        ChildDao.insertIntoDatabase(person),
                        new GroupDao().insertIntoDatabase(
                            gName, gDesc,
                        ),
                    ]);

                    // This time only add the new person to the new group.
                    await Database.none(`
                        INSERT INTO
                            group_person_relation (person_id, group_id)
                        VALUES ($(personId), $(groupId))
                        `, { personId, groupId },
                    );

                    assert(
                        !((await new GroupsDao().getSharedGroupings(
                            personId || 0, adminAccId,
                        ))!.length > 0),
                    );
                });
        });
    });
});
