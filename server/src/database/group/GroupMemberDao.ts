import Group from "../../entities/Group";
import GroupingMemberDao from "../GroupingMemberDao";

export default class GroupMemberDao extends GroupingMemberDao<Group> {
    constructor() {
        super(Group);
    }
}
