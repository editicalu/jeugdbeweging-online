import Group from "../../entities/Group";
import GroupingDao from "../GroupingDao";

export default class GroupDao extends GroupingDao<Group> {
    constructor() {
        super(Group);
    }
}
