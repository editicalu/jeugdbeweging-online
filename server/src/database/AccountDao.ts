import Account, { AccountMin } from "../authentication/Account";
import Permissions from "../authentication/Permissions";
import Result from "../dataPermissionLayer/Result";
import Database from "./Database";
import { LeaderDao } from "./LeaderDao";

export default class AccountDao {

    public static async fromEmailDatabase(email: string): Promise<Account | null> {
        try {
            const queryResult = await Database.oneOrNone(
                `SELECT * FROM user_account WHERE email=$1`,
                [email.toLowerCase()],
            );

            if (queryResult !== null) {
                const parentQueryResult =
                    Database.oneOrNone(`SELECT * FROM parent WHERE user_account_id=$1`, [queryResult.id]);
                return AccountDao.fromQueryResult(queryResult, await parentQueryResult);
            } else {
                return null;
            }

        } catch (err) {
            console.error("Error reading Account from database: " + err);
            return null;
        }
    }

    public static async fromIdDatabase(id: number): Promise<Account | null> {
        try {
            const accountQueryResult = Database.oneOrNone(`SELECT * FROM user_account WHERE id=$1`, [id]);
            const parentQueryResult = Database.oneOrNone(`SELECT * FROM parent WHERE user_account_id=$1`, [id]);

            return AccountDao.fromQueryResult(await accountQueryResult, await parentQueryResult);
        } catch (err) {
            console.error("Error reading Account from database: " + err);
            return null;
        }
    }

    /**
     * Saves an Account to the Database. Returns the id on success, null if not.
     * @param acc the Account to be added to the database.
     */
    public static async saveToDatabase(acc: Account): Promise<number | null> {
        try {
            let id = acc.getUid();
            if (acc.getUid() === null) {
                const queryResult = await Database.one(
                    "INSERT INTO user_account (email, password_hash, permission)" +
                    "VALUES ($1, $2, $3) RETURNING id;",
                    [acc.getEmail(), acc.getHashedPassword(), acc.getPermissions().toBitString()]);
                id = queryResult.id;
            } else {
                await Database.none(
                    "UPDATE user_account SET email=$1, password_hash=$2, permission=$3 WHERE id=$4;",
                    [acc.getEmail(), acc.getHashedPassword(), acc.getPermissions().toBitString(), acc.getUid()!]);
            }

            if (acc.isParent()) {
                if (await Database.oneOrNone(`SELECT * FROM parent WHERE user_account_id=$1`, [id]) === null) {
                    await Database.none("INSERT INTO parent(user_account_id) VALUES ($1)", [id]);
                }
            } else {
                await Database.none("DELETE FROM parent WHERE user_account_id=$1", [id]);
            }
            return id;
        } catch (e) {
            console.error("Error writing Account to database: " + e);
            return null;
        }
    }

    public static async leaderIdFrom(accId: number): Promise<number | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                SELECT leader.person_id
                FROM user_account
                    INNER JOIN leader
                    ON user_account.id = leader.user_account_id
                WHERE user_account.id = $(accId)
            `,
                { accId },
            );
            return queryRes ? queryRes.person_id : null;
        } catch (err) {
            console.error(`Error getting person id from account id ${accId}: ${err}`);
            return null;
        }
    }

    public static async updatePassword(accId: number, passwordHash: string): Promise<boolean> {
        try {
            const queryRes = await Database.none(`
                UPDATE user_account
                SET password_hash = $(passwordHash)
                WHERE id = $(accId)
            `,
                { passwordHash, accId },
            );
            return true;
        } catch (err) {
            console.error(`Error updating password for user with id ${accId}`);
            return false;
        }
    }

    public static async updateEmail(accId: number, email: string): Promise<boolean> {
        try {
            const queryRes = await Database.none(`
                UPDATE user_account
                SET email = $(email)
                WHERE id = $(accId)
            `,
                { email, accId },
            );
            return true;
        } catch (err) {
            console.error(`Error updating password for user with id ${accId}`);
            return false;
        }
    }

    public static async deleteAccount(accId: number): Promise<boolean> {
        try {
            const personId = await LeaderDao.translatePersonToAccountID(accId);
            const userPromise = Database.none(`DELETE FROM user_account WHERE id=$(accId)`, { accId });

            // Delete person if this is a leader
            if (personId !== null) {
                await Database.none(`DELETE FROM person WHERE id=$(personId)`, { personId });
            }
            await userPromise;
            return true;
        } catch (err) {
            console.error(`Error deleting person: ${err}`);
            return false;
        }
    }

    public static async getAllAccounts(page: number, limit: number): Promise<AccountMin[] | null> {
        try {
            const offset = page * limit;
            const queryResult = await Database.any(
                `SELECT id, email, user_account_id FROM
            account LEFT OUTER JOIN parent ON user_account.id=parent.user_account_id OFFSET $(offset)`,
                { offset },
            );
            return queryResult.map((accountDbResult) =>
                new AccountMin(accountDbResult.id, accountDbResult.email, accountDbResult.user_account_id !== null));
        } catch (err) {
            console.error(`error loading all accounts: ${err}`);
            return null;
        }
    }

    public static async deleteAccountByEmail(email: string): Promise<Result<void>> {
        try {
            await Database.none(`
            DELETE FROM user_account
            WHERE email = $(email)`,
            {email});
            return Result.correct();
        } catch (e) {
            console.error(`Database error removing account with email: ${email}`);
            return Result.error(500, "Database error");
        }
    }

    private static async fromQueryResult(
        resultAccount: { email: string, password_hash: string, id: number, permission: string },
        resultParent: {},
    )
        : Promise<Account | null> {
        return resultAccount ? Account.fromHashedPassword(
            resultAccount.email,
            resultAccount.password_hash,
            resultAccount.id,
            Permissions.fromBitString(resultAccount.permission),
            resultParent !== null,
        ) : null;
    }
}

export { AccountDao };
