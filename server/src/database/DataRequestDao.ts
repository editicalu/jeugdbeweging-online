import { Account } from "../authentication/Account";
import { Child } from "../entities/Child";
import { ILeaderDataRequest, IParentDataRequest, IPersonDataRequest } from "../entities/DataRequest";
import { Leader } from "../entities/Leader";
import { Person } from "../entities/Person";
import AccountDao from "./AccountDao";
import ChildDao from "./ChildDao";
import GroupsDao from "./group/GroupsDao";
import { LeaderDao } from "./LeaderDao";
import ParentDao from "./ParentDao";
import { PersonDao } from "./PersonDao";

export default class DataRequestDao {
    public static async getAccountDataRequest(
        account: Account,
    ): Promise<ILeaderDataRequest | IParentDataRequest | null> {
        if (account.isParent()) {
            // A Parent requested their data
            const children = await ParentDao.getChildren(account.getUid()!);
            const childrenDataRequest = children ? await Promise.all(
                children.map(async (child) => DataRequestDao.personToPersonRequest(child)),
            ) : [];

            return {
                children: childrenDataRequest,
                email: account.getEmail(),
            };
        } else {
            // A Leader requested their data
            const leaderId = await LeaderDao.translateAccountToPersonID(account.getUid()!);
            if (!leaderId) {
                return null;
            }

            const leader = (await PersonDao.fromId(leaderId)) as Leader;

            return {
                email: leader.getAccount().getEmail(),
                ...await this.personToPersonRequest(leader),
            };
        }
    }

    public static async personToPersonRequest(person: Person): Promise<IPersonDataRequest> {
        const address = person.getAddress();
        const addressList: [string, string, string] | null = address
            ? [address.getStreetAddress(), address.getZipcode(), address.getCity()]
            : null;

        // Fetch groups of the person
        const groupsDao = new GroupsDao();
        const groupCount = await groupsDao.countFromPersonIdDatabase(person.getId()!) || 0;

        const groups =
            (await groupsDao.fromPersonIdDatabase(person.getId()!, groupCount))!
                .map((group) => group.title);

        // TODO: bills
        const bills = [] as Array<{
            name: string;
            date: Date;
            amount: number;
            paid: boolean;
        }>;
        // TODO: events
        const activities = [] as Array<{
            name: string;
            start: Date;
            end: Date;
        }>;

        return {
            address: addressList,
            comments: person.getComment(),
            dateOfBirth: person.getBirthdate(),
            firstName: person.getFirstName(),
            groups,
            lastName: person.getLastName(),
            phoneNumber: person.getPhoneNumber(),
            placeOfBirth: person.getBirthplace(),

            activities,
            bills,
        };
    }
}
