import Activity from "../../entities/Activity";
import GroupingMemberDao from "../GroupingMemberDao";

export default class ActivityMemberDao extends GroupingMemberDao<Activity> {
    constructor() {
        super(Activity);
    }
}
