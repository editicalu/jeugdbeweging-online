import Activity from "../../entities/Activity";
import Database from "../Database";
import GroupingDao from "../GroupingDao";

export default class ActivityDao extends GroupingDao<Activity> {
    constructor() {
        super(Activity);
    }

    public async insertActivityIntoDatabase(
        title: string,
        description: string,
        preRegisterDate: Date | null,
        startDate: Date,
        stopDate: Date,
    ): Promise<number | null> {
        try {
            const answer =
                await Database.one(`
                    INSERT INTO activity (
                        title,
                        description,
                        registration_date,
                        start_date,
                        stop_date
                    )
                    VALUES (
                        $(title),
                        $(description),
                        $(preRegisterDate),
                        $(startDate),
                        $(stopDate)
                    )
                    RETURNING id;`,
                    {
                        description,
                        preRegisterDate,
                        startDate,
                        stopDate,
                        title,
                    },
                );
            return answer ? Number(answer.id) : null;
        } catch (e) {
            console.error(e);
            return null;
        }
    }

    public async updateActivityInDatabase(
        id: number,
        title: string,
        description: string,
        preRegisterDate: Date | null,
        startDate: Date,
        stopDate: Date,
    ): Promise<boolean | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                UPDATE activity
                SET title = $(title),
                    description = $(description),
                    registration_date = $(preRegisterDate),
                    start_date = $(startDate),
                    stop_date = $(stopDate)
                WHERE id = $(id)
                RETURNING id
            `,
                {
                    description,
                    id,
                    preRegisterDate,
                    startDate,
                    stopDate,
                    title,
                },
            );
            // queryRes.id has type any, this makes the strict comparison fail
            // with a number. Instead of using 'normal' comparison and let JS
            // decide what to cast the data to, we tell JS what to do.
            return Number(queryRes.id) === Number(id);
        } catch (err) {
            console.error(`Error inserting ${this.groupingName} in db: ${err}`);
            return null;
        }
    }
}
