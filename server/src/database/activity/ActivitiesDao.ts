import Activity from "../../entities/Activity";
import GroupingsDao from "../GroupingsDao";

export default class ActivitiesDao extends GroupingsDao<Activity> {
    constructor() {
        super(Activity);
    }
}
