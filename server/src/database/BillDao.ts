import { stringify } from "querystring";
import { Z_DATA_ERROR } from "zlib";
import Result from "../dataPermissionLayer/Result";
import Bill from "../entities/Bill";
import { Person } from "../entities/Person";
import {Report, ReportMin} from "../entities/Report";
import Database from "./Database";
import PersonDao from "./PersonDao";

export default class BillDao {
    /**
     * Add a new bill to the database
     * @param personIds all id's of the persons for which this bill is
     * @param description the description of this bill
     * @param amount The amount to pay for this bill
     * @returns the id of the bill
     */
    public static async saveNew(personIds: number[], description: string, amount: number): Promise<Result<number>> {
        try {
            // Add row to bill
            const billId: {id: number} = await Database.one(`
                INSERT INTO bill (description, amount, created_on)
                VALUES ($(description), $(amount), $(date))
                RETURNING id`,
                {description, amount, date: new Date()},
            );
            // Add row to bill_person_relation
            const x = await BillDao.addPersons(billId.id, personIds);
            return Result.correct(billId.id);
        } catch (e) {
            console.error(e);
            return Result.error(500, "Database Error");
        }
    }

    /**
     * Add persons to a bill they should pay
     * @param billId the id of the bill
     * @param personIds the id's of the persons you want to add
     * @returns void
     */
    public static async addPersons(billId: number, personIds: number[]): Promise<Result<void>> {
        try {
            for (const personId of personIds) {
                await Database.none(`
                    INSERT INTO bill_person_relation (paid, person_id, bill_id)
                    VALUES (FALSE, $(personId), $(billId))`,
                    {personId, billId},
                );
            }
            return Result.correct();
        }   catch (e) {
            console.error(e);
            return Result.error(500, "Database Error");
        }
    }

    /**
     * Remove persons from a bill
     * @param billId the id of the bill
     * @param personIds a list of id's of persons you want to remove from the bill
     * @returns void
     */
    public static async removePersons(billId: number, personIds: number[]): Promise<Result<void>> {
        try {
            for (const personId of personIds) {
                await Database.none(`
                    DELETE FROM bill_person_relation
                    WHERE person_id = $(personId) AND bill_id = $(billId)`,
                    {personId, billId},
                );
            }
            return Result.correct();
        } catch (e) {
            console.error(e);
            return Result.error(500, "Database Error");
        }
    }

    /**
     * Remove a bill from the database
     * @param billId the id of the bill to remove
     * @returns Promise<void>
     */
    public static async removeBill(billId: number): Promise<Result<void>> {
        try {
            await Database.none(`
            DELETE FROM bill
            WHERE id = $(billId)`,
            {billId});
            return Result.correct();
        } catch (e) {
            console.error(e);
            return Result.error(500, "Database Error");
        }
    }

    /**
     * Get all the bills of a person, you can choose to get the paid
     * or the unpaid ills
     * @param personId the id of the person's bills
     * @param paid true if you want the paid bills, else false for unpaid bills
     * @returns array of bills
     */
    public static async getBillsOfPerson(personId: number, paid: boolean): Promise<Result<Bill[]>> {
        try {
            const dbResponse: Array<{id: number, description: string, amount: number, created_on: Date}> =
                await Database.any(`
                    SELECT id, description, amount, created_on
                    FROM bill INNER JOIN bill_person_relation
                        ON id = bill_id
                    WHERE person_id = $(personId) AND bill_person_relation.paid = $(paid);`,
                    {personId, paid});

            return Result.correct(
                dbResponse.map((bill) => new Bill(bill.id, bill.description, bill.amount, bill.created_on)),
            );
        } catch (e) {
            console.error(e);
            return Result.error(500, "Database Error");
        }
    }

    /**
     * Get a bill from the database
     * @param billId The id of the bill
     * @returns a bill: id, creation date, description and amount of money to pay
     */
    public static async getBill(billId: number): Promise<Result<Bill>> {
        try {
            const respBill: {id: number, description: string, amount: number, created_on: Date} =
                await Database.one(`
                    SELECT *
                    FROM bill
                    WHERE id = $(billId)
                    ORDER BY created_on DESC`,
                    {billId});

            return Result.correct(new Bill(respBill.id, respBill.description, respBill.amount, respBill.created_on));

        } catch (e) {
            console.error(e);
            return Result.error(500, "Server error");
        }
    }

    /**
     * Get a bill from the database
     * @param billId The id of the bill
     * @returns a bill: id, creation date, description and amount of money to pay
     */
    public static async getAllBills(): Promise<Result<Bill[]>> {
        try {
            const dbResponse: Array<{id: number, description: string, amount: number, created_on: Date}> =
                await Database.any(`
                    SELECT *
                    FROM bill
                    ORDER BY created_on DESC`,
                );

            return Result.correct(
                dbResponse.map((bill) => new Bill(bill.id, bill.description, bill.amount, bill.created_on)),
            );

        } catch (e) {
            console.error(e);
            return Result.error(500, "Server error");
        }
    }

    /**
     * Get the id's of all people that are linked to a certain bll
     * @param billId the id of the bill you want to see
     * @returns an array with person-id's
     */
    public static async getBilledIds(billId: number): Promise<Result<number[]>> {
        try {
            const respPersons: Array<{person_id: number}> =
                await Database.any(`
                    SELECT person_id
                    FROM bill_person_relation
                    WHERE bill_id = $(billId)
                    ORDER BY paid ASC`,
                    {billId});

            return Result.correct(respPersons.map((respPerson) => respPerson.person_id));

        } catch (e) {
            console.error(e);
            return Result.error(500, "Database Error");
        }
    }

    /**
     * Get all person-id's that are connected to a bill, selected by the constrain paid
     * @param billId the id of the bill
     * @param paid if true: get all persons that paid, else get all persons that didn't pay
     */
    public static async getBilledIdsByPaid(billId: number, paid: boolean): Promise<Result<number[]>> {
        try {
            const respPersons: Array<{person_id: number}> =
            await Database.any(`
                SELECT person_id
                FROM bill_person_relation
                WHERE bill_id = $(billId) AND paid = $(paid)
                ORDER BY person_id ASC`,
                {billId, paid});

            return Result.correct(respPersons.map((resPerson) => resPerson.person_id));

        } catch (e) {
            console.error(e);
            return Result.error(500, "Database error");
        }
    }

    /**
     * Set the paid-status of a billed person for a certain bill
     * @param billId the id of the bill
     * @param personId the id of the person
     * @param paid the new paid-status
     * @returns Result<void>
     */
    public static async setPaid(billId: number, personId: number, paid: boolean): Promise<Result<void>> {
        try {
            await Database.none(`
                UPDATE bill_person_relation
                SET paid = $(paid)
                WHERE person_id = $(personId) AND bill_id = $(billId)`,
                {paid, personId, billId});
            return Result.correct();
        } catch (e) {
            console.error(e);
            return Result.error(500, "Database Error");
        }
    }

    /**
     * Edit a bill in the database
     * @post the bill's id is already an id in the database
     * @param bill containing all the data of the new bill
     * @returns Result<void>
     */
    public static async edit(bill: Bill): Promise<Result<void>> {
        try {
            await Database.none(`
                UPDATE bill
                SET description = $(description), amount = $(amount)
                WHERE id = $(billId)`,
                {description: bill.description, amount: bill.amount, billId: bill.id});
            return Result.correct();
        } catch (e) {
            return Result.error(500, "Database error");
        }
    }
}
