import { IDatabase, IMain } from "pg-promise";
import * as pgPromise from "pg-promise";

const connectionFactory: IMain = pgPromise({
    // Initialization Options
    error(error: any, errorContext: pgPromise.IEventContext) {
        if (errorContext.cn) {
            console.error("Database connection error occurred:");
            console.error(error);
            console.error(errorContext);
        }
    },
});

const url: string =
    `postgres://${
    process.env.DB_USER
    }:${
    process.env.DB_PASS
    }@${
    process.env.NODE_ENV === "production"
        ? "database"
        : process.env.DB_IP
    }:${
    process.env.DB_PORT
    }/${
    process.env.DB_NAME
    }`;

const Database: IDatabase<any> = connectionFactory(url);

export default Database;
