import { Z_DATA_ERROR } from "zlib";
import Result from "../dataPermissionLayer/Result";
import { Person } from "../entities/Person";
import { Report, ReportMin } from "../entities/Report";
import Database from "./Database";
import PersonDao from "./PersonDao";

export default class ReportDao {
    public static async saveNew(reportName: string, date: Date, content: string, writtenById: number):
        Promise<number | null> {
        try {
            const answer =
                await Database.one(
                    `INSERT INTO report (title, date, content, written_by_id)
                VALUES ($(reportName), $(date), $(content), $(writtenById))
                RETURNING id;`,
                    { reportName, date, content, writtenById },
                );
            if (answer === null) {
                return null;
            } else {
                return Number(answer.id);
            }
        } catch (e) {
            console.error(e);
            return null;
        }
    }

    public static async override(id: number, reportName: string, date: Date, content: string, writtenById: number):
        Promise<boolean> {
        try {
            // Try to remove old content
            if (!ReportDao.remove(id)) {
                return false;
            }
            await Database.none(
                `INSERT INTO report (id, title, date, content, written_by_id)
                VALUES ($(id), $(reportName), $(date), $(content), $(writtenById));`,
                { id, reportName, date, content, writtenById },
            );
            return true;

        } catch (e) {
            console.error(e);
            return false;
        }
    }

    public static async remove(id: number): Promise<boolean> {
        try {
            Database.none(`DELETE FROM report WHERE id=$(id)`, { id });
            return true;
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    public static async getAllReportsMin(): Promise<ReportMin[]> {
        try {
            const result: ReportMin[] = [];
            const answer =
                await Database.any(`
                    SELECT id, title, date, written_by_id
                    FROM report;`);
            for (const i of answer) {
                result.push(new ReportMin(i.id, i.title, i.written_by_id, i.date));
            }
            return result;
        } catch (e) {
            console.error(e);
            throw e;
        }
    }

    public static async getFullReport(id: number): Promise<Result<Report>> {
        try {
            const answer =
                await Database.oneOrNone(`
                    SELECT *
                    FROM report
                    WHERE id = $(reportId)`,
                    { reportId: id });

            if (answer === null) {
                throw Error("No such report");
            }

            // Try to find the creator his name
            const secondAnswer: Result<Person> =
                await PersonDao.fromIdDatabase(answer.written_by_id);
            if (secondAnswer.isError()) {
                return Result.errorByStatus(secondAnswer.getStatus());
            }

            return Result.correct(new Report(
                answer.id,
                answer.title,
                answer.written_by_id,
                answer.date,
                secondAnswer.getContent()!.getName(),
                answer.content,
            ));

        } catch (e) {
            console.error(e);
            throw (e);
        }
    }
}
