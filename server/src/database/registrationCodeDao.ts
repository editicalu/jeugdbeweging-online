import Database from "./Database";

export default class RegistrationCodeDao {
    /**
     * Insert a registration-code of a child into
     * The database
     * @param personID the person-id of the child
     * @param registrationCode The registration code belonging to the given child.
     * @returns true if correctly added, else false
     */
    public static async insertRegistrationCodeIntoDatabase(
        personID: number,
        registrationCode: string,
    ): Promise<boolean> {
        try {
            await Database.none(`INSERT INTO registration_codes (personid, registration_code)
                VALUES ($(personId), $(registrationCode))`, {
                    personId: personID,
                    registrationCode,
                });
            return true;
        } catch (e) {
            console.error(`Could not add registration code: ${e}`);
            return false;
        }
    }

    /**
     * Get the id of the child you can register with a given registration code
     * @param regCode the registration code
     * @returns The id of the child belonging to the registration-code, or null if registration-code isn't valid
     */
    public static async ChildIdFromRegistrationCode(regCode: string): Promise<number | null> {
        try {
            // Check if registrationCode is in the database
            const regInDatabase =
                await Database.oneOrNone(`SELECT personid FROM registration_codes WHERE registration_code = $(regCode)`,
                    { regCode });
            // Check if found
            if (regInDatabase === null) {
                return null;
            }
            return regInDatabase.personid;
        } catch (e) {
            console.error(e);
            return null;
        }
    }

    /**
     * Remove the given registration code from the database
     * @param regCode The registration code
     * @returns true if everything went fine
     */
    public static async removeRegistrationCode(regCode: string): Promise<boolean> {
        try {
            await Database.none(`DELETE FROM registration_codes WHERE registration_code=$(regCode)`, { regCode });
            return true;
        } catch (e) {
            console.error(e);
            return false;
        }
    }
}
