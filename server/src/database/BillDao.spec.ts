import assert = require("assert");

import Result from "../dataPermissionLayer/Result";
import Bill from "../entities/Bill";
import BillDao from "./BillDao";

describe("BillDao", () => {
    describe("#saveNewBill", () => {
        it(`Tries to save a new bill into the database`,
            async () => {
                const billIdResult: Result<number> = await BillDao.saveNew([1], "Name of Bill", 10);
                assert(billIdResult.isCorrect());
                const billId: number = billIdResult.getContent()!;
                assert(billId > 0);
                await BillDao.removeBill(billId);
            });
    });
    describe("#editBill", () => {
        it(`Add a new bill and then try to edit it`,
        async () => {
            // params
            const newDescription = "New description";
            const newAmount = 15;
            // test
            const billIdResult: Result<number> = await BillDao.saveNew([1], "Name of Bill", 10);
            assert(billIdResult.isCorrect());
            const billId: number = billIdResult.getContent()!;
            const editBillResult: Result<void> =
                await BillDao.edit(new Bill(billId, newDescription, newAmount, new Date()));
            assert(editBillResult.isCorrect());
            const deleteBillResult: Result<void> = await BillDao.removeBill(billId);
            assert(deleteBillResult.isCorrect());
        });
    });
    describe("#editUsersOfBill", () => {
        it(`Àdd users and remove users from a bill`,
        async () => {
            const billIdResult: Result<number> = await BillDao.saveNew([], "Name of Bill", 10);
            assert(billIdResult.isCorrect());
            const billId: number = billIdResult.getContent()!;
            assert((await BillDao.addPersons(billId, [1])).isCorrect());
            assert((await BillDao.removePersons(billId, [1])).isCorrect());
            assert((await BillDao.removeBill(billId)).isCorrect());
        });
    });
    describe("#GetBillsOfPerson", () => {
        it(`Get bills of a certain user`,
        async () => {
            const billIdResult: Result<number> = await BillDao.saveNew([], "Name of Bill", 10);
            assert(billIdResult.isCorrect());
            const billId: number = billIdResult.getContent()!;
            const addPersonsResult: Result<void> = await BillDao.addPersons(billId, [1]);
            assert(addPersonsResult.isCorrect());
            const billsResult: Result<Bill[]> = await BillDao.getBillsOfPerson(1, false);
            assert(billsResult.isCorrect());
            const bills: Bill[] = billsResult.getContent()!;

            // Check if the list of bills contains the just added bill
            let billsContainNewBill: boolean = false;
            bills.forEach((bill) => {
                if (bill.id === billId) {
                    billsContainNewBill = true;
                }
            });
            assert(billsContainNewBill);
            const removeBillResult: Result<void> = await BillDao.removeBill(billId);
            assert(removeBillResult.isCorrect());
        });
    });
    describe("#getBillById", () => {
        it(`Get bill based on it's id`, async () => {
            // params
            const descr: string = "msdlfjkdmlkfje";
            const amount: number = 6841352.67;
            // checkers
            const billIdResult: Result<number> = await BillDao.saveNew([], descr, amount);
            assert(billIdResult.isCorrect());
            const billId: number = billIdResult.getContent()!;
            const getBillResult: Result<Bill> = await BillDao.getBill(billId);
            assert(getBillResult.isCorrect());
            const bill: Bill = getBillResult.getContent()!;
            assert.deepEqual(bill.description, descr);
            assert.deepEqual(bill.amount, amount);
            assert((await BillDao.removeBill(billId)).isCorrect());
        });
    });
});
