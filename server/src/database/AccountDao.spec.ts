import assert = require("assert");
import process_configuration from "../server/configuration";
process_configuration();

import Account from "../authentication/Account";
import { Permission } from "../authentication/Permission";
import Permissions from "../authentication/Permissions";
import Address from "../entities/datatypes/Address";
import { Leader } from "../entities/Leader";
import { Gender } from "../entities/Person";
import AccountDao from "./AccountDao";
import { LeaderDao } from "./LeaderDao";

describe("AccountDao", () => {
    describe("#readExistingUser", () => {
        it("should read an Account from the database", async () => {
            const account: Account|null = await AccountDao.fromEmailDatabase("admin");
            assert(account !== null);
            assert.deepStrictEqual(account!.getEmail(), "admin");
            assert.deepStrictEqual(account!.getUid(), 1);
            assert(account!.getPermissions() !== null);
            const permissions: Permissions = account!.getPermissions()!;
            assert(permissions.hasPermission(Permission.ChildrenViewer));
            assert(permissions.hasPermission(Permission.ChildrenAdministrator));
            assert(permissions.hasPermission(Permission.LeaderAdministrator));
            assert(permissions.hasPermission(Permission.ParentAdministrator));
            assert(permissions.hasPermission(Permission.Financial));
            assert(permissions.hasPermission(Permission.Reporter));
            assert(permissions.hasPermission(Permission.Grouper));
        });
    });

    describe("#readNonExistingUser", () => {
        it("tries to load a non-existing account", (done) => {
            AccountDao.fromEmailDatabase("blablabla")
                .then((account) => { assert(account === null); })
                .then(() => { done(); })
                .catch((err: any) => { assert.fail(err); });
        });
    });

    describe("#saveToDatabase", () => {
        it("should add an Account to the database and then crash", async () => {
            const emailAddress: string = "username@example.com";
            const password: string = "testPass";
            const account: Account|null = Account.fromUnhashedPassword(emailAddress, password, null, false);

            // Remove account if exists
            await AccountDao.deleteAccountByEmail(emailAddress);

            // testing
            const savedToDatabase1: number|null = await AccountDao.saveToDatabase(account);
            assert(savedToDatabase1 !== null);
            const savedToDatabase2: number|null = await AccountDao.saveToDatabase(account);
            assert.deepStrictEqual(savedToDatabase2, null);

            const dbAccount: Account|null = await AccountDao.fromEmailDatabase("username@example.com");
            assert(dbAccount !== null);
            assert(dbAccount!.verifyPassword(password));

            // Clean up data
            assert((await AccountDao.deleteAccountByEmail(emailAddress)).isCorrect());
        });

        it("should add an Account to the database", async () => {
            const emailAddress: string = "test@example.com";
            const account: Account|null = Account.fromUnhashedPassword(emailAddress, "testPass", null, false);

            // Remove account if exists
            await AccountDao.deleteAccountByEmail(emailAddress);

            // do checks
            const id: number|null = await AccountDao.saveToDatabase(account);
            assert(id !== null);

            // clean up data
            assert((await AccountDao.deleteAccountByEmail(emailAddress)).isCorrect());
        });
    });

    describe("#AccountMin", async () => {
        const accountMin = await AccountDao.getAllAccounts(0, 1);
        assert.equal(accountMin, [{
            email: "admin", id: 1, isParent: false,
        }]);
    });
});
