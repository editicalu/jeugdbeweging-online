import Checklist from "../../entities/Checklist";
import GroupingsDao from "../GroupingsDao";

export default class ChecklistsDao extends GroupingsDao<Checklist> {
    constructor() {
        super(Checklist);
    }
}
