import Checklist from "../../entities/Checklist";
import GroupingDao from "../GroupingDao";

export default class ChecklistDao extends GroupingDao<Checklist> {
    constructor() {
        super(Checklist);
    }
}
