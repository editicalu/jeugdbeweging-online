import Database from "../../database/Database";
import Checklist from "../../entities/Checklist";
import GroupingMemberDao from "../GroupingMemberDao";

interface IPersonMinChecked {
    id: number;
    checked: boolean;
    firstName: string;
    lastName: string;
}

export default class ChecklistMemberDao extends GroupingMemberDao<Checklist> {
    constructor() {
        super(Checklist);
    }

    public async checkMember(
        gId: number,
        personId: number,
        checked: boolean = true,
    ): Promise<boolean | null> {
        try {
            const aValidId = await Database.one(`
                UPDATE checklist_person_relation
                SET checked = $(checked)
                WHERE person_id = $(personId)
                RETURNING person_id`,
                { checked, personId },
            );
            return aValidId ? true : false;
        } catch (err) {
            console.error(`Error updating checked state of persons ${personId}
            in checklist ${gId} to ${checked}: ${err}`);
            return null;
        }
    }

    public async allMinCheckedFromGIdDatabase(
        gId: number,
        limit: number = 0,
        page: number = 0,
    ): Promise<IPersonMinChecked[] | null> {
        try {
            const baseQuery = `
                SELECT person.id,
                    checklist_person_relation.checked,
                    person.first_name,
                    person.last_name
                FROM person
                    INNER JOIN checklist_person_relation
                    ON person.id = checklist_person_relation.person_id
                WHERE checklist_person_relation.checklist_id = $(gId)
                ORDER BY person.id
                OFFSET $(offset)
            `;
            const limitQuery = limit === 0
                ? `LIMIT ALL`
                : `LIMIT $(limit)`;
            const queryResult = await Database.any(
                baseQuery + limitQuery,
                { gId, offset: limit * page, limit },
            );
            if (queryResult === null) {
                return null;
            } else {
                return queryResult.map((row) =>
                    this.rowToPersonMinChecked(row));
            }
        } catch (err) {
            console.error(`Error getting all persons from ${this.groupingName} with id ${gId}: ${err}`);
            return null;
        }
    }

    private rowToPersonMinChecked(
        row: {
            id: number,
            checked: boolean,
            first_name: string,
            last_name: string,
        },
    ): IPersonMinChecked {
        return {
            checked: row.checked,
            firstName: row.first_name,
            id: row.id,
            lastName: row.last_name,
        };
    }
}
