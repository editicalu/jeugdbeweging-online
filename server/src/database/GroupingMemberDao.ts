import Grouping from "../entities/Grouping";
import { PersonMin } from "../entities/Person";
import Database from "./Database";

export default class GroupingMemberDao<T extends Grouping> {
    protected groupingName: string;

    constructor(
        grouping: new (id: number, title: string, description: string) => T,
    ) {
        this.groupingName = grouping.name.toLowerCase();
    }

    // returns true when delete successful
    public async deleteMember(gId: number, memberId: number)
        : Promise<boolean | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                DELETE FROM ${this.groupingName}_person_relation
                WHERE ${this.groupingName}_id = $(gId)
                    AND person_id = $(memberId)
                RETURNING person_id;
            `,
                { gId, memberId },
            );
            return queryRes !== null;
        } catch (err) {
            console.error(`Error removing member with id ${memberId}: ${err}`);
            return null;
        }
    }

    // Returns `true` on successful insertion.
    // Returns `false` when member was already inserted.
    // Returns `null` on DB error.
    public async addMember(gId: number, memberId: number)
        : Promise<boolean | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                INSERT INTO ${this.groupingName}_person_relation (person_id, ${this.groupingName}_id)
                VALUES ($(memberId), $(gId))
                ON CONFLICT (person_id, ${this.groupingName}_id)
                DO NOTHING
                RETURNING person_id
            `,
                { memberId, gId },
            );
            return queryRes !== null;
        } catch (err) {
            console.error(`Error inserting member with id ${memberId}: ${err}`);
            return null;
        }
    }

    public async containsMember(gId: number, memberId: number)
        : Promise<boolean | null> {
        try {
            const queryRes = await Database.oneOrNone(`
                SELECT * FROM ${this.groupingName}_person_relation
                WHERE ${this.groupingName}_id = $(gId) AND person_id = $(memberId)
            `,
                { memberId, gId },
            );
            return queryRes !== null;
        } catch (err) {
            console.error(`Error finding id ${memberId}: ${err}`);
            return null;
        }
    }

    public async containsChildOf(gId: number, parentId: number)
        : Promise<PersonMin[] | null> {
        try {
            const queryRes = await Database.manyOrNone(`
                SELECT person.id,
                    person.first_name,
                    person.last_name
                FROM ${this.groupingName}_person_relation
                    INNER JOIN parent_child_relation
                    ON ${this.groupingName}_person_relation.person_id =
                        parent_child_relation.child_id
                        LEFT JOIN person
                        ON person.id = ${this.groupingName}_person_relation.person_id
                WHERE ${this.groupingName}_person_relation.${this.groupingName}_id = $(gId)
                    AND parent_child_relation.parent_id = $(parentId)
            `,
                { gId, parentId },
            );
            return queryRes.map((row) => PersonMin.fromDbRow(row));
        } catch (err) {
            console.error(`Error determining whether account of parent (id =
                ${parentId}) has any children in the group with id = ${gId}.`);
            return null;
        }
    }

    public async countFromGIdDatabase(gId: number)
        : Promise<number | null> {
        try {
            const queryResult = await Database.oneOrNone(`
                SELECT COUNT(*)
                FROM person
                    INNER JOIN ${this.groupingName}_person_relation
                    ON person.id = ${this.groupingName}_person_relation.person_id
                WHERE ${this.groupingName}_person_relation.${this.groupingName}_id = $(gId)
            `,
                { gId },
            );
            if (queryResult === null) {
                return null;
            } else {
                return queryResult.count;
            }
        } catch (err) {
            console.error(`Error counting amount of persons in ${this.groupingName} with id ${gId}: ${err}`);
            return null;
        }
    }

    public async allMinFromGIdDatabase(
        gId: number,
        limit: number = 0,
        page: number = 0,
    ): Promise<PersonMin[] | null> {
        try {
            const baseQuery = `
                SELECT person.id,
                    person.first_name,
                    person.last_name
                FROM person
                    INNER JOIN ${this.groupingName}_person_relation
                    ON person.id = ${this.groupingName}_person_relation.person_id
                WHERE ${this.groupingName}_person_relation.${this.groupingName}_id = $(gId)
                ORDER BY person.id
                OFFSET $(offset)
            `;
            const limitQuery = limit === 0
                ? `LIMIT ALL`
                : `LIMIT $(limit)`;
            const queryResult = await Database.any(
                baseQuery + limitQuery,
                { gId, offset: limit * page, limit },
            );
            if (queryResult === null) {
                return null;
            } else {
                return queryResult.map((person) => PersonMin.fromDbRow(person));
            }
        } catch (err) {
            console.error(`Error getting all persons from ${this.groupingName} with id ${gId}: ${err}`);
            return null;
        }
    }
}
