import { AccountMin } from "../authentication/Account";
import { Child } from "../entities/Child";
import { Gender } from "../entities/Person";
import ChildDao from "./ChildDao";
import Database from "./Database";

export default class ParentDao {
    public static async addChild(parentId: number, childId: number): Promise<boolean> {
        try {
            await Database.none(
                `INSERT INTO parent_child_relation (parent_id, child_id) VALUES ($(parentId), $(childId))`,
                { parentId, childId },
            );
            return true;
        } catch (err) {
            console.error(`Error adding child to parent: ${err}`);
            return false;
        }
    }

    public static async getChildren(parentId: number): Promise<Child[] | null> {
        try {
            const queryRes = await Database.any(
                `SELECT id,
                    first_name,
                    last_name,
                    gender,
                    birthplace,
                    birthdate,
                    phone_number,
                    address_street,
                    address_zip,
                    address_place,
                    comments
                FROM person
                    INNER JOIN parent_child_relation
                    ON person.id = parent_child_relation.child_id
                WHERE parent_child_relation.parent_id = $(parentid)`,
                { parentid: parentId },
            );

            if (queryRes === null) {
                return null;
            } else {
                return queryRes.map((child) => {
                    const address = ChildDao.addressFromQueryResult(child);
                    return Child.withId(
                        child.id,
                        child.first_name,
                        child.last_name,
                        child.birthplace,
                        child.birthdate,
                        Gender[child.gender as keyof typeof Gender],
                        child.phone_number,
                        address,
                        child.comments,
                        child.registration_pending);
                });
            }
        } catch (err) {
            console.error(`Error getting children from parent: ${err}`);
            return null;
        }
    }

    public static async isParentOf(childId: number, accountId: number): Promise<boolean> {
        try {
            const queryRes: { child_id: number } | null = await Database.oneOrNone(
                `SELECT child_id
                FROM parent_child_relation
                WHERE child_id=$(child)
                  AND parent_id=$(parent)`,
                { childId, accountId },
            );
            if (queryRes === null) {
                return false;
            }
            return Number(queryRes.child_id) === childId;
        } catch (err) {
            console.error(`Could not check if parent: ${err}`);
            return false;
        }
    }

    public static async allParents(page: number, limit: number): Promise<AccountMin[] | null> {
        try {
            const offset = page * limit;
            const queryResult: Array<{ id: number, email: string }> = await Database.any(
                `SELECT id, email
                FROM parent
                    LEFT JOIN user_account
                    ON parent.user_account_id=user_account.id
                    OFFSET $(offset) LIMIT $(limit)`,
                { offset, limit },
            );
            return queryResult.map(
                (parentDbResult) => new AccountMin(parentDbResult.id, parentDbResult.email, true),
            );

        } catch (err) {
            console.error(`Error reading all parents: ${err}`);
            return null;
        }
    }

    public static async countParents(): Promise<number> {
        try {
            return Number((await Database.one("SELECT COUNT(*) FROM parent;")).count);
        } catch (err) {
            console.error(`Error counting parents: ${err}`);
            return -1;
        }
    }
}
