import { Child } from "../entities/Child";
import Address from "../entities/datatypes/Address";
import { Gender, Person, PersonMin } from "../entities/Person";

import Database from "./Database";

export default class ChildDao {
    public static async fromId(id: number): Promise<Child | null> {
        try {
            const childTableResult = await Database.oneOrNone(`SELECT * FROM child WHERE person_id=$(id)`, { id });
            if (childTableResult !== null) {
                // This could give a bug if a child is removed right in this spot (right where this comment is
                // located), but that would be a very rare event, and error handling would catch this.
                const personTableResult = await Database.one(`SELECT * FROM person WHERE id=$(id)`, { id });
                return await ChildDao.fromQuery({
                    registration_pending: childTableResult.registration_pending,
                    ...personTableResult,
                });
            } else {
                return null;
            }
        } catch (err) {
            console.error(`Error reading from Person from database: ${err}`);
            return null;
        }
    }

    /**
     * Returns all Persons in the database.
     */
    public static async all(): Promise<Child[] | null> {
        try {
            const result: Child[] = [];
            const queryResult = await Database.any(`
                SELECT *
                FROM person
                    INNER JOIN child
                    ON child.person_id = person.id
            `);
            if (queryResult === null || queryResult.length === 0) {
                return null;
            }
            for (const i of queryResult) {
                result.push(Child.withId(
                    i.id,
                    i.first_name,
                    i.last_name,
                    i.birthplace,
                    i.birthdate,
                    i.gender,
                    i.phone_number,
                    await ChildDao.addressFromQueryResult(i),
                    i.comments,
                ));
            }
        } catch (err) {
            console.error("Error reading Account from database: " + err);
        }
        return null;
    }

    /**
     * Returns all children as MinPersons in the database.
     */
    public static async allMin(): Promise<PersonMin[] | null> {
        try {
            const dbQuery = `
                SELECT person.id,
                    person.first_name,
                    person.last_name
                FROM person
                    INNER JOIN child
                    ON child.person_id = person.id
                ORDER BY person.id
            `;
            const queryResult = await Database.any(dbQuery);
            if (queryResult === null) {
                return null;
            }
            for (const i of queryResult) {
                return queryResult.map((person) => PersonMin.fromDbRow(person));
            }
        } catch (err) {
            console.error("Error reading Account from database: " + err);
        }
        return null;
    }

    /**
     * Returns all children as MinPersons in the database that share a group with given accountId.
     */
    public static async allShareAGroupMin(
        accountId: number,
    ): Promise<PersonMin[] | null> {
        try {
            const dbQuery = `
                SELECT DISTINCT person.id,
                    person.first_name,
                    person.last_name
                FROM person
                    INNER JOIN child
                    ON child.person_id = person.id
                    INNER JOIN group_person_relation
                    ON group_person_relation.person_id = person.id
                WHERE group_person_relation.group_id IN
                    (SELECT group_id
                    FROM group_person_relation
                        INNER JOIN leader
                        ON leader.person_id = group_person_relation.person_id
                    WHERE leader.user_account_id = $(accountId))
                ORDER BY person.id
            `;
            const queryResult = await Database.any(
                dbQuery,
                { accountId },
            );
            if (queryResult === null) {
                return null;
            }
            for (const i of queryResult) {
                return queryResult.map((person) => PersonMin.fromDbRow(person));
            }
        } catch (err) {
            console.error("Error reading Account from database: " + err);
        }
        return null;
    }

    public static async insertIntoDatabase(child: Child): Promise<number | null> {
        try {
            const address = child.getAddress();
            const addrStreet = address ? address.getStreetAddress() : null;
            const addrZip = address ? address.getZipcode() : null;
            const addrCity = address ? address.getCity() : null;

            let id = child.getId();
            if (!child.getId()) {
                id = (await Database.one(`
                INSERT INTO person(
                    first_name, last_name, gender, birthplace,
                    birthdate, phone_number, address_street,
                    address_zip, address_place, comments
                    )
                    VALUES (
                        $(first_name), $(last_name), $(gender), $(birthplace),
                        $(birthdate), $(phone_number), $(address_street),
                        $(address_zip), $(address_place), $(comments)
                    )
                    RETURNING id`,
                    {
                        address_place: addrCity,
                        address_street: addrStreet,
                        address_zip: addrZip,
                        birthdate: child.getBirthdate(),
                        birthplace: child.getBirthplace(),
                        comments: child.getComment(),
                        first_name: child.getFirstName(),
                        gender: child.getGender(),
                        last_name: child.getLastName(),
                        phone_number: child.getPhoneNumber(),
                    },
                )).id;
                await Database.none(
                    `INSERT INTO child(person_id, registration_pending) VALUES ($(id), $(registration))`,
                    { id, registration: child.getRegistrationPending() },
                );
            } else {
                await Database.none(`UPDATE child SET registration_pending=$(registration) WHERE person_id=$(id)`, {
                    id: child.getId(),
                    registration: child.getRegistrationPending(),
                });

                await Database.none(`
                        UPDATE person SET
                        first_name=$(first_name),
                        last_name=$(last_name),
                        gender=$(gender),
                        birthplace=$(birthplace),
                        birthdate=$(birthdate),
                        phone_number=$(phone_number),
                        address_street=$(address_street),
                        address_zip=$(address_zip),
                        address_place=$(address_place),
                        comments=$(comments)
                    WHERE id=$(id)`,
                    {
                        address_place: addrCity,
                        address_street: addrStreet,
                        address_zip: addrZip,
                        birthdate: child.getBirthdate(),
                        birthplace: child.getBirthplace(),
                        comments: child.getComment(),
                        first_name: child.getFirstName(),
                        gender: child.getGender(),
                        id: child.getId(),
                        last_name: child.getLastName(),
                        phone_number: child.getPhoneNumber(),
                    });
            }
            return id;
        } catch (err) {
            console.error(`Could not save Child: ${err}`);
            return null;
        }
    }

    public static addressFromQueryResult(queryResult: any): Address | null {
        return queryResult.address_street && queryResult.address_number &&
            queryResult.address_zip && queryResult.address_place
            ? new Address(
                queryResult.address_street,
                queryResult.address_zip,
                queryResult.address_place)
            : null;
    }

    private static fromQuery(
        queryResult: {
            id: number,
            first_name: string,
            last_name: string,
            gender: string,
            birthplace: string,
            birthdate: Date,
            phone_number: string,
            registration_pending: boolean,
            comments: string,
        },
    ): Child {
        const address: Address | null = ChildDao.addressFromQueryResult(queryResult);
        return Child.withId(
            queryResult.id,
            queryResult.first_name,
            queryResult.last_name,
            queryResult.birthplace,
            queryResult.birthdate,
            Gender[queryResult.gender as keyof typeof Gender],
            queryResult.phone_number,
            address,
            queryResult.comments,
            queryResult.registration_pending,
        );
    }

}

export { ChildDao };
