import Result from "../dataPermissionLayer/Result";
import { Leader } from "../entities/Leader";
import { PersonMin } from "../entities/Person";
import { AccountDao } from "./AccountDao";
import Database from "./Database";
import ParentDao from "./ParentDao";

class LeaderDao {
    /**
     * Inserts the given leader in the database. Returns a tuple (perid, accid) on success.
     * @param leader Leader to be added to the database.
     */
    public static async insertIntoDatabase(leader: Leader): Promise<[number, number] | null> {
        const accid = await AccountDao.saveToDatabase(leader.getAccount());
        try {
            let perid = leader.getId();

            const address = leader.getAddress();
            const addrStreet = address ? address.getStreetAddress() : null;
            const addrZip = address ? address.getZipcode() : null;
            const addrCity = address ? address.getCity() : null;

            if (perid) {
                await Database.none(`
                    UPDATE person SET
                        first_name=$(first_name),
                        last_name=$(last_name),
                        gender=$(gender),
                        birthplace=$(birthplace),
                        birthdate=$(birthdate),
                        phone_number=$(phone_number),
                        address_street=$(address_street),
                        address_zip=$(address_zip),
                        address_place=$(address_place),
                        comments=$(comments)
                    WHERE id=$(id)`,
                    {
                        address_place: addrCity,
                        address_street: addrStreet,
                        address_zip: addrZip,
                        birthdate: leader.getBirthdate(),
                        birthplace: leader.getBirthplace(),
                        comments: leader.getComment(),
                        first_name: leader.getFirstName(),
                        gender: leader.getGender(),
                        id: leader.getId(),
                        last_name: leader.getLastName(),
                        phone_number: leader.getPhoneNumber(),
                    });
            } else {
                perid = (await Database.one(`
                    INSERT INTO person(
                        first_name, last_name, gender, birthplace,
                        birthdate, phone_number, address_street,
                        address_zip, address_place, comments
                    ) VALUES (
                        $(first_name), $(last_name), $(gender), $(birthplace),
                        $(birthdate), $(phone_number), $(address_street),
                        $(address_zip), $(address_place), $(comments)
                    ) RETURNING id`, {
                        address_place: addrCity,
                        address_street: addrStreet,
                        address_zip: addrZip,
                        birthdate: leader.getBirthdate(),
                        birthplace: leader.getBirthplace(),
                        comments: leader.getComment(),
                        first_name: leader.getFirstName(),
                        gender: leader.getGender(),
                        last_name: leader.getLastName(),
                        phone_number: leader.getPhoneNumber(),
                    },
                )).id;
                await Database.none("INSERT INTO leader(user_account_id, person_id) VALUES ($(accid), $(perid))",
                    { perid, accid });
            }

            return accid && perid ? [accid, perid] : null;
        } catch (err) {
            console.error(`Could not insert Leader: ${err}`);
            return null;
        }
    }

    public static async translateAccountToPersonID(accID: number): Promise<number | null> {
        try {
            const accToPersonIdLink =
                await Database.oneOrNone(`
                SELECT person_id
                FROM leader
                WHERE user_account_id=$(accID)
                `, { accID });

            if (accToPersonIdLink === null) {
                return null;
            } else {
                return accToPersonIdLink.person_id;
            }
        } catch (e) {
            console.error(e);
            return null;
        }
    }

    public static async translatePersonToAccountID(personID: number): Promise<number | null> {
        try {
            const personToAccIdLink =
                await Database.oneOrNone(`
                SELECT user_account_id
                FROM leader
                WHERE person_id=$(personID)`, { personID });

            if (personToAccIdLink === null) {
                return null;
            } else {
                return personToAccIdLink.user_account_id;
            }
        } catch (e) {
            console.error(e);
            return null;
        }
    }

    /**
     * Get all leaders from the database
     */
    public static async getAll(): Promise<Result<PersonMin[]>> {
        const dbQuery: string = `
            SELECT *
            FROM leader INNER JOIN person ON leader.person_id = person.id`;

        try {
            const dbResult: any[] = await Database.any(dbQuery, {});
            return Result.correct(dbResult.map((dbLeader) => Leader.fromDbRow(dbLeader)));
        } catch (e) {
            console.error(e);
            return Result.error(500, `Database error`);
        }
    }
}

export { LeaderDao };
