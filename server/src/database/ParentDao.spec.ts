import assert = require("assert");
import Account from "../authentication/Account";
import Permissions from "../authentication/Permissions";
import { Child } from "../entities/Child";
import { Gender } from "../entities/Person";
import AccountDao from "./AccountDao";
import ChildDao from "./ChildDao";
import Database from "./Database";
import ParentDao from "./ParentDao";

describe("ParentDao", () => {
    describe("#checkIfParent", () => {
        it("should detect that admin is not a parent.", async () => {

            const child = Child.newChild("TEST", "PARENT", "Diepenbeek", new Date("2019-01-07"), Gender.Other);

            const childId = await ChildDao.insertIntoDatabase(child);

            const parent = Account.fromUnhashedPassword("parent", "parent", Permissions.empty(), true);
            const parentId = await AccountDao.saveToDatabase(parent);

            Database.none(`INSERT INTO parent_child_relation (parent_id, child_id) VALUES ($(parentId), $(childId))`,
                { parentId, childId });

            assert(ParentDao.isParentOf(childId!, parentId!));

            await Database.none(`DELETE FROM user_account WHERE id=$(parentId)`, { parentId });
            await Database.none(`DELETE FROM person WHERE id=$(childId)`, { childId });
        });
    });
});
