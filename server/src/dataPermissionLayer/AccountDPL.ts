import { Request } from "express";
import { Account } from "../authentication/Account";
import SessionManager from "../authentication/SessionManager";

import { Permission } from "../authentication/Permission";
import AccountDao from "../database/AccountDao";
import checkCanDeleteAccount from "./permissionCheck/checkCanDeleteAccount";
import checkLoggedIn from "./permissionCheck/checkLoggedIn";
import Result from "./Result";
import Status from "./Status";

export default class AccountDPL {
    public static async deleteFromId(req: Request, requestedAccountId: number): Promise<Result<boolean>> {
        const UNAUTHORIZED_DELETE_MESSAGE = "unauthorized to delete person";

        const accountPromise: Promise<Account | null> = AccountDao.fromIdDatabase(requestedAccountId);
        const loggedInUser: Account | null = await SessionManager.isLoggedIn(req);

        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        const account: Account | null = await accountPromise;
        if (account === null) {
            return Result.error(404, "No account found");
        }

        const canDeleteAccount = checkCanDeleteAccount(loggedInUser!, account);
        if (canDeleteAccount.isError()) {
            return Result.error(404, UNAUTHORIZED_DELETE_MESSAGE);
        } else {
            return Result.correct(await AccountDao.deleteAccount(requestedAccountId));
        }
    }

    public static async viewId(req: Request, requestedAccountId: number): Promise<Result<Account>> {
        const UNAUTHORIZED_ACCESS_MESSAGE = "Unauthorized to view account.";

        const accountPromise: Promise<Account | null> = AccountDao.fromIdDatabase(requestedAccountId);
        const loggedInUser: Account | null = await SessionManager.isLoggedIn(req);

        // Not signed in
        if (loggedInUser === null) {
            return Result.error(404, UNAUTHORIZED_ACCESS_MESSAGE);
        }

        // No account was found
        const account = await accountPromise;
        if (account === null) {
            return Result.error(404, "account not found");
        }

        // You can view your own account
        if (account.getUid() === loggedInUser.getUid()) {
            return Result.correct(account);
        }

        if (account.isParent()) {
            // A Parent has been requested
            if (loggedInUser.getPermissions().hasPermission(Permission.ParentAdministrator)) {
                return Result.correct(account);
            } else {
                return Result.error(404, UNAUTHORIZED_ACCESS_MESSAGE);
            }
        } else {
            // A Leader has been requested
            const permissions = loggedInUser.getPermissions();
            if (permissions.hasPermission(Permission.LeaderAdministrator)
                || permissions.hasPermission(Permission.LeaderViewer)) {
                return Result.correct(account);
            } else {
                return Result.error(404, UNAUTHORIZED_ACCESS_MESSAGE);
            }
        }
    }
}
