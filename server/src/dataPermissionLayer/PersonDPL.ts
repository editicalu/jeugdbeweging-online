import { Request } from "express";
import { Account } from "../authentication/Account";
import SessionManager from "../authentication/SessionManager";
import PersonDao from "../database/PersonDao";
import { Child } from "../entities/Child";
import { Leader } from "../entities/Leader";

import { Person } from "../entities/Person";
import checkCanDeletePerson from "./permissionCheck/checkCanDeletePerson";

import checkCanAccessPersonData from "./permissionCheck/checkCanAccessPersonData";
import checkLoggedIn from "./permissionCheck/checkLoggedIn";
import Result from "./Result";
import Status from "./Status";

export default class PersonDPL {

    /**
     * Get a person based on it's id
     * @param req the client-request
     * @param personId the id of the person wich you want data of
     * @returns The person you asked for
     */
    public static async getFromId(req: Request, personId: number): Promise<Result<Person>> {
        const loggedInUser: Account | null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        // Check if logged in user can view the asked person's data
        const canAccessPersonDataStatus: Status =
            await checkCanAccessPersonData(loggedInUser!, personId);
        if (canAccessPersonDataStatus.isError()) {
            return Result.errorByStatus(canAccessPersonDataStatus);
        }

        if (canAccessPersonDataStatus) {
            // Load data from database
            const personResult: Result<Person> = await PersonDao.fromIdDatabase(personId);
            return personResult;
        } else {
            return Result.error(404, "unauthorized to access person");
        }
    }

    public static async deleteFromId(req: Request, personId: number): Promise<Result<boolean>> {
        const UNAUTHORIZED_DELETE_MESSAGE = "unauthorized to delete person";

        const personPromise: Promise<Child | Leader | null> = PersonDao.fromId(personId);
        const loggedInUser: Account | null = await SessionManager.isLoggedIn(req);

        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        const person: Child | Leader | null = await personPromise;
        if (person === null) {
            return Result.error(404, "No person found");
        }

        const canDeletePerson = checkCanDeletePerson(loggedInUser!, person);
        if (canDeletePerson.isError()) {
            return Result.error(404, UNAUTHORIZED_DELETE_MESSAGE);
        } else {
            return Result.correct(await PersonDao.deletePerson(personId));
        }
    }
}
