import { Request } from "express";
import { Account } from "../authentication/Account";
import SessionManager from "../authentication/SessionManager";
import { Person } from "../entities/Person";
import PersonDPL from "./PersonDPL";
import Result from "./Result";
import Status from "./Status";

import BillDao from "../database/BillDao";
import Bill from "../entities/Bill";
import checkCanAccessAllBills from "./permissionCheck/checkCanAccessAllBills";
import checkCanAccessBill from "./permissionCheck/checkCanAccessBill";
import checkCanAccessPersonData from "./permissionCheck/checkCanAccessPersonData";
import checkCanEditBill from "./permissionCheck/checkCanEditBill";
import checkLoggedIn from "./permissionCheck/checkLoggedIn";

export default class BillDPL {
    /**
     * Get a bill by it's id
     * You only get bills you can view
     * @param req client-request
     * @param billId id of the bill you want to see
     */
    public static async getBill(req: Request, billId: number): Promise<Result<Bill>> {
        const loggedInUser: Account|null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        // Check if user can access this bill's data
        const canAccessBillStatus: Status = await checkCanAccessBill(loggedInUser!, billId);
        if (canAccessBillStatus.isError()) {
            return Result.errorByStatus(canAccessBillStatus);
        }

        const getbillResult: Result<Bill> = await BillDao.getBill(billId);
        return getbillResult;
    }

    /**
     * Get a bill by it's id
     * You only get bills you can view
     * @param req client-request
     * @param billId id of the bill you want to see
     */
    public static async getAllBills(req: Request): Promise<Result<Bill[]>> {
        const loggedInUser: Account|null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        // Check if user is allowed to access all bills
        const canAccessBillStatus: Status = await checkCanAccessAllBills(loggedInUser!);
        if (canAccessBillStatus.isError()) {
            return Result.errorByStatus(canAccessBillStatus);
        }

        const getbillResult: Result<Bill[]> = await BillDao.getAllBills();
        return getbillResult;
    }

    /**
     * Get all the persons that got billed on a certain bill,
     * you get only the persons you can see
     * @param req the client request
     * @param billId the id of the bill
     * @param paid if true: get all billed people that billed,
     *      else get all billed people that didn't pay
     * @returns All persons you can access and that are billed on given bill and have(n't) payed
     */
    public static async getBilled(req: Request, billId: number, paid: boolean): Promise<Result<Person[]>> {
        const loggedInUser: Account|null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        // Check if user can access this bill's data
        const canAccessBillStatus: Status = await checkCanAccessBill(loggedInUser!, billId);
        if (canAccessBillStatus.isError()) {
            return Result.errorByStatus(canAccessBillStatus);
        }

        // Get the asked id's
        const askedIdsResult: Result<number[]> = await BillDao.getBilledIdsByPaid(billId, paid);
        if (askedIdsResult.isError()) {
            return Result.errorByStatus(askedIdsResult.getStatus());
        }

        const askedPerson: Person[] = [];
        for (const askedId of askedIdsResult.getContent()!) {
            const askedPersonResult: Result<Person> = await PersonDPL.getFromId(req, askedId);
            if (askedPersonResult.isCorrect()) {
                askedPerson.push(askedPersonResult.getContent()!);
            }
        }

        return Result.correct(askedPerson);
    }

    /**
     * Save a new bill to the database
     * @param req client-request
     * @param personIds list of person-id's
     * @param bill the bill containing nescesary data
     */
    public static async saveNew(req: Request, personIds: number[], description: string, amount: number):
    Promise<Result<number>> {
        const loggedInUser: Account|null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        // Check if user has right to edit this bill
        const canEditBillStatus: Status = await checkCanEditBill(loggedInUser!);
        if (canEditBillStatus.isError()) {
            return Result.errorByStatus(canEditBillStatus);
        }

        const saveNewResult: Result<number> = await BillDao.saveNew(personIds, description, amount);
        return saveNewResult;
    }

    /**
     * update a bill if you have the right to do it
     * @param req client-request
     * @param bill the bill containing all the data
     * @returns Result<void>
     */
    public static async edit(req: Request, bill: Bill): Promise<Result<void>> {
        const loggedInUser: Account|null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus<void>(loggedInStatus);
        }

        // Check if user has right to edit this bill
        const canEditBillStatus: Status = await checkCanEditBill(loggedInUser!);
        if (canEditBillStatus.isError()) {
            return Result.errorByStatus<void>(canEditBillStatus);
        }

        return await BillDao.edit(bill);
    }

    /**
     * Link new persons to a certain bill
     * @param req client-request
     * @param personIds Array of person-id's to add
     * @param billId Bill-id to add persons to
     */
    public static async addPersons(req: Request, personIds: number[], billId: number): Promise<Result<void>> {
        const loggedInUser: Account|null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            throw loggedInStatus;
        }

        // Check if user has right to edit this bill
        const canEditBillStatus: Status = await checkCanEditBill(loggedInUser!);
        if (canEditBillStatus.isError()) {
            throw canEditBillStatus;
        }

        return await BillDao.addPersons(billId, personIds);
    }

    /**
     * Remove a person off a certain bill
     * @param req the client-request
     * @param personId the id of the person
     * @param billId the id of the bill
     */
    public static async removePerson(req: Request, personId: number, billId: number): Promise<Result<void>> {
        const loggedInUser: Account|null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            throw loggedInStatus;
        }

        // Check if user has right to edit this bill
        const canEditBillStatus: Status = await checkCanEditBill(loggedInUser!);
        if (canEditBillStatus.isError()) {
            throw canEditBillStatus;
        }

        return await BillDao.removePersons(billId, [personId]);
    }

    /**
     * set the paid status of a person on a certain bill
     * @param req client-request
     * @param personId id of the person
     * @param billId id of the bill
     * @param paid true if paid, else false
     * @returns Result<void>
     */
    public static async setPaid(req: Request, personId: number, billId: number, paid: boolean): Promise<Result<void>> {
        const loggedInUser: Account|null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        // Check if user has right to edit this bill
        const canEditBillStatus: Status = await checkCanEditBill(loggedInUser!);
        if (canEditBillStatus.isError()) {
            return Result.errorByStatus(canEditBillStatus);
        }

        const setPaidResult: Result<void> = await BillDao.setPaid(billId, personId, paid);
        return setPaidResult;
    }

    /**
     * Get all the bills belonging to that person.
     * Works only if you have rights to view this person.
     * Only bills you have permission to see will be returned
     * @param req client-request
     * @param personId the id of the person
     * @param paid true if you want all paid bills, else the unpaid bills
     */
    public static async getBillsOfPerson(req: Request, personId: number, paid: boolean): Promise<Result<Bill[]>> {
        const loggedInUser: Account|null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        // Check if user can acces this person's data
        const canAccessPersonDataStatus: Status = await checkCanAccessPersonData(loggedInUser!, personId);
        if (canAccessPersonDataStatus.isError()) {
            return Result.errorByStatus(canAccessPersonDataStatus);
        }

        const billsResult: Result<Bill[]> =  await BillDao.getBillsOfPerson(personId, paid);
        if (billsResult.isError()) {
            return billsResult;
        }

        // filter all bills the logged in user can't see
        const billsFiltered: Bill[] = [];
        for (const bill of billsResult.getContent()!) {
            if (bill.id !== undefined) {
                const canAccessBillStatus: Status = await checkCanAccessBill(loggedInUser!, bill.id);
                if (!canAccessBillStatus.isError()) {
                    billsFiltered.push(bill);
                }
            }
        }

        return Result.correct(billsFiltered);
    }

    /**
     * Delete a certain bill
     * @param req client-request
     * @param billId the id of the bill you want to delete
     */
    public static async delete(req: Request, billId: number): Promise<Result<void>> {
        const loggedInUser: Account|null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        // Check if user has right to edit this bill
        const canEditBillStatus: Status = await checkCanEditBill(loggedInUser!);
        if (canEditBillStatus.isError()) {
            return Result.errorByStatus(canEditBillStatus);
        }

        // delete bill
        const deleteBillResult: Result<void> = await BillDao.removeBill(billId);
        return deleteBillResult;
    }

}
