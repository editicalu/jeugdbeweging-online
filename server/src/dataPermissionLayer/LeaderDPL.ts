import { Request } from "express";
import { Account } from "../authentication/Account";
import SessionManager from "../authentication/SessionManager";
import PersonDao from "../database/PersonDao";
import { Gender, Person, PersonMin } from "../entities/Person";

import { LeaderDao } from "../database/LeaderDao";
import Address from "../entities/datatypes/Address";
import checkCanAccessPersonData from "./permissionCheck/checkCanAccessPersonData";
import checkCanEditPersonData from "./permissionCheck/checkCanEditPersonData";
import checkLoggedIn from "./permissionCheck/checkLoggedIn";
import Result from "./Result";
import Status from "./Status";

export default class LeaderDPL {
    /**
     * Get all leaders from the server
     * @param loggedInUser the currenty logged in user
     */
    public static async getAll(loggedInUser: Account|null): Promise<Result<PersonMin[]>> {
        // Check if user is logged in
        const loggedInStatus: Status = checkLoggedIn(loggedInUser);
        if (loggedInStatus.isError()) {
            return Result.errorByStatus(loggedInStatus);
        }

        // Get all leaders from database
        const allLeaderResult: Result<PersonMin[]> = await LeaderDao.getAll();
        if (allLeaderResult.isError()) {
            return Result.errorByStatus(allLeaderResult.getStatus());
        }

        const allLeaders: PersonMin[] = allLeaderResult.getContent()!;
        const filteredLeaders: PersonMin[] = [];

        allLeaders.forEach(async (leader) => {
            if (leader.id !== null) {
                const canAccessLeaderStatus: Status = await checkCanAccessPersonData(loggedInUser!, leader.id);
                if (!canAccessLeaderStatus.isError()) {
                    filteredLeaders.push(leader);
                }
            }
        });

        return Result.correct(allLeaders);
    }
}
