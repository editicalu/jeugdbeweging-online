/**
 * A Status represents an option of an error, that can be given when an Account tries to access
 * some data.
 */
export default class Status {
    public static error(errorCode: number, message: string): Status {
        return new Status(false, errorCode, message);
    }

    public static correct(): Status {
        return new Status(true, undefined, undefined);
    }

    private constructor(
        private correct: boolean,
        private errorCode: number | undefined,
        private message: string | undefined,
    ) {
        this.correct = correct;
        this.errorCode = errorCode;
        this.message = message;
    }

    public isError(): boolean {
        return !this.correct;
    }

    public getErrorCode(): number {
        if (this.errorCode === undefined) {
            throw Error("Errorcode is undefined");
        }
        return this.errorCode;
    }

    public getMessage(): string {
        if (this.message === undefined) {
            throw Error("Message is undefined");
        }
        return this.message;
    }
}
