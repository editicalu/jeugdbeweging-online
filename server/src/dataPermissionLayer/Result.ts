import { Response } from "express";
import Status from "./Status";

/**
 * A Result represents the access to a given object T.
 */
export default class Result<T> {

    /**
     * Access to the referenced item is permitted.
     * @param content The data that can be accessed
     */
    public static correct<A>(content?: A): Result<A> {
        return new Result<A>(content, Status.correct());
    }

    /**
     * Access to the referenced item is not allowed.
     * @param errorcode The HTTP error code representing the error
     * @param message An error message that should be returned to the user
     */
    public static error<T>(errorcode: number, message: string): Result<T> {
        return new Result<T>(undefined, Status.error(errorcode, message));
    }

    /**
     * Access to the referenced item is not allowed.
     * @param status An Status object, containing the HTTP error code and an error message
     */
    public static errorByStatus<T>(status: Status): Result<T> {
        return new Result<T>(undefined, status);
    }

    private constructor(
        private content: T | undefined,
        private status: Status,
    ) {
        this.content = content;
        this.status = status;
    }

    public isCorrect(): boolean {
        return !this.status.isError();
    }

    public isError(): boolean {
        return this.status.isError();
    }

    public getContent(): T | undefined {
        return this.content;
    }

    public getErrorCode(): number {
        return this.status.getErrorCode();
    }

    public getErrorMessage(): string {
        return this.status.getMessage();
    }

    public getStatus(): Status {
        return this.status;
    }
}
