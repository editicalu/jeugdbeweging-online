import Account from "../../authentication/Account";
import { Permission } from "../../authentication/Permission";
import Status from "../Status";

export default async function checkCanEditBill(loggedInUser: Account): Promise<Status> {
    // Check if logged in user is financial
    const isFinancial: boolean = loggedInUser.getPermissions().hasPermission(Permission.Financial);
    if (isFinancial) {
        return Status.correct();
    }

    return Status.error(403, "Unauthorized to edit bills");
}
