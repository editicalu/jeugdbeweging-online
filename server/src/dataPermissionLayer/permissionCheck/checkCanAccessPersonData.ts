import Account from "../../authentication/Account";
import { Permission } from "../../authentication/Permission";
import GroupDao from "../../database/group/GroupDao";
import GroupsDao from "../../database/group/GroupsDao";
import { LeaderDao } from "../../database/LeaderDao";
import PersonDao from "../../database/PersonDao";
import { Person } from "../../entities/Person";
import { PersonType } from "../../entities/PersonType";
import LeaderDPL from "../LeaderDPL";
import Result from "../Result";
import Status from "../Status";
import isParentOf from "./isParentOf";

export default async function checkCanAccessPersonData(loggedInUser: Account, personId: number): Promise<Status> {
    // Load person from database, if not exist --> return error
    const personResult: Result<Person> = await PersonDao.fromIdDatabase(personId);
    if (personResult.isError()) {
        return personResult.getStatus();
    }
    const person: Person = personResult.getContent()!;

    // Check if logged in user is a leader and wants to view his own data
    if (!loggedInUser.isParent()) {
        const loggedInPersonId: number | null = await LeaderDao.translateAccountToPersonID(loggedInUser.getUid()!);
        if (loggedInPersonId === null) {
            return Status.error(500, "Internal server error");
        }
        if (person.getId() === loggedInPersonId) {
            return Status.correct();
        }
    }

    // Check if logged in user is a parent and wants to view data of child
    const isParentOfPerson: boolean = await isParentOf(loggedInUser, personId);
    if (isParentOfPerson) {
        return Status.correct();
    }

    // Check if logged in user has rights to view all children and person is a child
    const personIsChild: boolean = person.getPersonType() === PersonType.Child;
    const isChildrenAdministrator: boolean =
        loggedInUser.getPermissions().hasPermission(Permission.ChildrenAdministrator);
    const isChildrenViewer: boolean =
        loggedInUser.getPermissions().hasPermission(Permission.ChildrenViewer);
    if (personIsChild && (isChildrenAdministrator || isChildrenViewer)) {
        return Status.correct();
    }

    // check if logged in user has rights to view al people
    const isLeaderAdministrator: boolean =
        loggedInUser.getPermissions().hasPermission(Permission.LeaderAdministrator);
    if (isLeaderAdministrator && !personIsChild) {
        return Status.correct();
    }

    // check if logged in user is leader and has rights to view other leader
    if (LeaderDao.translatePersonToAccountID(personId) !== null
    && !loggedInUser.isParent()
    && loggedInUser.getPermissions().hasPermission(Permission.LeaderViewer)) {
        return Status.correct();
    }

    // check if users share a group
    const accId = loggedInUser.getUid() || 0;
    const sharedGroupIds =
        await new GroupsDao()
            .getSharedGroupings(personId, accId);

    if (sharedGroupIds !== null) {
        const GDao = new GroupDao();
        const leaderOfGroupChances = await Promise.all(
            sharedGroupIds.map((gId) => GDao.leaderInGrouping(accId, gId)),
        );

        if (leaderOfGroupChances.some((c) => c === true)) {
            return Status.correct();
        }
    }

    // else return false
    return Status.error(403, "Unauthorized, no permission to access this person's data");
}
