import Account from "../../authentication/Account";
import { Permission } from "../../authentication/Permission";
import Status from "../Status";

export default function checkCanDeleteAccount(loggedInUser: Account, requestedAccount: Account): Status {
    const UNAUTRHORIZED_MESSAGE = "Unauthorized to delete account";

    if (requestedAccount.isParent()) {
        if (loggedInUser.getPermissions().hasPermission(Permission.ParentAdministrator)) {
            return Status.correct();
        } else {
            return Status.error(404, UNAUTRHORIZED_MESSAGE);
        }
    } else {
        if (loggedInUser.getPermissions().hasPermission(Permission.LeaderAdministrator)) {
            return Status.correct();
        } else {
            return Status.error(404, UNAUTRHORIZED_MESSAGE);
        }
    }
}
