import Account from "../../authentication/Account";
import { Permission } from "../../authentication/Permission";
import { LeaderDao } from "../../database/LeaderDao";
import ParentDao from "../../database/ParentDao";
import PersonDao from "../../database/PersonDao";
import { Child } from "../../entities/Child";
import { Person } from "../../entities/Person";
import { PersonType } from "../../entities/PersonType";
import Result from "../Result";
import Status from "../Status";

export default async function isParentOf(loggedInUser: Account, personId: number): Promise<boolean> {
    if (!loggedInUser.isParent()) {
        return false;
    }

    const children: Child[]|null = await ParentDao.getChildren(loggedInUser.getUid()!);
    if (children === null) {
        return false;
    }
    for (const child of children) {
        if (child.getId() === personId) {
            return true;
        }
    }
    return false;
}
