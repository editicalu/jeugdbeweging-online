import Account from "../../authentication/Account";
import { Permission } from "../../authentication/Permission";
import { LeaderDao } from "../../database/LeaderDao";
import Result from "../Result";
import Status from "../Status";
import isParentOf from "./isParentOf";

/**
 * Check if an account has permission to get an accounts data
 * @param loggedInUser the logged in account
 * @param accountId the id of the account one wants to get the data from
 */
export default function checkCanAccessAccountData(loggedInUser: Account, accountId: number): Status {
    // Check if logged in user has permission to edit parents
    if (loggedInUser.getPermissions().hasPermission(Permission.ParentAdministrator)) {
        return Status.correct();
    }

    // Check if logged in user is this person
    if (loggedInUser.getUid() === accountId) {
        return Status.correct();
    }

    return Status.error(403, `Unauthorized`);

}
