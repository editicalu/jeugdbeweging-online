import Account from "../../authentication/Account";
import { Permission } from "../../authentication/Permission";
import { LeaderDao } from "../../database/LeaderDao";
import Result from "../Result";
import isParentOf from "./isParentOf";

/**
 *
 * @param loggedInUser logged in user
 * @param personId the id you want to edit
 * @returns a result, this contains true if user can edit all data, and false if user can't edit all data
 */
export default async function checkCanEditPersonData(loggedInUser: Account, personId: number):
Promise<Result<boolean>> {
    // Check if logged in user has rights to edit all children
    const isChildrenAdministrator = loggedInUser.getPermissions().hasPermission(Permission.ChildrenAdministrator);
    if (isChildrenAdministrator) {
        return Result.correct(true);
    }

    // Check if logged in user is a leader and wants to view his own data
    if (!loggedInUser.isParent()) {
        const loggedInPersonId: number|null = await LeaderDao.translateAccountToPersonID(loggedInUser.getUid()!);
        if (loggedInPersonId === null) {
            return Result.error(500, "Internal server error");
        }
        if (personId === loggedInPersonId) {
            return Result.correct(true);
        }
    }

    // Check if logged in user is parent of the person
    const isParentOfPerson: boolean = await isParentOf(loggedInUser, personId);
    if (isParentOfPerson) {
        return Result.correct(false);
    }

    return Result.error(403, `Unauthorized`);
}
