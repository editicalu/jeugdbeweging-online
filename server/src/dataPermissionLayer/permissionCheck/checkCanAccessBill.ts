import Account from "../../authentication/Account";
import { Permission } from "../../authentication/Permission";
import BillDao from "../../database/BillDao";
import { LeaderDao } from "../../database/LeaderDao";
import ParentDao from "../../database/ParentDao";
import { Child } from "../../entities/Child";
import Result from "../Result";
import Status from "../Status";

export default async function checkCanAccessBill(loggedInUser: Account, billId: number): Promise<Status> {
    // Check if logged in user is financial
    const isFinancial: boolean = loggedInUser.getPermissions().hasPermission(Permission.Financial);
    if (isFinancial) {
        return Status.correct();
    }

    const billedIdsResult: Result<number[]> = await BillDao.getBilledIds(billId);
    if (billedIdsResult.isError()) {
        return billedIdsResult.getStatus();
    }

    const billedIds: number[] = billedIdsResult.getContent()!;

    // Check if logged in user is leader and is assigned to bill
    if (!loggedInUser.isParent()) {
        const loggedInPersonId: number|null = await LeaderDao.translateAccountToPersonID(loggedInUser.getUid()!);
        if (loggedInPersonId === null) {
            return Status.error(404, "Database error: could not find person-id of logged in leader");
        }
        if (billedIds.includes(loggedInPersonId)) {
            return Status.correct();
        }
    }

    // Check if logged in user is parent and one of children is assigned to bill
    if (loggedInUser.isParent()) {
        let children: Child[]|null = await ParentDao.getChildren(loggedInUser.getUid()!);
        if (children === null) {
            children = [];
        }
        for (const child of children) {
            if (billedIds.includes(child.getId()!)) {
                return Status.correct();
            }
        }
    }

    return Status.error(403, "Unauthorized");
}
