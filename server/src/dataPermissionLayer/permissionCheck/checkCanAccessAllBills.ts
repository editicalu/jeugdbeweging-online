import Account from "../../authentication/Account";
import { Permission } from "../../authentication/Permission";
import BillDao from "../../database/BillDao";
import { LeaderDao } from "../../database/LeaderDao";
import ParentDao from "../../database/ParentDao";
import { Child } from "../../entities/Child";
import Result from "../Result";
import Status from "../Status";

export default async function checkCanAccessBill(loggedInUser: Account): Promise<Status> {
    // Check if logged in user is financial
    const isFinancial: boolean = loggedInUser.getPermissions().hasPermission(Permission.Financial);
    if (isFinancial) {
        return Status.correct();
    }

    return Status.error(403, "Unauthorized");
}
