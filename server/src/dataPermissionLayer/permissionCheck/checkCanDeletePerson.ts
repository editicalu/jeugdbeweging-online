import Account from "../../authentication/Account";
import { Permission } from "../../authentication/Permission";
import { Person } from "../../entities/Person";
import { PersonType } from "../../entities/PersonType";
import Status from "../Status";

export default function checkCanDeletePerson(loggedInUser: Account, requestedAccount: Person): Status {
    const UNAUTRHORIZED_MESSAGE = "Unauthorized to delete person";

    if (requestedAccount.getPersonType() === PersonType.Child) {
        if (loggedInUser.getPermissions().hasPermission(Permission.ChildrenAdministrator)) {
            return Status.correct();
        } else {
            return Status.error(404, UNAUTRHORIZED_MESSAGE);
        }
    } else {
        if (loggedInUser.getPermissions().hasPermission(Permission.LeaderAdministrator)) {
            return Status.correct();
        } else {
            return Status.error(404, UNAUTRHORIZED_MESSAGE);
        }
    }
}
