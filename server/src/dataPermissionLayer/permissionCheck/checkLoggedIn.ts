import Account from "../../authentication/Account";
import Status from "../Status";

export default function checkLoggedIn(loggedInUser: Account|null): Status {
    if (loggedInUser === null) {
        return Status.error(403, "Unauthorized: user not logged in");
    } else {
        return Status.correct();
    }
}
