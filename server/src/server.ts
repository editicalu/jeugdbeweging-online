// Receive configuration file before anything else.
import process_configuration from "./server/configuration";
process_configuration();

import express = require("express");

import add_account_routes from "./server/account";
import add_authentication_routes from "./server/authentication";
import add_bills_routes from "./server/bills";
import add_child_routes from "./server/child";
import add_datarequest_routes from "./server/dataRequest";
import add_leader_routes from "./server/leader";
import add_parent_routes from "./server/parent";
import add_person_routes from "./server/person";
import add_report_routes from "./server/report";
import ActivitiesRouter from "./server/router/activity/ActivitiesRouter";
import ActivityMemberRouter from "./server/router/activity/ActivityMemberRouter";
import ActivityRouter from "./server/router/activity/ActivityRouter";
import ChecklistMemberRouter from "./server/router/checklist/ChecklistMemberRouter";
import ChecklistRouter from "./server/router/checklist/ChecklistRouter";
import ChecklistsRouter from "./server/router/checklist/ChecklistsRouter";
import GroupMemberRouter from "./server/router/group/GroupMemberRouter";
import GroupRouter from "./server/router/group/GroupRouter";
import GroupsRouter from "./server/router/group/GroupsRouter";

import add_express_uses from "./server/use";

// Setup webserver
const app = express();

// Injecting app.use-commands
add_express_uses(app);

// Adding routes
add_account_routes(app);
add_authentication_routes(app);
add_person_routes(app);
new GroupRouter().addRoutes(app);
new GroupsRouter().addRoutes(app);
new GroupMemberRouter().addRoutes(app);
add_leader_routes(app);
add_report_routes(app);
add_datarequest_routes(app);
add_child_routes(app);
add_bills_routes(app);
add_parent_routes(app);
new ChecklistRouter().addRoutes(app);
new ChecklistsRouter().addRoutes(app);
new ChecklistMemberRouter().addRoutes(app);
new ActivityRouter().addRoutes(app);
new ActivitiesRouter().addRoutes(app);
new ActivityMemberRouter().addRoutes(app);

export { app };
