import { kdfSync, paramsSync, verifyKdfSync } from "scrypt";
import Permissions from "./Permissions";

/**
 * An Account represents an entity that is able to login on the system
 */
export default class Account {
    /**
     * Creates a new Account from an unhashed password. This should be called when creating a new
     * user account that should be saved to a database.
     * @param email The email address
     * @param password The unhashed password
     */
    public static fromUnhashedPassword(
        email: string,
        password: string,
        permissions: Permissions | null,
        parent: boolean): Account {
        return new Account(email, this.hashPassword(password), null, permissions, parent);
    }

    /**
     * Creates a new Account from a hashed password. This should be called when loading a account from
     * the database.
     * @param email The email address
     * @param HashedPassword The hashed password
     * @param uid A unique identifier for this account
     * @param permissions Permissions for this new account
     * @param parent Whether this account is a parent
     */
    public static fromHashedPassword(
        email: string, hashedPassword: string, uid: number, permissions: Permissions | null, parent: boolean): Account {
        return new Account(email, hashedPassword, uid, permissions, parent);
    }

    /**
     * Returns an hashed version of the given password.
     * @param password An unhashed password
     */
    public static hashPassword(password: string): string {
        const scryptParameters = paramsSync(0.1);
        return kdfSync(password, scryptParameters).toString("hex");
    }

    /**
     * The email address used to login.
     */
    private email: string;

    /**
     * An hashed version of the password used to login.
     */
    private hashedPassword: string;

    /**
     * unique identifier of the user. This could correspond to a primary key in the database.
     *
     * If the user is new in the system and does not yet have such uid, this should be set to null.
     */
    private uid: number | null;

    /**
     * Permissions with the things that this account is allowed to do.
     */
    private permissions: Permissions;

    /**
     * Whether this account is a parent.
     */
    private parent: boolean;

    /**
     * Creates a new Account using the given credentials.
     * @param email The email address used to login
     * @param HashedPassword An hashed version of the password used to login
     */
    private constructor(
        email: string,
        hashedPassword: string,
        uid: number | null = null,
        permissions: Permissions | null = null,
        parent: boolean = true) {
        this.email = email;
        this.hashedPassword = hashedPassword;
        this.uid = uid;
        this.permissions = permissions ? permissions : Permissions.empty();
        this.parent = parent;
    }

    /**
     * Tests whether the provided password is valid.
     * @param password The password this Account should be checked against
     */
    public verifyPassword(password: string): boolean {
        return verifyKdfSync(Buffer.from(this.hashedPassword, "hex"), password);
    }

    /**
     * Returns the email address of this account.
     */
    public getEmail(): string {
        return this.email;
    }

    public setEmail(email: string) {
        this.email = email;
    }

    /**
     * Returns the hashed version of the password.
     */
    public getHashedPassword(): string {
        return this.hashedPassword;
    }

    /**
     * Sets the given unhashed password as the new password.
     * @param password The unhashed password
     */
    public setPassword(password: string) {
        this.hashedPassword = Account.hashPassword(password);
    }

    /**
     * Returns the unique identifier of this Account. Returns null if no such uid exists.
     */
    public getUid(): number | null {
        return this.uid;
    }

    /**
     * Returns the permissions of this account.
     */
    public getPermissions(): Permissions {
        return this.permissions;
    }

    /**
     * Sets the permissions of this account to the given value
     * @param permissions The permissions to be applied
     */
    public setPermissions(permissions: Permissions) {
        this.permissions = permissions;
    }

    public isParent(): boolean {
        return this.parent;
    }

    public toObject(): object {
        return {
            email: this.email,
            parent: this.parent,
            permissions: this.getPermissions().getRawNumber(),
            uid: this.uid,
        };
    }
}

class AccountMin {
    public id: number;
    public email: string;
    public isParent: boolean;

    public constructor(id: number, email: string, isParent: boolean) {
        this.id = id;
        this.email = email;
        this.isParent = isParent;
    }

    public toObject(): object {
        return { id: this.id, email: this.email, isParent: this.isParent };
    }
}

export { Account, AccountMin };
