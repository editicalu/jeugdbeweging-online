import assert = require("assert");
import { Permission } from "./Permission";
import Permissions from "./Permissions";

describe("Permissions", () => {
    describe("#newPermissions", () => {
        it("should give no permissions at all", () => {
            const permissions = Permissions.empty();
            const keys: number[] = Object.keys(Permission)
                .filter((v) => /^[0-9]+$/gi.test(v)).map((item) => Number(item)) as number[];
            keys
                .forEach((item: number) => {
                    assert(!permissions.hasPermission(item));
                });
            assert.deepStrictEqual(permissions.allPermissions(), []);
            assert.equal(permissions.toHexString(), "0x00000000");
        });
    });

    describe("#onePermission", () => {
        it("should only assign the ChildrenViewer permission", () => {
            const permissions = Permissions.empty();
            permissions.addPermission(Permission.ChildrenViewer);

            assert(permissions.hasPermission(Permission.ChildrenViewer) === true);
            assert(permissions.hasPermission(Permission.ChildrenAdministrator) === false);
            assert(permissions.hasPermission(Permission.LeaderAdministrator) === false);
            assert(permissions.hasPermission(Permission.ParentAdministrator) === false);
            assert(permissions.hasPermission(Permission.Financial) === false);
            assert(permissions.hasPermission(Permission.Reporter) === false);
            assert(permissions.hasPermission(Permission.Grouper) === false);
        });
        it("should only assign the ChildrenAdministrator permission", () => {
            const permissions = Permissions.empty();
            permissions.addPermission(Permission.ChildrenAdministrator);

            assert(permissions.hasPermission(Permission.ChildrenViewer) === false);
            assert(permissions.hasPermission(Permission.ChildrenAdministrator) === true);
            assert(permissions.hasPermission(Permission.LeaderAdministrator) === false);
            assert(permissions.hasPermission(Permission.ParentAdministrator) === false);
            assert(permissions.hasPermission(Permission.Financial) === false);
            assert(permissions.hasPermission(Permission.Reporter) === false);
            assert(permissions.hasPermission(Permission.Grouper) === false);
        });
        it("should only assign the LeaderAdministrator permission", () => {
            const permissions = Permissions.empty();
            permissions.addPermission(Permission.LeaderAdministrator);

            assert(permissions.hasPermission(Permission.ChildrenViewer) === false);
            assert(permissions.hasPermission(Permission.ChildrenAdministrator) === false);
            assert(permissions.hasPermission(Permission.LeaderAdministrator) === true);
            assert(permissions.hasPermission(Permission.ParentAdministrator) === false);
            assert(permissions.hasPermission(Permission.Financial) === false);
            assert(permissions.hasPermission(Permission.Reporter) === false);
            assert(permissions.hasPermission(Permission.Grouper) === false);
        });
        it("should only assign the ParentAdministrator permission", () => {
            const permissions = Permissions.empty();
            permissions.addPermission(Permission.ParentAdministrator);

            assert(permissions.hasPermission(Permission.ChildrenViewer) === false);
            assert(permissions.hasPermission(Permission.ChildrenAdministrator) === false);
            assert(permissions.hasPermission(Permission.LeaderAdministrator) === false);
            assert(permissions.hasPermission(Permission.ParentAdministrator) === true);
            assert(permissions.hasPermission(Permission.Financial) === false);
            assert(permissions.hasPermission(Permission.Reporter) === false);
            assert(permissions.hasPermission(Permission.Grouper) === false);
        });
        it("should only assign the Financial permission", () => {
            const permissions = Permissions.empty();
            permissions.addPermission(Permission.Financial);

            assert(permissions.hasPermission(Permission.ChildrenViewer) === false);
            assert(permissions.hasPermission(Permission.ChildrenAdministrator) === false);
            assert(permissions.hasPermission(Permission.LeaderAdministrator) === false);
            assert(permissions.hasPermission(Permission.ParentAdministrator) === false);
            assert(permissions.hasPermission(Permission.Financial) === true);
            assert(permissions.hasPermission(Permission.Reporter) === false);
            assert(permissions.hasPermission(Permission.Grouper) === false);
        });
        it("should only assign the Reporter permission", () => {
            const permissions = Permissions.empty();
            permissions.addPermission(Permission.Reporter);

            assert(permissions.hasPermission(Permission.ChildrenViewer) === false);
            assert(permissions.hasPermission(Permission.ChildrenAdministrator) === false);
            assert(permissions.hasPermission(Permission.LeaderAdministrator) === false);
            assert(permissions.hasPermission(Permission.ParentAdministrator) === false);
            assert(permissions.hasPermission(Permission.Financial) === false);
            assert(permissions.hasPermission(Permission.Reporter) === true);
            assert(permissions.hasPermission(Permission.Grouper) === false);
        });
        it("should only assign the Grouper permission", () => {
            const permissions = Permissions.empty();
            permissions.addPermission(Permission.Grouper);

            assert(permissions.hasPermission(Permission.ChildrenViewer) === false);
            assert(permissions.hasPermission(Permission.ChildrenAdministrator) === false);
            assert(permissions.hasPermission(Permission.LeaderAdministrator) === false);
            assert(permissions.hasPermission(Permission.ParentAdministrator) === false);
            assert(permissions.hasPermission(Permission.Financial) === false);
            assert(permissions.hasPermission(Permission.Reporter) === false);
            assert(permissions.hasPermission(Permission.Grouper) === true);
        });
    });

    describe("#multiplePermissions", () => {
        it("should set some permissions", () => {
            const permissions = Permissions.empty();
            permissions.addPermission(Permission.ParentAdministrator);
            permissions.addPermission(Permission.Grouper);
            permissions.addPermission(Permission.ChildrenViewer);
            permissions.addPermission(Permission.ChildrenViewer);
            permissions.addPermission(Permission.ChildrenViewer);
            permissions.addPermission(Permission.ChildrenViewer);

            assert(permissions.hasPermission(Permission.ChildrenViewer) === true);
            assert(permissions.hasPermission(Permission.ChildrenAdministrator) === false);
            assert(permissions.hasPermission(Permission.LeaderAdministrator) === false);
            assert(permissions.hasPermission(Permission.ParentAdministrator) === true);
            assert(permissions.hasPermission(Permission.Financial) === false);
            assert(permissions.hasPermission(Permission.Reporter) === false);
            assert(permissions.hasPermission(Permission.Grouper) === true);
        });
    });

    describe("#multiplePermissions", () => {
        it("deletes some permissions", () => {
            const permissions = Permissions.fromInteger(126);
            permissions.deletePermission(Permission.Financial);
            permissions.deletePermission(Permission.LeaderAdministrator);
            permissions.deletePermission(Permission.ChildrenViewer);

            assert.deepEqual(permissions, Permissions.fromBitString("1101010"));
        });
    });

    describe("#PermissionsConvertions", () => {
        it("should read the correct permissions from an integer", () => {
            const permissions = Permissions.fromInteger(126);

            assert(permissions.hasPermission(Permission.ChildrenViewer) === false);
            assert(permissions.hasPermission(Permission.ChildrenAdministrator) === true);
            assert(permissions.hasPermission(Permission.LeaderAdministrator) === true);
            assert(permissions.hasPermission(Permission.ParentAdministrator) === true);
            assert(permissions.hasPermission(Permission.Financial) === true);
            assert(permissions.hasPermission(Permission.Reporter) === true);
            assert(permissions.hasPermission(Permission.Grouper) === true);
        });

        it("should read the correct permissions from a bitstring", () => {
            assert.deepStrictEqual(Permissions.fromInteger(126), Permissions.fromBitString("1111110"));
        });

        it("should return the correct bitstring.", () => {
            assert.deepStrictEqual(
                Permissions.fromInteger(126).toBitString(),
                "00000000000000000000000001111110");
        });
    });
});
