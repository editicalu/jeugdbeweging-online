import { Request, Response } from "express";
import AccountDao from "../database/AccountDao";
import Account from "./Account";

const EXPIRE_COOKIE_NAME: string = "session_expire";

export default class SessionManager {
    public static login(req: Request, res: Response, account: Account) {
        // A cookie which only makes the expiry date of the session available
        // to JS. This allows us to check, client-side, whether the user still
        // has a valid session.
        res.cookie(
            EXPIRE_COOKIE_NAME,
            new Date().getTime() + Number(process.env.SESSION_MAXAGE_MILLISECONDS),
            {
                domain: process.env.VUE_APP_DOMAIN_NAME,
                maxAge: Number(process.env.SESSION_MAXAGE_MILLISECONDS),
                secure: process.env.VUE_APP_SSL === "true",
            },
        );
        req.session!.loggedInId = account.getUid();
    }

    /**
     * Checks whether an Account is signed in. Returns that Account when logged in.
     * @param req The request to be checked on.
     */
    public static async isLoggedIn(req: Request): Promise<Account | null> {
        if (req.session!.loggedInId) {
            return await AccountDao.fromIdDatabase(req.session!.loggedInId);
        } else {
            return null;
        }
    }

    /**
     * Logs the user out.
     * @param req The request on which to logout.
     */
    public static logout(req: Request, res: Response) {
        res.clearCookie(EXPIRE_COOKIE_NAME);
        req.session!.destroy(() => null);
    }
}
