import assert = require("assert");
import { Permission } from "./Permission";

describe("Permission", () => {
    describe("#readAllPermissions", () => {
        it("should say that all permissions still have the same id", () => {
            // This test is to ensure no-one accidently changes the values, as this could break an
            // existing database.
            // Add new permissions to this test.
            assert(Permission.ChildrenViewer === 0);
            assert(Permission.ChildrenAdministrator === 1);
            assert(Permission.LeaderAdministrator === 2);
            assert(Permission.ParentAdministrator === 3);
            assert(Permission.Financial === 4);
            assert(Permission.Reporter === 5);
            assert(Permission.Grouper === 6);
            assert(Permission.EventAdministrator === 7);
            assert(Permission.LeaderViewer === 10);
            assert(Permission.Checklister === 11);
        });
    });
});
