import assert = require("assert");
import "mocha";
import Account from "./Account";

describe("Account", () => {
    describe("#validCredentials", () => {
        it("should return true, as the correct credentials were provided", () => {
            const USERNAME = "dummy@example.com";
            const TEST_PASSWORD = "rfjeuirfwelrn";
            const account: Account = Account.fromUnhashedPassword(USERNAME, TEST_PASSWORD, null, false);
            assert.equal(account.verifyPassword(TEST_PASSWORD), true);
            assert.equal(account.getPermissions().toHexString(), "0x00000000");
        });
    });

    describe("#invalidCredentials", () => {
        it("should return true, as the correct credentials were provided", () => {
            const USERNAME = "dummy@example.com";
            const TEST_PASSWORD = "rfjeuirfwelrn";
            const NOT_TEST_PASSWORD = "ffdhisdh";
            const account: Account = Account.fromUnhashedPassword(USERNAME, TEST_PASSWORD, null, false);
            assert.equal(account.verifyPassword(NOT_TEST_PASSWORD), false);
        });
    });

    describe("#ChangedCredentials", () => {
        it("tests whether the verify function works after a change", () => {
            const USERNAME = "dummy@example.com";
            const OLD_PASSWORD = "rfjeuirfwelrn";
            const NEW_PASSWORD = "fnjkalfhsdkjlfh";
            const account: Account = Account.fromUnhashedPassword(USERNAME, OLD_PASSWORD, null, false);
            assert.equal(account.verifyPassword(OLD_PASSWORD), true);
            assert.equal(account.verifyPassword(NEW_PASSWORD), false);

            account.setPassword(NEW_PASSWORD);
            assert.equal(account.verifyPassword(OLD_PASSWORD), false);
            assert.equal(account.verifyPassword(NEW_PASSWORD), true);
        });
    });
});
