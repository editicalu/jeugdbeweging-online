
export enum Permission {
    /**
     * Able to view the basic  details of every child.
     */
    ChildrenViewer = 0,

    /**
     * Able to edit all the data of every child.
     */
    ChildrenAdministrator = 1,

    /**
     * Is able to create new and manage accounts for leaders.
     */
    LeaderAdministrator = 2,

    /**
     * Is able to create and manage accounts of parents.
     */
    ParentAdministrator = 3,

    /**
     * Is able to create and manage invoices.
     */
    Financial = 4,

    /**
     * Is able to write reports.
     */
    Reporter = 5,

    /**
     * Is able to create and manage groups.
     */
    Grouper = 6,

    /*
     * Creates and manages events. They can confirm pre-registrations as well.
     */
    EventAdministrator = 7,

    /**
     * Can view leaders, but cannot edit them.
     */
    LeaderViewer = 10,

    /**
     * Allowed to manage checklists
     */
    Checklister = 11,
}
