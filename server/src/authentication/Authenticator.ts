import assert = require("assert");
import AccountDao from "../database/AccountDao";
import Account from "./Account";

/**
 * Returns whether the given credentials were valid.
 * @param email The email address to check on
 * @param password The password to check on
 */
export default async function login(email: string, password: string): Promise<Account | null> {
    assert(email, "email to login cannot be null");
    assert(password, "password to login cannot be null");

    const account: Account | null = await AccountDao.fromEmailDatabase(email);
    if (!account) {
        return null;
    }
    return account.verifyPassword(password) ? account : null;
}
