import { Express, Request, Response } from "express";
import Account from "../authentication/Account";
import { Permission } from "../authentication/Permission";
import SessionManager from "../authentication/SessionManager";
import ParentDao from "../database/ParentDao";
import { Child } from "../entities/Child";
import catchRouteErrors from "./catchRouteErrors";

export default function add_parent_routes(app: Express) {
    get_parent_children(app);
    get_parents_route(app);

    post_parent_child_relation(app);
}

function get_parent_children(app: Express) {
    app.get("/api/parent/get_children/:parentid", catchRouteErrors(get_children));

    async function get_children(req: Request, res: Response) {
        // Test whether the user is logged in. If logged in, the account should have the appropriate permissions.
        const signedInAccount: Account | null = await SessionManager.isLoggedIn(req);

        if (signedInAccount === null) {
            res.status(403);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }
        const requestedParent: number = req.params.parentid;

        if (!requestedParent) {
            res.status(404);
            res.json({ valid: false, error: "notFound" });
            return;
        }

        let children: Child[] | null = await ParentDao.getChildren(requestedParent);
        if (children === null) {
            children = [];
        }

        res.json({ valid: true, children });
        return;
    }
}

function get_parents_route(app: Express) {
    const PARENTS_REQUEST_LIMIT = 20;

    app.get("/api/parent/", catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const signedInAccount = await SessionManager.isLoggedIn(req);

        if (signedInAccount === null) {
            res.status(404);
            res.json({ valid: false, error: "Unauthorized." });
            return;
        }

        if (!signedInAccount.getPermissions().hasPermission(Permission.ParentAdministrator)) {
            res.status(404);
            res.json({ valid: false, error: "Unauthorized." });
            return;
        }

        const countPromise = ParentDao.countParents();

        let page = 0;
        if (req.query.page !== null) {
            page = Number(req.query.page);
            if (isNaN(page)) {
                page = 0;
            }
        }

        const result = await ParentDao.allParents(page, PARENTS_REQUEST_LIMIT);
        const amountOfParents = await countPromise;
        if (result === null || amountOfParents < 0) {
            res.status(500);
            res.json({ valid: false, error: "Internal server error." });
            return;
        }
        res.json({ amountOfParents, parents: result.map((personMin) => personMin.toObject()) });
    }
}

function post_parent_child_relation(app: Express) {
    app.post("/api/parent_child", catchRouteErrors(new_parent_child_relation));

    async function new_parent_child_relation(req: Request, res: Response) {
        const signedInAccount: Account | null = await SessionManager.isLoggedIn(req);

        if (signedInAccount === null) {
            res.status(403);
            res.json({valid: false, error: "unauthorized"});
            return;
        }

        const { parentId, childId } = req.body;

        try {
            if (!await ParentDao.addChild(parentId, childId)) {
                res.status(500);
                res.json({valid: false, error: "Internal server error"});
                return;
            }
            res.status(200);
            res.json({valid: true});
            return;
        } catch (e) {
            console.error(e);
            res.status(400);
            res.json({valid: false, error: "Bad request"});
            return;
        }
    }
}
