// Read .env file
import path = require("path");

import dotenv = require("dotenv");

export default function process_configuration() {
    const result = dotenv.config({ path: path.resolve(process.cwd(), ".env") });
    if (result.error) {
        console.error("No configuration found");
    } else {
        if (process.env.NODE_ENV !== "production") {
            console.log("Configuration:");
            console.log(result.parsed);
        }
    }
}
