import { Express, Request, Response } from "express";
import Account from "../authentication/Account";
import { Permission } from "../authentication/Permission";
import Permissions from "../authentication/Permissions";
import SessionManager from "../authentication/SessionManager";
import AccountDao from "../database/AccountDao";
import AccountDPL from "../dataPermissionLayer/AccountDPL";
import catchRouteErrors from "./catchRouteErrors";

/**
 * Adds all routes around authentication to the server
 * @param app The webserver object
 */
export default function add_account_routes(app: Express) {
    post_change_password_route(app);
    post_change_email_route(app);
    post_change_permissions_route(app);
    delete_account_route(app);
    get_account(app);
}

/**
 * See https://docs.google.com/document/d/1JPYX1FjX-HTlJuP4PqMmAXKNeCpdvuoZQlw2wcAwsLY for API documentation
 */
function get_account(app: Express) {
    app.get("/api/account/:id(\\d+)/", catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const result = await AccountDPL.viewId(req, Number(req.params.id));
        if (result.isCorrect()) {
            res.json(result.getContent()!.toObject());
        } else {
            res.status(result.getErrorCode());
            res.json({ error: result.getErrorMessage() });
        }
    }
}

/**
 * See https://docs.google.com/document/d/1JPYX1FjX-HTlJuP4PqMmAXKNeCpdvuoZQlw2wcAwsLY for API documentation
 */
function post_change_password_route(app: Express) {
    app.post("/api/account/change_password/", catchRouteErrors(post_route));

    async function post_route(req: Request, res: Response) {
        // Safely get credentials from request body
        function getPassword(): string[] | null {
            try {
                const { originalPassword, password } = req.body;
                return (originalPassword === null) || (password === null) ? null : [originalPassword, password];
            } catch (e) {
                console.error(e);
                return null;
            }
        }

        const currentAccount: Account | null = await SessionManager.isLoggedIn(req);
        if (!currentAccount) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        const uid = currentAccount.getUid()!;

        const passwords = getPassword();
        if (passwords === null) {
            res.status(400);
            res.json({ valid: false, error: "invalidParams" });
            return;
        }
        const pwd = passwords[1];
        const oriPwd = passwords[0];

        if (!currentAccount.verifyPassword(oriPwd)) {
            res.status(403);
            res.json({ valid: false, error: "WrongPassword" });
            return;
        }

        if (pwd === null) {
            res.status(400);
            res.json({ valid: false, error: "invalidParams" });
            return;
        } else {
            const hashedPassword = Account.hashPassword(pwd);
            const valid: boolean = await AccountDao.updatePassword(uid, hashedPassword);
            if (valid) {
                res.json({ valid });
            } else {
                res.status(500);
                res.json({ valid: false, error: "Internal Server Error" });
            }
            return;
        }
    }
}

/**
 * See https://docs.google.com/document/d/1JPYX1FjX-HTlJuP4PqMmAXKNeCpdvuoZQlw2wcAwsLY for API documentation
 */
function post_change_email_route(app: Express) {
    app.post("/api/account/:accountid/change_email/", catchRouteErrors(post_route));

    async function post_route(req: Request, res: Response) {
        // Safely get credentials from request body
        const email = req.body.email;
        if (!email) {
            res.status(400);
            res.json({ valid: false, error: "invalidParams" });
            return;
        }

        const currentAccount: Account | null = await SessionManager.isLoggedIn(req);
        if (!currentAccount) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        const uid = req.params.accountid!;

        if (!(Number(currentAccount.getUid()) === uid
            || currentAccount.getPermissions().hasPermission(Permission.LeaderAdministrator))) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        const valid: boolean = await AccountDao.updateEmail(uid, email);
        if (valid) {
            res.json({ valid });
        } else {
            res.status(500);
            res.json({ valid: false, error: "Internal Server Error" });
        }
        return;
    }
}

/**
 * See https://docs.google.com/document/d/1JPYX1FjX-HTlJuP4PqMmAXKNeCpdvuoZQlw2wcAwsLY for API documentation
 */
function post_change_permissions_route(app: Express) {
    app.post("/api/account/:personid/change_permissions/", catchRouteErrors(post_route));

    async function post_route(req: Request, res: Response) {
        const reqId = Number(req.params.personid);
        if (reqId === null) {
            res.status(400);
            res.json({ valid: false, error: "syntaxError" });
            return;
        }

        // Safely get permissions from request body
        function getPermissions(): Permission[] | null {
            try {
                const { permissions } = req.body;
                return (permissions === null) ? null : permissions;
            } catch (e) {
                console.error(e);
                return null;
            }
        }

        const currentAccount: Account | null = await SessionManager.isLoggedIn(req);
        if (!currentAccount) {
            res.status(403);
            res.json({ valid: false, error: "Not logged in" });
            return;
        }

        // is logged in user allowed to change permissions?
        const currentUserPermissions: Permissions = currentAccount!.getPermissions();
        if (currentUserPermissions.hasPermission(Permission.LeaderAdministrator)) {
            // ok
        } else {
            res.status(403);
            res.json({ valid: false, error: "Not enough permissions" });
            return;
        }

        const userAccount: Account | null = await AccountDao.fromIdDatabase(reqId);
        if (!userAccount) {
            res.status(404);
            res.json({ valid: false, error: "Not Found" });
            return;
        }

        const newPermissions: Permission[] | null = getPermissions();
        const newPermissionsObject: Permissions = Permissions.empty();
        if (newPermissions) {
            for (const perm of newPermissions) {
                newPermissionsObject.addPermission(perm);
            }
        }
        userAccount.setPermissions(newPermissionsObject);

        await AccountDao.saveToDatabase(userAccount);

        res.json({ valid: true });
    }
}

function delete_account_route(app: Express) {
    app.delete("/api/account/:id(\\d)+/", catchRouteErrors(delete_route));

    async function delete_route(req: Request, res: Response) {
        const result = await AccountDPL.deleteFromId(req, Number(req.params.id));

        if (result.isCorrect()) {
            res.json({ valid: result.getContent() });
        } else {
            res.status(result.getErrorCode());
            res.json({ valid: false, error: result.getErrorMessage() });
        }
    }
}
