import { Express, Request, Response } from "express";
import { Permission } from "../../authentication/Permission";
import SessionManager from "../../authentication/SessionManager";
import AccountDao from "../../database/AccountDao";
import GroupingsDao from "../../database/GroupingsDao";
import { LeaderDao } from "../../database/LeaderDao";
import ParentDao from "../../database/ParentDao";
import Grouping from "../../entities/Grouping";
import catchRouteErrors from "../catchRouteErrors";

type IGroupingConstructor<T extends Grouping> = new (id: number, title: string, description: string) => T;

interface IRespBuilder { [key: string]: any; }

/*
 * The `groupings` routes are used to query lists of groupings.
 * For management of a specific grouping, see the `grouping` routes.
 */
export default class GroupingsRouter<T extends Grouping> {
    protected grouping: IGroupingConstructor<T>;
    protected groupingName: string;
    protected groupingNameCapitalised: string;
    protected groupingApiUrl: string;
    protected groupingPermission: Permission;
    protected limit: number = 20;

    constructor(
        grouping: IGroupingConstructor<T>,
        groupingPermission: Permission,
    ) {
        this.grouping = grouping;
        this.groupingName = grouping.name.toLowerCase();
        this.groupingNameCapitalised = grouping.name;
        this.groupingApiUrl = `/api/${this.groupingName}s/`;
        this.groupingPermission = groupingPermission;
    }

    public addRoutes(this: GroupingsRouter<T>, app: Express) {
        this.getRoute(app);
    }

    protected getRoute(app: Express) {
        const self = this;

        // Optional query parameter:
        // -> page: number = 0
        app.get(`${this.groupingApiUrl}:id?`, catchRouteErrors(getRoute));

        async function getRoute(req: Request, res: Response) {
            const loggedInUserP = SessionManager.isLoggedIn(req);
            const requestedPerson: number | undefined = req.params.id;
            const page: number = req.query.page || 0;

            const loggedInUser = await loggedInUserP;
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            }

            const GsDao = new GroupingsDao(self.grouping);

            const haveManagementPermission =
                loggedInUser
                    .getPermissions()
                    .hasPermission(self.groupingPermission);

            if (requestedPerson) {
                // The user either has a power permission,
                // or they share a grouping with the requested person,
                // or they are the parent of the person,
                // or they ARE the person.
                const allowedToRequestThisPerson =
                    haveManagementPermission
                    || (
                        await Promise.all([
                            GsDao.getSharedGroupings(
                                requestedPerson, loggedInUser.getUid() || 0),
                            ParentDao.isParentOf(requestedPerson, loggedInUser.getUid() || 0),
                            AccountDao.leaderIdFrom(loggedInUser.getUid() || 0),
                        ])
                    ).some((b: number[] | boolean | number | null) => b !== null);

                if (allowedToRequestThisPerson
                ) {
                    const [amtGroupings, groupings] = await Promise.all([
                        GsDao.countFromPersonIdDatabase(requestedPerson),
                        GsDao.fromPersonIdDatabase(requestedPerson, self.limit, page),
                    ]);

                    if (amtGroupings === null
                        || groupings === null) {
                        res.status(500);
                        res.json({ error: "internalServerError" });
                        return;
                    }
                    res.json(self.buildResponse(amtGroupings, groupings));
                    return;
                } else {
                    // If a person was requested but the logged in user does not
                    // have permissions, return an error.
                    res.status(404);
                    res.json({ error: "forbidden" });
                    return;
                }
            }

            // No person id specified means we just want to see all groupings
            // which we are allowed to see.
            if (haveManagementPermission) {
                const [amtGroupings, groupings] = await Promise.all([
                    GsDao.countAllDatabase(),
                    GsDao.allDatabase(self.limit, page),
                ]);

                if (amtGroupings === null
                    || groupings === null) {
                    res.status(500);
                    res.json({ error: "internalServerError" });
                    return;
                }
                res.json(self.buildResponse(amtGroupings, groupings));
                return;
            }

            // If the user does not have special permissions, they're only
            // allowed to see groupings which they are part of. However, only
            // Leaders can have a Person object, and thus groupings linked
            // to them.
            const leaderId = await LeaderDao
                .translateAccountToPersonID(loggedInUser.getUid() || 0);
            if (leaderId) {
                const [amtGroupings, groupings] = await Promise.all([
                    GsDao.countFromPersonIdDatabase(leaderId),
                    GsDao.fromPersonIdDatabase(leaderId, self.limit, page),
                ]);

                if (amtGroupings === null
                    || groupings === null) {
                    res.status(500);
                    res.json({ error: "internalServerError" });
                    return;
                }
                res.json(self.buildResponse(amtGroupings, groupings));
                return;
            }

            // Parents are not allowed to see any groupings as it is
            // protected info.
            res.json(self.buildResponse(0, []));
            return;
        }
    }

    protected buildResponse(amtGroupings: number, groupings: T[]) {
        const respObj: IRespBuilder = {};
        respObj["amt" + this.groupingNameCapitalised + "s"] =
            amtGroupings;
        respObj[this.groupingName + "s"] = groupings;
        return respObj;
    }
}
