import { Express, Request, Response } from "express";
import { Permission } from "../../authentication/Permission";
import SessionManager from "../../authentication/SessionManager";
import AccountDao from "../../database/AccountDao";
import GroupingDao from "../../database/GroupingDao";
import GroupingMemberDao from "../../database/GroupingMemberDao";
import Grouping from "../../entities/Grouping";
import { PersonMin } from "../../entities/Person";
import catchRouteErrors from "../catchRouteErrors";

type IGroupingConstructor<T extends Grouping> = new (id: number, title: string, description: string) => T;

interface IRespBuilder {
    amtPersons: number;
    persons: PersonMin[];
    [key: string]: any;
}

/*
 * The `grouping` routes are used to manage a specific grouping.
 * For enumerating groupings, see the `groupings` routes.
 */
export default class GroupingRouter<T extends Grouping> {
    protected grouping: IGroupingConstructor<T>;
    protected groupingName: string;
    protected groupingApiUrl: string;
    protected groupingPermission: Permission;
    protected limit: number = 20;

    constructor(
        grouping: IGroupingConstructor<T>,
        groupingPermission: Permission,
    ) {
        this.grouping = grouping;
        this.groupingName = grouping.name.toLowerCase();
        this.groupingApiUrl = `/api/${this.groupingName}/`;
        this.groupingPermission = groupingPermission;
    }

    public addRoutes(this: GroupingRouter<T>, app: Express) {
        this.getRoute(app);
        this.putRoute(app);
        this.postRoute(app);
        this.deleteRoute(app);
    }

    protected deleteRoute(app: Express) {
        const self = this;
        app.delete(`${this.groupingApiUrl}:id`, catchRouteErrors(deleteRoute));

        async function deleteRoute(req: Request, res: Response) {

            const loggedInUser = await SessionManager.isLoggedIn(req);
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            } else {
                const haveManagementPermission =
                    loggedInUser
                        .getPermissions()
                        .hasPermission(self.groupingPermission);
                if (!haveManagementPermission) {
                    res.status(404);
                    res.json({ error: "forbidden" });
                    return;
                }
            }

            const requestedGrouping: number | undefined = req.params.id;
            if (!requestedGrouping) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            const deleted =
                await new GroupingDao(self.grouping).deleteFromDatabase(requestedGrouping);
            if (!deleted) {
                res.status(500);
                res.json({ error: "internalServerError" });
                return;
            }

            res.json({ id: requestedGrouping });
            return;
        }
    }

    protected putRoute(app: Express) {
        const self = this;
        app.put(`${this.groupingApiUrl}:id`, catchRouteErrors(putRoute));

        async function putRoute(req: Request, res: Response) {
            const loggedInUser = await SessionManager.isLoggedIn(req);
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            } else {
                const haveManagementPermission =
                    loggedInUser
                        .getPermissions()
                        .hasPermission(self.groupingPermission);
                if (!haveManagementPermission) {
                    res.status(404);
                    res.json({ error: "forbidden" });
                    return;
                }
            }

            const { title, description } = req.body;
            if (!title) {
                res.status(400);
                res.json({ error: "syntaxError" });
                return;
            }

            const reqId: number | undefined = req.params.id;
            if (!reqId) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            const updateSucceeded =
                await new GroupingDao(self.grouping).updateInDatabase(reqId, title, description || "");
            if (!updateSucceeded) {
                res.status(500);
                res.json({ error: "internalServerError" });
                return;
            }

            res.json({ updateSucceeded });
            return;
        }
    }

    protected postRoute(app: Express) {
        const self = this;
        app.post(`${this.groupingApiUrl}`, catchRouteErrors(postRoute));

        async function postRoute(req: Request, res: Response) {
            const loggedInUser = await SessionManager.isLoggedIn(req);
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            } else {
                const haveManagementPermission =
                    loggedInUser
                        .getPermissions()
                        .hasPermission(self.groupingPermission);
                if (!haveManagementPermission) {
                    res.status(404);
                    res.json({ error: "forbidden" });
                    return;
                }
            }

            const personIdP =
                AccountDao.leaderIdFrom(loggedInUser.getUid() || 0);

            const { title, description } = req.body;
            if (!title) {
                res.status(400);
                res.json({ error: "syntaxError" });
                return;
            }

            const id = await new GroupingDao(self.grouping).insertIntoDatabase(title, description || "");
            if (!id) {
                res.status(500);
                res.json({ error: "internalServerError" });
                return;
            }

            if (!(await new GroupingMemberDao(self.grouping)
                .addMember(id, await personIdP || 0)
            )) {
                res.status(500);
                res.json({ error: "internalServerError" });
                return;
            }

            res.json({ id });
            return;
        }
    }

    protected getRoute(app: Express) {
        const self = this;
        // Optional query parameter:
        // -> page: number = 0
        app.get(`${this.groupingApiUrl}:id`, catchRouteErrors(getRoute));

        async function getRoute(req: Request, res: Response) {
            const loggedInUserP = SessionManager.isLoggedIn(req);
            const requestedGrouping: number | undefined = req.params.id;
            const page: number = req.query.page || 0;

            if (!requestedGrouping) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            const loggedInUser = await loggedInUserP;
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            }

            const GDao = new GroupingDao(self.grouping);

            const haveManagementPermission =
                loggedInUser
                    .getPermissions()
                    .hasPermission(self.groupingPermission);
            const leaderInGrouping =
                haveManagementPermission
                || await GDao.leaderInGrouping(
                    loggedInUser.getUid() || 0,
                    requestedGrouping,
                );

            const GMemberDao = new GroupingMemberDao(self.grouping);
            if (leaderInGrouping) {
                const [grouping, amtPersons, persons] = await Promise.all([
                    GDao.fromIdDatabase(requestedGrouping),
                    GMemberDao.countFromGIdDatabase(requestedGrouping),
                    GMemberDao.allMinFromGIdDatabase(requestedGrouping, self.limit, page),
                ]);

                if (grouping === null
                    || amtPersons === null
                    || persons === null) {
                    res.status(500);
                    res.json({ error: "internalServerError" });
                    return;
                }

                const respObj: IRespBuilder = { amtPersons, persons };
                respObj[self.groupingName] = grouping;

                res.json(respObj);
                return;
            } else {
                const [possibleChildren, grouping] = await Promise.all([
                    GMemberDao.containsChildOf(
                        requestedGrouping,
                        loggedInUser.getUid() || 0,
                    ),
                    GDao.fromIdDatabase(requestedGrouping),
                ]);
                if (possibleChildren !== null && grouping !== null) {
                    const respObj: IRespBuilder = {
                        amtPersons: possibleChildren.length,
                        persons: possibleChildren,
                    };
                    respObj[self.groupingName] = grouping;

                    res.json(respObj);
                    return;
                }
            }

            res.status(404);
            res.json({ error: "forbidden" });
            return;
        }
    }
}
