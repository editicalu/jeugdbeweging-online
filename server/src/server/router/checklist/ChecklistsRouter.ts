import { Permission } from "../../../authentication/Permission";
import Checklist from "../../../entities/Checklist";
import GroupingsRouter from "../GroupingsRouter";

export default class ChecklistsRouter extends GroupingsRouter<Checklist> {
    constructor() {
        super(Checklist, Permission.Checklister);
    }
}
