import { Express, Request, Response } from "express";
import { Permission } from "../../../authentication/Permission";
import SessionManager from "../../../authentication/SessionManager";
import ChecklistDao from "../../../database/checklist/ChecklistDao";
import ChecklistMemberDao from "../../../database/checklist/ChecklistMemberDao";
import Checklist from "../../../entities/Checklist";
import catchRouteErrors from "../../catchRouteErrors";
import GroupingRouter from "../GroupingRouter";

export default class ChecklistRouter extends GroupingRouter<Checklist> {
    constructor() {
        super(Checklist, Permission.Checklister);
    }

    protected getRoute(app: Express) {
        const self = this;
        // Optional query parameter:
        // -> page: number = 0
        app.get(`${this.groupingApiUrl}:id`, catchRouteErrors(getRoute));

        async function getRoute(req: Request, res: Response) {
            const loggedInUserP = SessionManager.isLoggedIn(req);
            const requestedGrouping: number | undefined = req.params.id;
            const page: number = req.query.page || 0;

            if (!requestedGrouping) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            const loggedInUser = await loggedInUserP;
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            }

            const GDao = new ChecklistDao();

            const haveManagementPermission =
                loggedInUser
                    .getPermissions()
                    .hasPermission(self.groupingPermission);
            const leaderInGrouping =
                haveManagementPermission
                || await GDao.leaderInGrouping(
                    loggedInUser.getUid() || 0,
                    requestedGrouping,
                );

            if (leaderInGrouping) {
                const GMemberDao = new ChecklistMemberDao();
                const [grouping, amtPersons, persons] = await Promise.all([
                    GDao.fromIdDatabase(requestedGrouping),
                    GMemberDao.countFromGIdDatabase(requestedGrouping),
                    GMemberDao.allMinCheckedFromGIdDatabase(requestedGrouping, self.limit, page),
                ]);

                if (grouping === null
                    || amtPersons === null
                    || persons === null) {
                    res.status(500);
                    res.json({ error: "internalServerError" });
                    return;
                }

                interface IRespBuilder {
                    amtPersons: number;
                    persons: Array<{
                        id: number,
                        checked: boolean,
                        firstName: string,
                        lastName: string,
                    }>;
                    [key: string]: any;
                }
                const respObj: IRespBuilder = { amtPersons, persons };
                respObj[self.groupingName] = grouping;

                res.json(respObj);
                return;
            }

            res.status(404);
            res.json({ error: "forbidden" });
            return;
        }
    }
}
