import { Express, Request, Response } from "express";
import { Permission } from "../../../authentication/Permission";
import SessionManager from "../../../authentication/SessionManager";
import ChecklistDao from "../../../database/checklist/ChecklistDao";
import ChecklistMemberDao from "../../../database/checklist/ChecklistMemberDao";
import Activity from "../../../entities/Activity";
import Checklist from "../../../entities/Checklist";
import Group from "../../../entities/Group";
import catchRouteErrors from "../../catchRouteErrors";
import GroupingMemberRouter from "../GroupingMemberRouter";

export default class ChecklistMemberRouter
    extends GroupingMemberRouter<Checklist> {
    constructor() {
        super(
            Checklist,
            Permission.Checklister,
            [
                [Group, Permission.Grouper],
                [Activity, Permission.EventAdministrator],
            ]);
    }

    public addRoutes(this: ChecklistMemberRouter, app: Express) {
        this.deleteMemberRoute(app);
        this.addMemberRoute(app);
        this.checkMemberRoute(app);

        for (const [addedGrouping, addedGroupingPerm] of this.addableGroupings) {
            this.addGroupingMembersRoute(app, addedGrouping, addedGroupingPerm);
        }
    }

    protected checkMemberRoute(app: Express) {
        const self = this;
        app.put(`${this.groupingApiUrl}:groupingId/:memberId`, catchRouteErrors(putRoute));

        async function putRoute(req: Request, res: Response) {
            const loggedInUser = await SessionManager.isLoggedIn(req);
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            }

            const requestedGrouping: number | undefined = req.params.groupingId;
            const requestedPerson: number | undefined = req.params.memberId;
            if (!requestedGrouping || !requestedPerson) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            const { checked } = req.body;
            if (checked === null) {
                res.status(400);
                res.json({ error: "syntaxError" });
                return;
            }

            const haveManagementPermission =
                loggedInUser
                    .getPermissions()
                    .hasPermission(self.groupingPermission);

            const loggedInUserUid: number | null = loggedInUser.getUid();
            if (!haveManagementPermission) {
                if (loggedInUserUid !== null) {
                    const allowed: boolean | null =
                        await new ChecklistDao().leaderInGrouping(loggedInUserUid, requestedGrouping);
                    if (!allowed) {
                        res.status(404);
                        res.json({ error: "forbidden" });
                        return;
                    }
                } else {
                    res.status(404);
                    res.json({ error: "unauthorized" });
                    return;
                }
            }

            const success =
                await new ChecklistMemberDao()
                    .checkMember(requestedGrouping, requestedPerson, checked);

            if (!success) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            res.status(200);
            res.json({ success });
        }
    }
}
