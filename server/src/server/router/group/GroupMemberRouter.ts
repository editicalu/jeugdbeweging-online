import { Permission } from "../../../authentication/Permission";
import Group from "../../../entities/Group";
import GroupingMemberRouter from "../GroupingMemberRouter";

export default class GroupMemberRouter extends GroupingMemberRouter<Group> {
    constructor() {
        super(Group, Permission.Grouper);
    }
}
