import { Permission } from "../../../authentication/Permission";
import Group from "../../../entities/Group";
import GroupingRouter from "../GroupingRouter";

export default class GroupRouter extends GroupingRouter<Group> {
    constructor() {
        super(Group, Permission.Grouper);
    }
}
