import { Permission } from "../../../authentication/Permission";
import Group from "../../../entities/Group";
import GroupingsRouter from "../GroupingsRouter";

export default class GroupsRouter extends GroupingsRouter<Group> {
    constructor() {
        super(Group, Permission.Grouper);
    }
}
