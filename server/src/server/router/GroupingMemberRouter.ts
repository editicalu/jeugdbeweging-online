import { Express, Request, Response } from "express";
import { Permission } from "../../authentication/Permission";
import SessionManager from "../../authentication/SessionManager";
import GroupingDao from "../../database/GroupingDao";
import GroupingMemberDao from "../../database/GroupingMemberDao";
import Grouping from "../../entities/Grouping";
import { PersonMin } from "../../entities/Person";
import "../../lib/ArrayExtend";
import catchRouteErrors from "../catchRouteErrors";

type IGroupingConstructor<T extends Grouping> = new (id: number, title: string, description: string) => T;

/*
 * The `groupingmember` routes are used to manage members of a grouping.
 */
export default class GroupingMemberRouter<T extends Grouping> {
    protected grouping: IGroupingConstructor<T>;
    protected groupingName: string;
    protected groupingApiUrl: string;
    protected groupingPermission: Permission;
    protected addableGroupings: Array<[IGroupingConstructor<T>, Permission]>;

    constructor(
        grouping: IGroupingConstructor<T>,
        groupingPermission: Permission,
        addableGroupings: Array<[IGroupingConstructor<T>, Permission]> = [],
    ) {
        this.grouping = grouping;
        this.groupingName = grouping.name.toLowerCase();
        this.groupingApiUrl = `/api/${this.groupingName}_member/`;
        this.groupingPermission = groupingPermission;
        this.addableGroupings = addableGroupings;
    }

    public addRoutes(this: GroupingMemberRouter<T>, app: Express) {
        this.deleteMemberRoute(app);
        this.addMemberRoute(app);

        for (const [addedGrouping, addedGroupingPerm] of this.addableGroupings) {
            this.addGroupingMembersRoute(app, addedGrouping, addedGroupingPerm);
        }
    }

    protected addGroupingMembersRoute(
        app: Express,
        addedGrouping: IGroupingConstructor<T>,
        addedGroupingPerm: Permission,
    ) {
        const self = this;
        const agName = addedGrouping.name.toLowerCase();
        app.options(
            `${this.groupingApiUrl}:groupingId/${agName}/:addedGroupingId`,
            (req, res, next) => next(),
        );
        app.post(
            `${this.groupingApiUrl}:groupingId/${agName}/:addedGroupingId`, catchRouteErrors(addRoute),
        );

        async function addRoute(req: Request, res: Response) {
            const loggedInUser = await SessionManager.isLoggedIn(req);
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            }

            const requestedGroupingId: number | undefined =
                req.params.groupingId;
            const addedGroupingId: number | undefined =
                req.params.addedGroupingId;

            if (!requestedGroupingId || !addedGroupingId) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            const loggedInUserUid: number | null = loggedInUser.getUid();
            if (loggedInUserUid === null) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            }

            // Test whether user is allowed to add members of the added grouping
            // to the base-grouping.
            const haveAddedGroupingPermission =
                loggedInUser
                    .getPermissions()
                    .hasPermission(addedGroupingPerm);
            if (!haveAddedGroupingPermission) {
                const allowed: boolean | null =
                    await new GroupingDao(addedGrouping)
                        .leaderInGrouping(
                            loggedInUserUid,
                            addedGroupingId,
                        );
                if (!allowed) {
                    res.status(404);
                    res.json({ error: "forbidden" });
                    return;
                }
            }

            // Test whether user is allowed to add to the grouping
            const haveManagementPermission =
                loggedInUser
                    .getPermissions()
                    .hasPermission(self.groupingPermission);
            if (!haveManagementPermission) {
                const allowed: boolean | null =
                    await new GroupingDao(self.grouping)
                        .leaderInGrouping(
                            loggedInUserUid,
                            requestedGroupingId,
                        );
                if (!allowed) {
                    res.status(404);
                    res.json({ error: "forbidden" });
                    return;
                }
            }

            const baseGMemberDao = new GroupingMemberDao(self.grouping);
            const addedGMemberDao = new GroupingMemberDao(addedGrouping);

            const membersToBeAdded = await addedGMemberDao
                .allMinFromGIdDatabase(addedGroupingId) || [];
            const membersAdded = await Promise.all(
                membersToBeAdded.map((p) =>
                    baseGMemberDao.addMember(requestedGroupingId, p.id || 0),
                ),
            );

            // Every single addition of a member has failed.
            if (JSON.stringify(membersAdded) !== JSON.stringify([])
                && membersAdded.every((r) => r === null)
            ) {
                res.status(500);
                res.json({
                    error: "internalServerError",
                });
                return;
            }

            const notAddedIds =
                membersToBeAdded
                    .zip(membersAdded)
                    .filter((m) => m[1] === null)
                    .map((m) => m[0].id);

            res.status(200);
            res.json({ notAddedIds });
        }
    }

    protected deleteMemberRoute(app: Express) {
        const self = this;
        app.options(
            `${this.groupingApiUrl}:groupingId/:memberId`,
            (req, res, next) => next(),
        );
        app.delete(`${this.groupingApiUrl}:groupingId/:memberId`, catchRouteErrors(deleteRoute));

        async function deleteRoute(req: Request, res: Response) {
            const loggedInUser = await SessionManager.isLoggedIn(req);
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            }

            const requestedGrouping: number | undefined = req.params.groupingId;
            const requestedPerson: number | undefined = req.params.memberId;

            if (!requestedGrouping || !requestedPerson) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            const haveManagementPermission =
                loggedInUser
                    .getPermissions()
                    .hasPermission(self.groupingPermission);

            const loggedInUserUid: number | null = loggedInUser.getUid();
            if (!haveManagementPermission) {
                if (loggedInUserUid !== null) {
                    const allowed: boolean | null =
                        await new GroupingDao(self.grouping).leaderInGrouping(loggedInUserUid, requestedGrouping);
                    if (!allowed) {
                        res.status(404);
                        res.json({ error: "forbidden" });
                        return;
                    }
                } else {
                    res.status(404);
                    res.json({ error: "unauthorized" });
                    return;
                }
            }

            const deleted =
                await new GroupingMemberDao(self.grouping)
                    .deleteMember(requestedGrouping, requestedPerson);

            if (!deleted) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            res.json({ success: true });
        }
    }

    protected addMemberRoute(app: Express) {
        const self = this;
        app.post(`${this.groupingApiUrl}:groupingId/:memberId`, catchRouteErrors(postRoute));

        async function postRoute(req: Request, res: Response) {
            const loggedInUser = await SessionManager.isLoggedIn(req);
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            }

            const requestedGrouping: number | undefined = req.params.groupingId;
            const requestedPerson: number | undefined = req.params.memberId;

            if (!requestedGrouping || !requestedPerson) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            const haveManagementPermission =
                loggedInUser
                    .getPermissions()
                    .hasPermission(self.groupingPermission);

            const loggedInUserUid: number | null = loggedInUser.getUid();
            if (!haveManagementPermission) {
                if (loggedInUserUid !== null) {
                    const allowed: boolean | null =
                        await new GroupingDao(self.grouping).leaderInGrouping(loggedInUserUid, requestedGrouping);
                    if (!allowed) {
                        res.status(404);
                        res.json({ error: "forbidden" });
                        return;
                    }
                } else {
                    res.status(404);
                    res.json({ error: "unauthorized" });
                    return;
                }
            }

            const GMemberDao = new GroupingMemberDao(self.grouping);

            const added = await GMemberDao
                .addMember(requestedGrouping, requestedPerson);
            if (added === false) {
                res.status(400);
                res.json({ error: "alreadyAdded" });
                return;
            } else if (added === null) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            res.json({ success: true });
        }
    }
}
