import { Express, Request, Response } from "express";
import { Permission } from "../../../authentication/Permission";
import SessionManager from "../../../authentication/SessionManager";
import ActivityDao from "../../../database/activity/ActivityDao";
import Activity from "../../../entities/Activity";
import catchRouteErrors from "../../catchRouteErrors";
import GroupingRouter from "../GroupingRouter";

export default class ActivityRouter extends GroupingRouter<Activity> {
    constructor() {
        super(Activity, Permission.EventAdministrator);
    }

    protected postRoute(app: Express) {
        const self = this;
        app.post(`${this.groupingApiUrl}`, catchRouteErrors(postRoute));

        async function postRoute(req: Request, res: Response) {
            const loggedInUser = await SessionManager.isLoggedIn(req);
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            } else {
                const haveManagementPermission =
                    loggedInUser
                        .getPermissions()
                        .hasPermission(self.groupingPermission);
                if (!haveManagementPermission) {
                    res.status(404);
                    res.json({ error: "forbidden" });
                    return;
                }
            }

            const { title, description, preRegister, startDate, stopDate } = req.body;
            if (!title || !startDate || !stopDate) {
                res.status(400);
                res.json({ error: "syntaxError" });
                return;
            }

            const id = await new ActivityDao()
                .insertActivityIntoDatabase(
                    title,
                    description || "",
                    preRegister,
                    startDate,
                    stopDate,
                );
            if (!id) {
                res.status(500);
                res.json({ error: "internalServerError" });
                return;
            }

            res.json({ id });
            return;
        }
    }

    protected putRoute(app: Express) {
        const self = this;
        app.put(`${this.groupingApiUrl}:id`, catchRouteErrors(putRoute));

        async function putRoute(req: Request, res: Response) {
            const loggedInUser = await SessionManager.isLoggedIn(req);
            if (!loggedInUser) {
                res.status(404);
                res.json({ error: "unauthorized" });
                return;
            } else {
                const haveManagementPermission =
                    loggedInUser
                        .getPermissions()
                        .hasPermission(self.groupingPermission);
                if (!haveManagementPermission) {
                    res.status(404);
                    res.json({ error: "forbidden" });
                    return;
                }
            }

            const { title, description, preRegister, startDate, stopDate } = req.body;
            if (!title || !startDate || !stopDate) {
                res.status(400);
                res.json({ error: "syntaxError" });
                return;
            }

            const reqId: number | undefined = req.params.id;
            if (!reqId) {
                res.status(404);
                res.json({ error: "notFound" });
                return;
            }

            const updateSucceeded = await new ActivityDao()
                .updateActivityInDatabase(
                    reqId,
                    title,
                    description || "",
                    preRegister,
                    startDate,
                    stopDate,
                );
            if (!updateSucceeded) {
                res.status(500);
                res.json({ error: "internalServerError" });
                return;
            }

            res.json({ updateSucceeded });
            return;
        }
    }
}
