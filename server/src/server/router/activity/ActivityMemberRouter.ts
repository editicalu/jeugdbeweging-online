import { Permission } from "../../../authentication/Permission";
import Activity from "../../../entities/Activity";
import Group from "../../../entities/Group";
import GroupingMemberRouter from "../GroupingMemberRouter";

export default class ActivityMemberRouter extends GroupingMemberRouter<Activity> {
    constructor() {
        // @ts-ignore
        super(Activity, Permission.EventAdministrator, [[Group, Permission.Grouper]]);
    }
}
