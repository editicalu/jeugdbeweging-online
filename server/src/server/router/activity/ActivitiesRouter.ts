import { Permission } from "../../../authentication/Permission";
import Activity from "../../../entities/Activity";
import GroupingsRouter from "../GroupingsRouter";

export default class ActivitiesRouter extends GroupingsRouter<Activity> {
    constructor() {
        super(Activity, Permission.EventAdministrator);
        this.groupingName = "activitie";
        this.groupingNameCapitalised = "Activitie";
        this.groupingApiUrl = `/api/activities/`;
    }
}
