interface IResponseReport {
    id: number;
    reportName: string;
    writtenById: number;
    date: string;
    writtenByName: string;
    content: string;
}

export default IResponseReport;
