import ResponseAddress from "./ResponseAddress";

interface IResponsePerson {
    id: number|undefined;
    firstName: string|undefined;
    lastName: string|undefined;
    birthplace: string|undefined;
    birthdate: string|undefined;
    gender: string|undefined;
    phoneNumber: string|undefined;
    address: ResponseAddress|undefined;
    comment: string|undefined;
    registrationPending: boolean|undefined;
}

export default IResponsePerson;
