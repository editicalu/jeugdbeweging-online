import { NextFunction } from "connect";
import { Request, Response } from "express";

const catchRouteErrors =
    (routeFn: (req: Request, res: Response, next: NextFunction) => Promise<any>,
    ) =>
        (req: Request, res: Response, next: NextFunction) =>
            routeFn(req, res, next).catch((err) => {
                console.error(err); next();
            });

export default catchRouteErrors;
