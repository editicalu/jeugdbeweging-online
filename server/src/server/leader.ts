import { Express, Request, Response } from "express";
import { LeaderDao } from "../database/LeaderDao";
import catchRouteErrors from "./catchRouteErrors";

/**
 * Adds all routes around person objects to the server
 * @param app The webserver object
 */
export default function add_leader_routes(app: Express) {
    get_acc_to_person_id(app);
    get_person_to_acc_id(app);
}

/**
 * Use this API to translate an account-id to a leader-id
 * This will only succeed if the account-id belongs to a leader
 */
function get_acc_to_person_id(app: Express) {
    app.get("/api/translate/account/:accID", catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {

        // Check if account id is missing
        if (req.params.accID === null) {
            res.status(400);
            res.json({valid: false, error: "Bad request: account id missing"});
            return;
        }

        const accID = Number(req.params.accID);
        const personID: number|null = await LeaderDao.translateAccountToPersonID(accID);

        if (personID === null) {
            res.status(400);
            res.json({valid: false, error: "Something went wrong." });
            return;
        } else {
            res.status(200);
            res.json({valid: true, personID});
        }

    }
}

/**
 * Use this API to translate a leader-id to an account-id
 * This will only succeed if the leader-id belongs to a leader
 */
function get_person_to_acc_id(app: Express) {
    app.get("/api/translate/person/:personID", catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {

        // Check if person id is missing
        if (req.params.personID === null) {
            res.status(400);
            res.json({valid: false, error: "Bad request: person id missing."});
            return;
        }

        const personID = Number(req.params.personID);
        const accID = await LeaderDao.translatePersonToAccountID(personID);

        // Check if something went wrong
        if (accID === null) {
            res.status(400);
            res.json({valid: false, error: "Something went wrong"});
            return;
        } else {
            res.status(200);
            res.json({valid: true, accountID: accID});
        }
    }
}
