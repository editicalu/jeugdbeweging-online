import { Express, Request, Response } from "express";
import Account from "../authentication/Account";
import { Permission } from "../authentication/Permission";
import SessionManager from "../authentication/SessionManager";
import AccountDao from "../database/AccountDao";
import ChildDao from "../database/ChildDao";
import DataRequestDao from "../database/DataRequestDao";
import GroupsDao from "../database/group/GroupsDao";
import ParentDao from "../database/ParentDao";
import catchRouteErrors from "./catchRouteErrors";

export default function add_datarequest_routes(app: Express) {
    get_account_dr(app);
    get_child_dr(app);
}

function get_account_dr(app: Express) {
    app.get("/api/account/:id(\\d+)/datarequest/", catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        async function send(acc: Account, response: Response) {
            const accData = await DataRequestDao.getAccountDataRequest(acc);
            if (accData !== null) {
                // Force the user to download the file
                response.append("Content-Disposition", `attachment; filename="dataverzoek.json"`);
                response.json(accData);
            } else {
                response.json({ valid: false, error: "unknownError" });
            }
        }

        // TODO: DPL integration, once !132 is merged.

        const signedInAccount = await SessionManager.isLoggedIn(req);

        if (signedInAccount === null) {
            res.status(404);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }

        const requestedId: number = Number(req.params.id);
        if (isNaN(requestedId)) {
            res.status(400);
            res.json({ valid: false, error: "invalidParams" });
            return;
        }

        const requestedAccount = signedInAccount.getUid() === requestedId
            ? signedInAccount : await AccountDao.fromIdDatabase(req.params.id);

        if (requestedAccount === null) {
            res.status(404);
            res.json({ valid: false, error: "notFound" });
            return;
        }

        if (signedInAccount.getUid() !== requestedId) {
            if (requestedAccount.isParent()) {
                // another parent was requested

                if (signedInAccount.getPermissions().hasPermission(Permission.ParentAdministrator)) {
                    await send(requestedAccount, res);
                } else {
                    res.status(404);
                    res.json({ valid: false, error: "unauthorized" });
                }

            } else {
                // another leader was requested

                if (signedInAccount.getPermissions().hasPermission(Permission.LeaderAdministrator)) {
                    await send(requestedAccount, res);
                } else {
                    res.status(404);
                    res.json({ valid: false, error: "unauthorized" });
                }
            }
        } else {
            // Requested your own account.
            await send(requestedAccount, res);
        }
    }
}

function get_child_dr(app: Express) {
    app.get("/api/person/:id(\\d+)/datarequest/", catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const requestedId: number = Number(req.params.id);
        if (isNaN(requestedId)) {
            res.status(400);
            res.json({ valid: false, error: "invalidParams" });
            return;
        }

        const childPromise = ChildDao.fromId(requestedId);

        // TODO: DPL integration, once !132 is merged.

        const signedInAccount = await SessionManager.isLoggedIn(req);

        if (signedInAccount === null) {
            res.status(404);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }

        const child = await childPromise;

        if (child === null) {
            res.status(404);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }

        if (!(
            (await (new GroupsDao()).getSharedGroupings(child.getId()!, signedInAccount.getUid()!))!.length > 0 ||
            (await signedInAccount.getPermissions().hasPermission(Permission.ChildrenAdministrator)) ||
            (signedInAccount.isParent() && ParentDao.isParentOf(child.getId()!, signedInAccount.getUid()!)))) {

            res.status(404);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }

        const childData = await DataRequestDao.personToPersonRequest(child);
        res.append("Content-Disposition", `attachment; filename="dataverzoek.json"`);
        res.json(childData);
    }
}
