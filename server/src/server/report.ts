import { Express, Request, Response } from "express";
import Account from "../authentication/Account";
import { Permission } from "../authentication/Permission";
import SessionManager from "../authentication/SessionManager";
import { LeaderDao } from "../database/LeaderDao";
import PersonDao from "../database/PersonDao";
import ReportDao from "../database/ReportDao";
import LeaderDPL from "../dataPermissionLayer/LeaderDPL";
import Result from "../dataPermissionLayer/Result";
import { Person } from "../entities/Person";
import { Report, ReportMin } from "../entities/Report";
import catchRouteErrors from "./catchRouteErrors";

export default function add_report_routes(app: Express) {
    put_saveNew_route(app);
    get_all_reports_route(app);
    put_override_route(app);
    get_report_route(app);
    delete_report_route(app);
}

interface ISaveNewRequest {
    reportName: string;
    content: string;
}

function put_saveNew_route(app: Express) {
    app.put(`/api/report`, catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        const loggedInUser: Account | null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in
        if (loggedInUser === null) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        // Check if account has rights to add reports
        if (!loggedInUser.getPermissions().hasPermission(Permission.Reporter)) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        // Try to save new
        try {
            const requestData: ISaveNewRequest = req.body;

            const personId: number|null = await LeaderDao.translateAccountToPersonID(loggedInUser.getUid()!);
            if (personId === null) {
                res.status(500);
                res.json({valid: false, error: "Database error"});
                return;
            }

            const reportId: number | null =
                await ReportDao.saveNew(
                    requestData.reportName,
                    new Date(),
                    requestData.content,
                    personId,
                );

            if (reportId === null) {
                res.status(500);
                res.json({ valid: false, error: "Internal server error" });
                return;
            }
            res.status(200);
            res.json({ valid: true, reportID: reportId });
            return;
        } catch (e) {
            console.error(e);
            res.status(400);
            res.json({ valid: false, error: "Bad request" });
            return;
        }
    }
}

function put_override_route(app: Express) {
    app.put(`/api/report/:id`, catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        const loggedInUser: Account | null = await SessionManager.isLoggedIn(req);
        const reportID: number = req.params.id;

        // Check if user is logged in
        if (loggedInUser === null) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        // Check if account has rights to add reports
        if (!loggedInUser.getPermissions().hasPermission(Permission.Reporter)) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        // Try to override
        try {
            const requestData: ISaveNewRequest = req.body;

            const succeeded =
                await ReportDao.override(
                    reportID,
                    requestData.reportName,
                    new Date(),
                    requestData.content,
                    loggedInUser.getUid()!,
                );

            if (!succeeded) {
                res.status(500);
                res.json({ valid: false, error: "Internal server error" });
                return;
            }
            res.status(200);
            res.json({ valid: true, reportID });
            return;
        } catch (e) {
            console.error(e);
            res.status(400);
            res.json({ valid: false, error: "Bad request" });
            return;
        }
    }
}

function get_all_reports_route(app: Express) {
    app.get(`/api/report`, catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const loggedInUser: Account | null = await SessionManager.isLoggedIn(req);

        // Check if user is logged in or isn't a leader
        if (loggedInUser === null || loggedInUser.isParent()) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        try {
            const allReportsMin: ReportMin[] = await ReportDao.getAllReportsMin();
            for (const i of allReportsMin) {
                const personResult: Result<Person> = await PersonDao.fromIdDatabase(i.writtenById);
                if (personResult.isError()) {
                    i.writtenByName = "Voornaam" + "Achternaam";
                }
                i.writtenByName = personResult.getContent()!.getName();
            }

            res.status(200);
            res.json({ valid: true, reports: allReportsMin });
            return;
        } catch (e) {
            res.status(400);
            res.json({ valid: false, error: "Internal server error" });
            return;
        }
    }
}

function get_report_route(app: Express) {
    app.get(`/api/report/:id`, catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const loggedInUser: Account | null = await SessionManager.isLoggedIn(req);
        const reportID: number = req.params.id;

        // Check if user is logged in or isn't a leader
        if (loggedInUser === null || loggedInUser.isParent()) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        try {
            const reportResult: Result<Report> = await ReportDao.getFullReport(reportID);
            if (reportResult.isError()) {
                res.status(reportResult.getErrorCode());
                res.json({valid: false, error: reportResult.getErrorMessage()});
                return;
            }
            const report: Report = reportResult.getContent()!;

            res.status(200);
            res.json({ valid: true, report: Report.toServerMessage(report) });
            return;
        } catch (e) {
            console.error(e);
            res.status(400);
            res.json({ valid: false, error: e.message });
            return;
        }
    }
}

function delete_report_route(app: Express) {
    app.delete(`/api/report/:id`, catchRouteErrors(delete_route));

    async function delete_route(req: Request, res: Response) {
        const loggedInUser: Account | null = await SessionManager.isLoggedIn(req);
        const reportID: number = req.params.id;

        // Check if user is logged in and is a leader
        if (loggedInUser === null || loggedInUser.isParent()) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        // Check if account has rights to add reports
        if (!loggedInUser.getPermissions().hasPermission(Permission.Reporter)) {
            res.status(403);
            res.json({ valid: false, error: "Unauthorized" });
            return;
        }

        const report = await ReportDao.remove(reportID);

        if (report) {
            res.status(200);
            res.json({ valid: true });
            return;
        } else {
            res.status(500);
            res.json({ valid: false, error: "Internal server error" });
            return;
        }
    }
}
