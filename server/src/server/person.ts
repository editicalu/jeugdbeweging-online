import { Express, Request, Response } from "express";
import Account from "../authentication/Account";
import { Permission } from "../authentication/Permission";
import Permissions from "../authentication/Permissions";
import SessionManager from "../authentication/SessionManager";
import AccountDao from "../database/AccountDao";
import { ChildDao } from "../database/ChildDao";
import { LeaderDao } from "../database/LeaderDao";
import { PersonDao } from "../database/PersonDao";
import LeaderDPL from "../dataPermissionLayer/LeaderDPL";
import PersonDPL from "../dataPermissionLayer/PersonDPL";
import Result from "../dataPermissionLayer/Result";
import Status from "../dataPermissionLayer/Status";
import { Child } from "../entities/Child";
import Address from "../entities/datatypes/Address";
import { Leader } from "../entities/Leader";
import { Gender, Person, PersonMin } from "../entities/Person";
import { PersonType } from "../entities/PersonType";
import catchRouteErrors from "./catchRouteErrors";
import ResponseAddress from "./serverMessages/ResponseAddress";
import ResponsePerson from "./serverMessages/ResponsePerson";

/**
 * Adds all routes around person objects to the server
 * @param app The webserver object
 */
export default function add_person_routes(app: Express) {
    get_request_person_route(app);
    get_allchildren_route(app);
    get_all_leaders_route(app);
    get_allpersons_route(app);

    put_new_child_route(app);
    put_existing_child_route(app);

    put_new_parent_route(app);
    put_parent_route(app);

    put_new_leader_route(app);
    put_leader_route(app);

    delete_person_route(app);
}

function readChildFromRequest(req: Request, regPending: boolean): Child | null {
    const { firstName, lastName, birthplace, birthdate, gender, phoneNumber, address, comments } = req.body;
    const addressObj = address ? new Address(address.streetAddress, address.zipcode, address.city) || null : null;
    if (firstName && lastName && birthplace && birthdate) {
        return Child.newChild(
            firstName,
            lastName,
            birthplace,
            birthdate,
            Gender[gender as keyof typeof Gender],
            phoneNumber,
            addressObj,
            comments,
            regPending,
        );
    } else {
        return null;
    }
}

async function insertNewChild(req: Request, res: Response, registrationPending: boolean): Promise<number | null> {
    const child = readChildFromRequest(req, registrationPending);

    if (child === null) {
        res.status(400);
        res.json({ valid: false, error: "invalidParams" });
        return null;
    } else {
        return ChildDao.insertIntoDatabase(child);
    }
}

/**
 * @param parent Whether a parent account is being created.
 */
async function updateExistingAccount(req: Request, res: Response, canChangePermissions: boolean)
    : Promise<number | null> {
    try {
        const id = Number(req.params.id) || null;
        let account: Account;
        if (id === null) {
            res.status(404);
            res.json({ valid: false, error: "invalidParams" });
            return null;
        }

        const leaderAccountOption = await AccountDao.fromIdDatabase(id);
        if (!leaderAccountOption) {
            res.status(404);
            res.json({ valid: false, error: "notFound" });
            return null;
        }
        account = leaderAccountOption!;

        // Change email
        const username = req.body.username;
        if (username !== undefined) {
            account.setEmail(username);
        }

        // Change password
        const password = req.body.password;
        if (password !== undefined) {
            account.setPassword(password);
        }

        // Change permissions, if allowed to
        if (canChangePermissions) {
            const permissions = Permissions.fromInteger(req.body.permissions) || Permissions.empty();
            account.setPermissions(permissions);
        }

        return AccountDao.saveToDatabase(account);
    } catch (err) {
        console.error(`Could not save Account from request: ${err}`);

        return null;
    }
}

async function insertNewAccount(req: Request, res: Response, parent: boolean, canChangePermissions: boolean)
    : Promise<number | null> {
    try {
        // Should we create a new account or edit an existing one?
        // Add a new leader to the system
        if (!(req.body.username && req.body.password)) {
            res.status(400);
            res.json({ valid: false, error: "invalidParameters" });
            return null;
        }

        const email = req.body.username;
        const password = req.body.password;
        const permissions = canChangePermissions ? Permissions.fromInteger(req.body.permissions)
            || Permissions.empty() : Permissions.empty();

        const leaderAccount: Account = Account.fromUnhashedPassword(email, password, permissions, parent);
        return AccountDao.saveToDatabase(leaderAccount);
    } catch (err) {
        console.error(`Could not save new Account from request: ${err}`);

        return null;
    }
}

async function provide_basic_information(personOption: Person | null, res: Response) {
    if (personOption === null) {
        res.status(404);
        res.json({ valid: false, error: "notFound" });
    } else {
        res.json({ valid: true, ...personOption!.getBasicInformation() });
    }
}

async function provide_extended_information(personOption: Person | null, res: Response) {
    if (personOption === null) {
        res.status(404);
        res.json({ valid: false, error: "notFound" });
    } else {
        res.json({ valid: true, ...personOption!.getExtendedInformation() });
    }
}

/**
 * See Google Drive for API documentation
 */
function get_request_person_route(app: Express) {
    app.get("/api/person/:personid(\\d+)/", catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const reqId = Number(req.params.personid);
        const personResult: Result<Person> = await PersonDPL.getFromId(req, reqId);
        if (personResult.isCorrect()) {
            res.json({ valid: true, person: personResult.getContent()!.getExtendedInformation() });
        } else {
            res.status(personResult.getErrorCode());
            res.json({ valid: false, error: personResult.getErrorMessage() });
        }
    }
}

/**
 * See Google Drive for API documentation
 */
function put_new_child_route(app: Express) {
    app.put("/api/child/", catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        // Test whether the user is logged in.
        try {
            const account: Account | null = await SessionManager.isLoggedIn(req);
            if (account === null) {
                res.status(403);
                res.json({ valid: false, error: "unauthorized" });
                return;
            }

            const id: number | null = await insertNewChild(req, res, account ? account.isParent() : false);
            if (id !== null) {
                const valid = true;
                res.json({ valid, id });
            }
        } catch (err) {
            console.error(`error saving a new child: ${err}`);
            res.status(500);
            res.json({ valid: false, error: "unknownError" });
        }
    }
}

/**
 * See Google Drive for API documentation
 */
function put_existing_child_route(app: Express) {
    app.put("/api/child/:id(\\d+)/", catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        try {
            const account = await SessionManager.isLoggedIn(req);
            if (account === null) {
                res.status(403);
                res.json({ valid: false, error: "unauthorized" });
                return;
            }

            const changingId = Number(req.params.id);
            if (changingId === null || isNaN(changingId)) {
                res.status(400);
                res.json({ valid: false, error: "invalidParams" });
                return;
            }

            const childOpt: Child | null = await ChildDao.fromId(changingId);
            if (!childOpt) {
                res.status(404);
                res.json({ valid: false, error: "notFound" });
                return;
            }

            const child = childOpt as Child;

            if (req.body.firstName) {
                child.setFirstName(req.body.firstName);
            }

            if (req.body.lastName) {
                child.setLastName(req.body.lastName);
            }

            if (req.body.birthplace) {
                child.setBirthplace(req.body.birthplace);
            }

            if (req.body.birthdate) {
                child.setBirthdate(req.body.birthdate);
            }

            if (req.body.phoneNumber !== undefined) {
                child.setPhoneNumber(req.body.phoneNumber);
            }

            if (req.body.address !== undefined) {
                child.setAddress(req.body.address ?
                    new Address(req.body.address.streetAddress, req.body.address.zipCode, req.body.address.city) || null
                    : null);
            }

            if (!account.isParent() && req.body.registrationPending === false) {
                child.confirmRegistration();
            }

            const id = await ChildDao.insertIntoDatabase(child);
            const valid = id !== null;

            res.json({ valid, id });
        } catch (err) {
            console.error(`Error creating a child: ${err}`);

            res.status(500);
            res.json({ valid: false, error: "unknownError" });
        }
    }
}

function put_new_leader_route(app: Express) {
    app.put("/api/leader/", catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        try {
            // Test whether the user is logged in.
            const signedInAccount: Account | null = await SessionManager.isLoggedIn(req);
            if (signedInAccount === null) {
                res.status(403);
                res.json({ valid: false, error: "unauthorized" });
                return;
            }

            // Can this account create new accounts?
            if (!signedInAccount.getPermissions().hasPermission(Permission.LeaderAdministrator)) {
                res.status(403);
                res.json({ valid: false, error: "unauthorized" });
                return;
            }

            // Read data
            const { firstName, lastName, birthplace, birthdate, gender, phoneNumber, address, comments,
                username, password, permissions } = req.body;
            const addressObj = address ? new Address(address.streetAddress, address.zipCode, address.city)
                || null : null;
            if (!(firstName && lastName && birthplace && birthdate && username && password && permissions !== null)) {
                res.status(400);
                res.json({ valid: false, error: "invalidParams" });
                return;
            }

            // Create leader
            const account = await Account.fromUnhashedPassword(username, password,
                Permissions.fromInteger(permissions), false);
            const leader = Leader.newLeader(firstName, lastName, birthplace, birthdate, gender, phoneNumber,
                addressObj, comments, account);

            const tuple = await LeaderDao.insertIntoDatabase(leader);
            const valid = tuple !== null;
            if (valid) {
                const accId = tuple![0];
                const perId = tuple![1];
                res.json({ valid, accId, perId });
            } else {
                res.json({ valid: false, error: "unknownError" });
            }
        } catch (err) {
            console.error(`Error at /api/leader: ${err}`);
            res.json({ valid: false, error: "unknownError" });
        }
    }
}

function put_leader_route(app: Express) {
    app.put("/api/leader/:id(\\d+)/", catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        // Test whether the user is logged in.
        const signedInAccount: Account | null = await SessionManager.isLoggedIn(req);
        if (signedInAccount === null) {
            res.status(403);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }

        // Appropriate permissions?
        const canManage = signedInAccount.getPermissions().hasPermission(Permission.LeaderAdministrator);
        if (signedInAccount.getUid() !== req.params.accid && !canManage) {
            res.status(403);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }

        // Load the leader
        const person = await PersonDao.fromId(req.params.id);
        if (!person || person.getPersonType() !== PersonType.Leader) {
            res.status(404);
            res.json({ valid: false, error: "notFound" });
            return;
        }
        const leader = person as Leader;
        console.log(leader);

        // Change the provided data
        const { firstName, lastName, birthplace, birthdate, phoneNumber, address, comments,
            username, password, permissions } = req.body;
        if (firstName) {
            leader.setFirstName(firstName);
        }

        if (lastName) {
            leader.setLastName(lastName);
        }

        if (birthplace) {
            leader.setBirthplace(birthplace);
        }

        if (birthdate) {
            leader.setBirthdate(birthdate);
        }

        if (phoneNumber) {
            leader.setPhoneNumber(phoneNumber);
        }

        if (address) {
            const addressObj = address ? new Address(address.streetAddress, address.zipcode, address.city)
                || null : null;
            leader.setAddress(addressObj);
        }

        if (comments) {
            leader.setComment(comments);
        }

        if (username) {
            leader.getAccount().setEmail(username);
        }

        if (password) {
            leader.getAccount().setPassword(password);
        }

        if (permissions !== null && permissions !== undefined
            && signedInAccount.getPermissions().hasPermission(Permission.LeaderAdministrator)) {

            leader.getAccount().setPermissions(Permissions.fromInteger(permissions));
        }

        console.log(leader);
        const tuple = await LeaderDao.insertIntoDatabase(leader);
        const valid = tuple !== null;
        if (valid) {
            const accId = tuple![0];
            const perId = tuple![1];

            res.json({ valid, accId, perId });
        } else {
            res.status(404);
            res.json({ valid, error: "unknownError" });
        }
    }
}

function put_new_parent_route(app: Express) {
    app.put("/api/parent/", catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        // Test whether the user is logged in. If logged in, the account should have the appropriate permissions.
        const signedInAccount: Account | null = await SessionManager.isLoggedIn(req);

        if (signedInAccount !== null) {
            if (!signedInAccount.getPermissions().hasPermission(Permission.ParentAdministrator)) {
                res.status(403);
                res.json({ valid: false, error: "unauthorized" });
                return;
            }
        }

        // Safely get the parent from the body
        const canManage = signedInAccount ?
            signedInAccount.getPermissions().hasPermission(Permission.ParentAdministrator) : false;
        const id = await insertNewAccount(req, res, true, canManage);

        const valid = id !== null;
        if (!valid) {
            res.status(400);
            res.json({ valid: false, error: "invalidParams" });
            return;
        }

        if (signedInAccount === null) {
            await SessionManager.login(req, res, (await AccountDao.fromIdDatabase(id!))!);
        }

        const account = id ? await AccountDao.fromIdDatabase(id) : undefined;
        res.json({ valid, account: account!.toObject() || undefined });
    }
}

function put_parent_route(app: Express) {
    app.put("/api/parent/:id(\\d+)/", catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        // Test whether the user is logged in. If logged in, the account should have the appropriate permissions.
        const signedInAccount: Account | null = await SessionManager.isLoggedIn(req);
        if (signedInAccount === null) {
            res.status(403);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }
        const canManage = signedInAccount.getPermissions().hasPermission(Permission.ParentAdministrator);

        if (!canManage && (signedInAccount.getUid() !== Number(req.params.id))) {
            res.status(403);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }

        // Safely get the parent from the body
        const id = await updateExistingAccount(req, res, canManage);

        const valid = id !== null;
        if (!valid) {
            res.status(400);
            res.json({ valid: false, error: "invalidParams" });
            return;
        }
        res.json({ valid, id });

    }
}

function get_allchildren_route(app: Express) {
    app.get("/api/allchildren", catchRouteErrors(get_allchildren));

    async function get_allchildren(req: Request, res: Response) {
        const loggedInUserP = SessionManager.isLoggedIn(req);

        const loggedInUser = await loggedInUserP;
        if (!loggedInUser) {
            res.status(404);
            res.json({ error: "unauthorized" });
            return;
        }

        const permissions: Permissions = loggedInUser!.getPermissions();

        if (permissions.hasPermission(Permission.ChildrenViewer) ||
            permissions.hasPermission(Permission.ChildrenAdministrator)) {
            // allowed to see all children

            const allChildren: PersonMin[] | null = await ChildDao.allMin();
            const allChildrenList: PersonMin[] = allChildren !== null ? allChildren : [];

            res.json({ totalAmount: allChildrenList.length, persons: allChildrenList });
        } else { // only allowed to see children in own group(s)

            const uid = loggedInUser.getUid()!;

            const allChildrenFiltered: PersonMin[] | null = await ChildDao.allShareAGroupMin(uid);
            if (!allChildrenFiltered) {
                res.json({ totalAmount: 0, persons: {} });
                return;
            }

            res.json({ totalAmount: allChildrenFiltered.length, persons: allChildrenFiltered });
        }
    }
}

function get_allpersons_route(app: Express) {
    // TODO: get rid of /api/allpersons
    app.get("/api/allpersons/", catchRouteErrors(get_allpersons));
    app.get("/api/person/", catchRouteErrors(get_allpersons));

    async function get_allpersons(req: Request, res: Response) {
        const loggedInUserP = SessionManager.isLoggedIn(req);

        const loggedInUser = await loggedInUserP;
        if (!loggedInUser) {
            res.status(403);
            res.json({ error: "unauthorized" });
            return;
        }

        const loggedInUserPersonId = await LeaderDao.translateAccountToPersonID(loggedInUser.getUid()!);

        const permissions: Permissions = loggedInUser!.getPermissions();

        const visiblePersons: PersonMin[] = [];

        // TODO: Fix this permissions error (Permission.LeaderViewer)
        if (permissions.hasPermission(Permission.ChildrenViewer)) { // allowed to see all children
            const allChildren: PersonMin[] = await ChildDao.allMin() || [];

            const allChildrenNotNull: PersonMin[] = allChildren!;

            allChildrenNotNull.forEach((child) => {
                let doesContain = false;
                visiblePersons.forEach((vp) => {
                    if (vp.id === child.id) {
                        doesContain = true;
                    }
                });
                if (doesContain === false) {
                    visiblePersons.push(child);
                }
            });

        } else { // only allowed to see children in own group(s)

            const uid = loggedInUser.getUid()!;

            const allChildrenFiltered: PersonMin[] | null = await PersonDao.allShareAGroupMin(uid);
            if (!allChildrenFiltered) {
                res.json({ totalAmount: 0, persons: {} });
                return;
            }

            allChildrenFiltered.forEach((child) => {
                visiblePersons.push(child);
            });
        }
        if (permissions.hasPermission(Permission.LeaderViewer)) {
            const allLeadersResult: Result<PersonMin[]> = await LeaderDPL.getAll(loggedInUser);
            if (allLeadersResult.isError()) {
                res.status(allLeadersResult.getErrorCode());
                res.json({valid: false, error: allLeadersResult.getErrorMessage()});
                return;
            }
            const allLeaders = allLeadersResult.getContent()!;

            allLeaders.forEach((leader) => {
                let doesContain = false;
                visiblePersons.forEach((vp) => {
                    if (vp.id === leader.id) {
                        doesContain = true;
                    }
                });
                if (!doesContain) {
                    visiblePersons.push(leader);
                }
            });
        }

        // add own person to list if not yet in it
        const meResult: Result<PersonMin> = await PersonDao.fromIdDatabase(loggedInUserPersonId!);
        if (meResult.isError()) {
            // TODO: errorhandling
        }
        const me: PersonMin = meResult.getContent()!;
        let contains = false;
        visiblePersons.forEach((vp) => {
            if (me.id === vp.id) {
                contains = true;
            }
        });
        if (!contains) {
            visiblePersons.push(me);
        }

        res.json({ totalAmount: visiblePersons.length, persons: visiblePersons });
    }
}

function delete_person_route(app: Express) {
    app.delete("/api/person/:id(\\d+)", catchRouteErrors(delete_route));

    async function delete_route(req: Request, res: Response) {
        const result = await PersonDPL.deleteFromId(req, req.params.id);

        if (result.isCorrect()) {
            res.json({ valid: result.getContent() });
        } else {
            res.status(result.getErrorCode());
            res.json({ valid: false, error: result.getErrorMessage() });
        }
    }
}

function get_all_leaders_route(app: Express) {
    app.get("/api/allleaders", catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const loggedInUser: Account | null = await SessionManager.isLoggedIn(req);

        const leadersResult: Result<PersonMin[]> = await LeaderDPL.getAll(loggedInUser);
        if (leadersResult.isError()) {
            res.status(leadersResult.getErrorCode());
            res.json({ valid: false, error: leadersResult.getErrorMessage() });
            return;
        }
        const leaders: PersonMin[] = leadersResult.getContent()!;
        res.json({ valid: true, leaders });
        return;
    }
}
