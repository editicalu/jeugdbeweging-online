import assert = require("assert");
import * as request from "supertest";
import { app } from "../server";

describe("Authentication API tests", () => {
    it("should login and logout", async () => {
        const agent = request.agent(app);
        await agent.post("/api/login")
            .send({ username: "admin", password: "admin" })
            .set("Accept", "application/json")
            .then((res) => {
                assert.strictEqual(res.status, 200);
                assert.deepStrictEqual(res.body, {
                    account: {
                        email: "admin",
                        parent: false,
                        permissions: 4294967295,
                        uid: 1,
                    },
                    personID: 1,
                    valid: true,
                });
            });

        await agent.post("/api/logout")
            .send({})
            .set("Accept", "application/json")
            .then((res) => {
                assert.strictEqual(res.status, 200);
                assert.deepStrictEqual({ valid: true }, res.body);
            });
    });
});
