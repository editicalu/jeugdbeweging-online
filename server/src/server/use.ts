import { Express } from "express";

import bodyParser = require("body-parser");
import { Request, RequestHandler, Response } from "express-serve-static-core";
import session = require("express-session");
import Database from "../database/Database";

/**
 * Adds all app.use commands for the webserver
 * @param app The webserver object
 */
export default function add_express_uses(app: Express) {
    add_json_use(app);
    add_json_syntax_error(app);
    add_allow_cross_origin(app);
    add_express_session(app);
}

function add_json_use(app: Express) {
    // Parse application/json content
    app.use(bodyParser.json());
}

function add_json_syntax_error(app: Express) {
    // Custom error handler. Sends {error: syntaxerror} when the received JSON is invalid.
    app.use((err: Error, req: Request, res: Response, next: RequestHandler) => {
        if (err instanceof SyntaxError) {
            res.status(400);
            res.json({ error: "syntaxError" });
        }
    });
}

function add_allow_cross_origin(app: Express) {
    app.use((req, res, next) => {
        res.set({
            "Access-Control-Allow-Credentials": "true",
            "Access-Control-Allow-Headers": "Origin,Accept,X-Requested-With, Content-Type",
            "Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT,DELETE",
        });

        if (process.env.NODE_ENV !== "production") {
            res.set({
                "Access-Control-Allow-Origin":
                    `http${
                    process.env.VUE_APP_SSL === "true"
                        ? "s"
                        : ""
                    }://${
                    process.env.VUE_APP_DOMAIN_NAME
                    }:8080`,
            });
        }
        next();
    });
}

function add_express_session(app: Express) {
    const secret = process.env.API_SESSION_SECRET === undefined
        ? "SECRET" : process.env.API_SESSION_SECRET;
    const sessionConfiguration = {
        cookie: {
            domain: process.env.VUE_APP_DOMAIN_NAME,
            // maxAge takes milliseconds
            httpOnly: true,
            maxAge: Number(process.env.SESSION_MAXAGE_MILLISECONDS),
            secure: process.env.VUE_APP_SSL === "true",
        },
        resave: false,
        rolling: true,
        saveUninitialized: false,
        secret,
        store: new (require("connect-pg-simple")(session))({
            pgPromise: Database,
            tableName: "user_session",
        }),
        unset: "destroy",
    };
    app.use(session(sessionConfiguration));
}
