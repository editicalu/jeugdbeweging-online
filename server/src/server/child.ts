import { Express, Request, Response } from "express";
import Account from "../authentication/Account";
import { Permission } from "../authentication/Permission";
import SessionManager from "../authentication/SessionManager";
import GroupsDao from "../database/group/GroupsDao";
import ParentDao from "../database/ParentDao";
import RegistrationCodeDao from "../database/registrationCodeDao";
import catchRouteErrors from "./catchRouteErrors";

export default function add_child_routes(app: Express) {
    get_generate_registration_code(app);
    post_add_child_to_parent(app);
}

function generateRegistrationCode(): string {
    const characters = "abcdefghijklmnopqrstuvwxyz0123456789";
    let registrationCode = "";

    for (let i: number = 0; i < 4; i += 1) {
        for (let j: number = 0; j < 4; j += 1) {
            registrationCode += characters.charAt(Math.floor(Math.random() * characters.length));
        }

        if (i < 3) {
            registrationCode += "-";
        }
    }

    return registrationCode;
}

function get_generate_registration_code(app: Express) {
    app.get("/api/child/:personid/code", catchRouteErrors(respondRegistrationCode));

    async function respondRegistrationCode(req: Request, res: Response) {
        const account: Account | null = await SessionManager.isLoggedIn(req);
        const personid: number = Number(req.params.personid);

        // Test whether the user is logged in.
        if (account === null) {
            res.status(403);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }

        // Check if leader is in the same group as the child:
        const shareAGroup: boolean | null =
            (await new GroupsDao().getSharedGroupings(personid, account.getUid()!))!.length > 0;

        // Check if server could check if they share a group
        if (shareAGroup === null) {
            res.status(500);
            res.json({ valid: false });
            return;
        }

        // Check if leader has rights to view every child:
        const hasRightsToViewKids: boolean = account.getPermissions().hasPermission(Permission.ChildrenAdministrator);

        // Check if account has rights to ask for registration code:
        if (!shareAGroup && !hasRightsToViewKids) {
            // Account doesn't have the rights to ask for this information
            res.status(403);
            res.json({ valid: false, error: "unauthorized" });
            return;
        }

        // person has the rights to generate a registration code
        // Generate registration code
        const regCode: string = generateRegistrationCode();

        // Save this registration code to on the server
        const succeeded: boolean = await RegistrationCodeDao.insertRegistrationCodeIntoDatabase(personid, regCode);
        if (succeeded) {
            res.status(200);
            res.json({ valid: true, registrationCode: regCode });
            return;
        } else {
            res.status(500);
            res.json({ valid: false, error: "Internal server error" });
        }
    }
}

/**
 * @pre The uid of the account in the sessionmanager is not null if the sessionmanager contains an account
 */
function post_add_child_to_parent(app: Express) {
    app.post("/api/parent/add_child/", catchRouteErrors(respondAddChildToParent));

    async function respondAddChildToParent(req: Request, res: Response) {
        const account: Account | null = await SessionManager.isLoggedIn(req);
        let regCode: string = req.body.regCode;

        if (!regCode) {
            res.status(400);
            res.json({ valid: false, error: "invalidParams" });

        } else if (account === null) {
            // Test whether the user is logged in.
            res.status(403);
            res.json({ valid: false, error: "unauthorized: not logged in." });

        } else if (!account.isParent()) {
            // Check if session-account is a parent, else return error
            res.status(403);
            res.json({ valid: false, error: "unauthorized, only parents can do this." });

        } else {
            regCode = regCode.toLowerCase();
            const childId: number | null = await RegistrationCodeDao.ChildIdFromRegistrationCode(regCode);

            // Check if registration code is invalid
            if (childId === null) {
                res.status(404);
                res.json({ valid: false, error: "Invalid registration-code." });
                return;
            }

            // Add parent-child-relation and check if it went well
            const addChildPromise = ParentDao.addChild(account.getUid()!, childId);
            const removeRegCodePromise = RegistrationCodeDao.removeRegistrationCode(regCode);

            if (!await addChildPromise || !await removeRegCodePromise) {
                res.status(500);
                res.json({ valid: false, error: "Could not add parent-child-relation." });
                return;
            }

            // Everything went fine, return correct answer
            res.status(200);
            res.json({ valid: true });
        }
    }
}
