import { Express, Request, Response } from "express";
import Account from "../authentication/Account";
import login from "../authentication/Authenticator";
import { Permission } from "../authentication/Permission";
import Permissions from "../authentication/Permissions";
import SessionManager from "../authentication/SessionManager";
import AccountDao from "../database/AccountDao";
import { LeaderDao } from "../database/LeaderDao";
import catchRouteErrors from "./catchRouteErrors";

/**
 * Adds all routes around authentication to the server
 * @param app The webserver object
 */
export default function add_authentication_routes(app: Express) {
    post_login_route(app);
    post_logout_route(app);
}

/**
 * See https://docs.google.com/document/d/1JPYX1FjX-HTlJuP4PqMmAXKNeCpdvuoZQlw2wcAwsLY for API documentation
 */
function post_login_route(app: Express) {
    app.post("/api/login", catchRouteErrors(post_route));

    async function post_route(req: Request, res: Response) {
        // Safely get credentials from request body
        function getCredentials(): [string, string] | null {
            try {
                const { username, password } = req.body;
                return (username === null) || (password === null) ? null : [username, password];
            } catch (e) {
                console.error(e);
                return null;
            }
        }

        const credentials = getCredentials();
        if (credentials === null) {
            res.status(400);
            res.json({ valid: false });
        } else {
            const [username, password] = credentials;
            const accountOption: Account | null = await login(username, password);
            const valid: boolean = accountOption !== null;
            if (valid) {
                const account = accountOption as Account;
                SessionManager.login(req, res, account);
                if (account.isParent()) {
                    res.json({ valid, account: account.toObject() });
                } else {
                    const personID = await LeaderDao.translateAccountToPersonID(account.getUid()!);
                    res.json({ account: account.toObject(), personID, valid });
                }
            } else {
                res.json({ valid });
            }
        }
    }
}

function post_logout_route(app: Express) {
    app.post("/api/logout", catchRouteErrors(post_route));

    async function post_route(req: Request, res: Response) {
        try {
            SessionManager.logout(req, res);
            res.json({ valid: true });
        } catch (err) {
            res.status(500);
            res.json({ error: "InternalError" });
        }
    }
}
