import { Express, Request, Response } from "express";
import Account from "../authentication/Account";
import { Permission } from "../authentication/Permission";
import SessionManager from "../authentication/SessionManager";
import BillDao from "../database/BillDao";
import ParentDao from "../database/ParentDao";
import Bill from "../entities/Bill";
import { Child } from "../entities/Child";
import { Person } from "../entities/Person";
import catchRouteErrors from "./catchRouteErrors";

import BillDPL from "../dataPermissionLayer/BillDPL";
import Result from "../dataPermissionLayer/Result";
import ResponseBill from "./responseInterfaces/ResponseBill";

export default function add_bills_routes(app: Express) {
    post_save_new_bill(app);
    put_edit_bill(app);
    get_bills(app);
    get_bill(app);
    get_all_bills(app);
    delete_bill(app);
    get_billed(app);
    post_billed(app);
    delete_billed(app);
    put_billed_paid_status(app);
}

interface INewBillReq {
    bill: ResponseBill;
    personIds: number[];
}

interface IEditBillReq {
    bill: ResponseBill;
}

interface IEditPaidStatus {
    paid: boolean;
}

interface IAddBilled {
    personIds: number[];
}

/**
 * PUT /api/bill
 *
 * Save a new bill
 *
 * body:
 *  {
 *      bill: {id, description, amount}
 *      personIds
 *  }
 */
function post_save_new_bill(app: Express) {
    app.post(`/api/bill`, catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        const requestData: INewBillReq = req.body;

        // Check if all paramenters are provided
        if (requestData.bill.description === undefined || requestData.bill.amount === undefined) {
            res.status(400);
            res.json({valid: false, error: "Incorrect parameters"});
            return;
        }

        const saveNewResult: Result<number> =
            await BillDPL.saveNew(req, requestData.personIds, requestData.bill.description, requestData.bill.amount);

        if (saveNewResult.isCorrect()) {
            res.json({valid: true, billId: saveNewResult.getContent()});
            return;
        } else {
            res.status(saveNewResult.getErrorCode());
            res.json({valid: false, error: saveNewResult.getErrorMessage()});
            return;
        }
    }
}

/**
 * PUT /api/bill/[billId]
 *
 * edit a bill and save it's new information
 */
function put_edit_bill(app: Express) {
    app.put(`/api/bill/:billId`, catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        const billId: number = Number(req.params.billId);

        const requestData: IEditBillReq = req.body;

        const editResult: Result<void> =
            await BillDPL.edit(
                req,
                Bill.fromServerMessage(requestData.bill),
            );

        if (editResult.isCorrect) {
            res.json({valid: true, billID: billId});
        } else {
            res.status(editResult.getErrorCode());
            res.json({valid: false, error: editResult.getErrorMessage()});
        }
    }
}

/**
 * Get all bills beloning to a certain person, you can choose
 * between paid and unpaid bills
 */
function get_bills(app: Express) {
    app.get(`/api/bills/:personId/:paid`, catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const personId: number = Number(req.params.personId);
        const paid: boolean = req.params.paid;

        const getBillsOfPersonResult: Result<Bill[]> = await BillDPL.getBillsOfPerson(req, personId, paid);
        if (getBillsOfPersonResult.isCorrect()) {
            res.json({valid: true, bills: getBillsOfPersonResult.getContent()});
            return;
        } else {
            res.status(getBillsOfPersonResult.getErrorCode());
            res.json({valid: false, error: getBillsOfPersonResult.getErrorMessage()});
            return;
        }
    }
}

/**
 * GET /api/bills/:billId
 *
 * Get all the information about a bill, based on it's id
 */
function get_bill(app: Express) {
    app.get(`/api/bills/:billId`, catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const billId: number = Number(req.params.billId);

        const getBillResult: Result<Bill> = await BillDPL.getBill(req, billId);
        if (getBillResult.isCorrect()) {
            res.json({valid: true, bill: getBillResult.getContent()});
            return;
        } else {
            res.status(getBillResult.getErrorCode());
            res.json({valid: false, error: getBillResult.getErrorMessage()});
            return;
        }
    }
}

/**
 * GET /api/bills/
 *
 * Get a list of all bills
 */
function get_all_bills(app: Express) {
    app.get(`/api/bills/`, catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const getBillResult: Result<Bill[]> = await BillDPL.getAllBills(req);
        if (getBillResult.isCorrect()) {
            res.json({valid: true, bills: getBillResult.getContent()});
            return;
        } else {
            res.status(getBillResult.getErrorCode());
            res.json({valid: false, error: getBillResult.getErrorMessage()});
            return;
        }
    }
}

/**
 * DELETE /api/bills/:billId
 *
 * Delete a bill based on its id
 */
function delete_bill(app: Express) {
    app.delete(`/api/bills/:billId`, catchRouteErrors(delete_route));

    async function delete_route(req: Request, res: Response) {
        const billId: number = Number(req.params.billId);

        const deleteBillResult: Result<void> = await BillDPL.delete(req, billId);
        if (deleteBillResult.isCorrect()) {
            res.json({valid: true});
            return;
        } else {
            res.status(deleteBillResult.getErrorCode());
            res.json({valid: false, error: deleteBillResult.getErrorMessage()});
            return;
        }
    }
}

/**
 * GET /api/billed/:billId
 *
 * Get all the billed persons of a certain bill
 */
function get_billed(app: Express) {
    app.get(`/api/billed/:billId`, catchRouteErrors(get_route));

    async function get_route(req: Request, res: Response) {
        const billId: number = Number(req.params.billId);

        const paidResult: Result<Person[]> = await BillDPL.getBilled(req, billId, true);
        if (paidResult.isError()) {
            res.status(paidResult.getErrorCode());
            res.json({valid: false, error: paidResult.getErrorMessage()});
            return;
        }

        const unPaidResult: Result<Person[]> = await BillDPL.getBilled(req, billId, false);
        if (unPaidResult.isError()) {
            res.status(unPaidResult.getErrorCode());
            res.json({valid: false, error: unPaidResult.getErrorMessage()});
            return;
        }

        res.json({valid: true, paid: paidResult.getContent(), unPaid: unPaidResult.getContent()});
        return;
    }
}

function post_billed(app: Express) {
    app.post(`/api/billed/:billId`, catchRouteErrors(post_route));

    async function post_route(req: Request, res: Response) {
        const billId: number = Number(req.params.billId);
        if (isNaN(billId)) {
            res.status(400);
            res.json({valid: false, error: `No bill id given`});
            return;
        }

        const body: IAddBilled = req.body;
        const addPersonsResult: Result<void> = await BillDPL.addPersons(req, body.personIds, billId);
        if (addPersonsResult.isError()) {
            res.status(addPersonsResult.getErrorCode());
            res.json({valid: false, error: addPersonsResult.getErrorMessage()});
            return;
        }
        res.json({valid: true});
        return;
    }
}

/**
 * DELETE /api/billed/:billId/personId
 *
 * Delete a certain connection between a person and a bill
 */
function delete_billed(app: Express) {
    app.delete(`/api/billed/:billId/:personId`, catchRouteErrors(delete_route));

    async function delete_route(req: Request, res: Response) {
        const billId: number = Number(req.params.billId);
        const personId: number = Number(req.params.personId);

        const removeResult: Result<void> = await BillDPL.removePerson(req, personId, billId);
        if (removeResult.isCorrect()) {
            res.json({valid: true});
            return;
        } else {
            res.status(removeResult.getErrorCode());
            res.json({valid: false, error: removeResult.getErrorMessage()});
            return;
        }
    }
}

/**
 * PUT /api/billed/:billId/:personId
 *
 * edit the paid-status of a person for a certain bill
 */
function put_billed_paid_status(app: Express) {
    app.put(`/api/billed/:billId/:personId`, catchRouteErrors(put_route));

    async function put_route(req: Request, res: Response) {
        const billId: number = Number(req.params.billId);
        const personId: number = Number(req.params.personId);
        const requestData: IEditPaidStatus = req.body;

        const setPaidResult: Result<void> = await BillDPL.setPaid(req, personId, billId, requestData.paid);
        if (setPaidResult.isCorrect()) {
            res.json({valid: true});
            return;
        } else {
            res.status(setPaidResult.getErrorCode());
            res.json({valid: false, error: setPaidResult.getErrorMessage()});
            return;
        }
    }
}
