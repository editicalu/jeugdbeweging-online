interface IResponseAddress {
    streetAddress: string|undefined;
    zipcode: string|undefined;
    city: string|undefined;
}

export default IResponseAddress;
