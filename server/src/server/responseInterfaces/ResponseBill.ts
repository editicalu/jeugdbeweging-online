interface IResponseBill {
    id: number|undefined;
    description: string|undefined;
    amount: number|undefined;
    creationDate: string|undefined;
}

export default IResponseBill;
