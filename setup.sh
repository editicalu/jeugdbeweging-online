#! /bin/bash

# Terminate on any error
set -e

scripts/load_db.sh
python3 scripts/gen_config.py
cp ./.env ./client/.env
cp ./.env ./server/.env
docker-compose up --build