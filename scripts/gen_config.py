import getpass
import uuid
from shutil import copyfile
from pathlib import Path

config = {
    "VUE_APP_YM_NAME": "Jeugdbeweging Online",
    "VUE_APP_LOGO_PATH": "/img/icons/logo.png",
    "VUE_APP_PRIVACY_POLICY_URL": "http://example.com/",
    "VUE_APP_TERMS_URL": "http://example.com/",
    "VUE_APP_DOMAIN_NAME": "127.0.0.1",
    "ALLOW_PARENTS": "true",
    "DB_PASS": "youthmovement",
    "VUE_APP_SSL": "false",
    "SSL_KEY_PATH": "",
    "SSL_CERT_PATH": "",
    "SSL_PASS": "",
    "API_SESSION_SECRET": "CHANGE_ME",
    "VUE_APP_PORT": "80",
    "DB_NAME": "youthmovement",
    "DB_USER": "youthmovement",
    "DB_IP": "127.0.0.1",
    "DB_PORT": "5432",
    "SESSION_MAXAGE_MILLISECONDS": "7200000",
    "NODE_ENV": "production",
}
ICON_DIR = "/img/icons/"


def ask_password(text):
    print(text)

    pass_one = getpass.getpass("password: ")
    pass_two = getpass.getpass("confirm password: ")

    while pass_one != pass_two:
        print("Password do not match.")
        pass_one = getpass.getpass("password: ")
        pass_two = getpass.getpass("confirm password: ")

    return pass_one


def set_opt_str(text, config_option):
    answer = input(text)

    if (answer != ""):
        config[config_option] = answer


def set_opt_int(text, config_option):
    answer_correct = False

    while not answer_correct:
        answer = input(text)

        if (answer == ""):
            break

        try:
            int(answer)
            answer_correct = True
        except ValueError:
            print("Not a valid number.")

    if (answer != ""):
        config[config_option] = answer


def get_non_opt_int(text):
    answer_correct = False

    while not answer_correct:
        answer = non_optional_input(text)
        try:
            int(answer)
            answer_correct = True
        except ValueError:
            print("Not a valid number.")

    return answer


def non_optional_input(text):
    answer = input(text)

    while len(answer) == 0:
        print("This field is not optional.")
        answer = input(text)

    return answer


def yes_no_input(text):
    answer = input(text)

    if len(answer) == 0:
        return 0

    possible_confirms = ["y", "yes", "Y", "Yes", "YES", "1"]
    possible_denies = ["n", "no", "N", "No", "NO", "0"]
    while answer not in possible_confirms and answer not in possible_denies:
        print("Not a valid option.")
        answer = input(text)

    return answer in possible_confirms


def private_key_input(text):
    answer = input(text)

    if len(answer) == 0:
        return str(uuid.uuid4())
    else:
        return answer


def copy_file(src_file, dst_file):
    try:
        copyfile(src_file, dst_file)
        return True
    except:
        print("Failed to get file. Does the specified file exist?")
        return False


def ask_logo():
    ask_logo_again = True
    while ask_logo_again:
        logo_path = input("Path to your organisation's logo [optional]: ")
        if (logo_path != ""):
            ask_logo_again = not copy_file(
                logo_path, "./client/public" + ICON_DIR + logo_path.split("/")[-1])
        else:
            ask_logo_again = False
    if (logo_path != ""):
        config["VUE_APP_LOGO_PATH"] = ICON_DIR + logo_path.split("/")[-1]


def ask_ssl_cert():
    ask_again = True
    while ask_again:
        cert_path = non_optional_input("Path to the SSL certificate: ")
        ask_again = not Path(cert_path).is_file()
    config["SSL_CERT_PATH"] = cert_path


def ask_ssl_key():
    ask_again = True
    while ask_again:
        key_path = non_optional_input("Path to the SSL key: ")
        ask_again = not Path(key_path).is_file()
    config["SSL_KEY_PATH"] = key_path


def interactive():
    accepted = False

    while not accepted:
        print("This is the interactive setup for the Jeugdbeweging Online project.")
        print("[optional] fields can be skipped by pressing Enter.")
        print("")

        config["VUE_APP_YM_NAME"] = non_optional_input(
            "Your organisation's name: ")

        ask_logo()

        config["VUE_APP_PRIVACY_POLICY_URL"] = "http://" + non_optional_input(
            "URL of your privacy policy: http://")

        config["VUE_APP_TERMS_URL"] = "http://" + non_optional_input(
            "URL of your terms of service: http://")

        config["VUE_APP_DOMAIN_NAME"] = non_optional_input(
            "Site's domain name: ")

        config["ALLOW_PARENTS"] = "true" if yes_no_input(
            "Allow parents to register accounts [y/N]: ") else "false"

        config["DB_PASS"] = ask_password("The database's password: ")

        ssl = yes_no_input("Use SSL [y/N]: ")
        if ssl:
            ask_ssl_cert()
            ask_ssl_key()
            config["SSL_PASS"] = ask_password(
                "The SSL certificate's password: ")
            config["VUE_APP_SSL"] = "true"
            config["VUE_APP_PORT"] = "443"
        print("")

        if yes_no_input("> Configure advanced options? [y/N] "):
            set_opt_int(
                "Proxy port number [optional]: ",  "VUE_APP_PORT")
            set_opt_str("Database name [optional]: ", "DB_NAME")
            set_opt_str("Database username [optional]: ", "DB_USER")
            set_opt_str("Database IP [optional]: ", "DB_IP")
            set_opt_int("Database port [optional]: ", "DB_PORT")
            config["API_SESSION_SECRET"] = private_key_input(
                "Session secret [optional]: ")
        else:
            config["API_SESSION_SECRET"] = str(uuid.uuid4())

        # just a newline for readability of output
        print("")
        if yes_no_input("> Scroll back up and check your input. Everything correct? [y/N] "):
            accepted = True
        else:
            print("--------------")
            print("")


def dict_to_env():
    env_file_str = ""
    for key, val in config.items():
        env_file_str = env_file_str + key + "=" + val + "\n"
    return env_file_str


def main():
    try:
        interactive()
        with open("./.env", "w+") as write_file:
            write_file.write(dict_to_env())

        print("WARNING: the file `.env` contains sensitive information. Ensure that no malicious users are able to access it!")
        # just a newline for readability of output
        print("")
    except KeyboardInterrupt:
        exit(1)


main()
