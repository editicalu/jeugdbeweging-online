#! /bin/bash

volume_name=$(docker volume ls | grep -o -e jeugdbeweging-*online_db_volume)
docker rm $(docker volume rm -f $volume_name 2>&1 | grep -o '\[.*\]' | grep -o '[[:alnum:]]*')
docker volume rm -f $volume_name

echo ""
echo "Select the mode to load the db in"

index=0
for file in $(ls database/scripts | grep .sql);
do
    echo -n ["${index}"]" "
    echo "${file}" | grep '[a-zA-Z_0-9\-]*\.' -o
    ((index++))
done

echo ""
read -p "-: " answer

index=0
for file in $(ls database/scripts | grep .sql);
do
    if [ ${index} -eq ${answer} ];
    then
        cp database/scripts/${file} database/scripts/current.sql
        echo -n "The mode is now "
        echo "${file}" | grep '[a-zA-Z_0-9\-]*\.' -o
        echo
        exit 0
    fi
    ((index++))
done
echo "Error: invalid input"
exit 1

