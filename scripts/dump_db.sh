#! /bin/bash

DB_NAME=$(grep DB_NAME= <.env | cut -d "=" -f2)
DB_USER=$(grep DB_USER= <.env | cut -d "=" -f2)
DB_PASS=$(grep DB_PASS= <.env | cut -d "=" -f2)
DB_IP=$(grep DB_IP= <.env | cut -d "=" -f2)
DB_PORT=$(grep DB_PORT= <.env | cut -d "=" -f2)
SCRIPT_DIR="database/scripts/"

if [ ! -d ${SCRIPT_DIR} ]
then
	mkdir ${SCRIPT_DIR}
fi
PGPASSWORD=${DB_PASS} pg_dump -h ${DB_IP} -p ${DB_PORT} -U ${DB_USER} ${DB_NAME} -o > ${SCRIPT_DIR}DB_BACKUP_$(date -u -I).sql

if [ ! $? -eq 0 ]
then
    rm ${SCRIPT_DIR}DB_BACKUP_$(date -u -I).sql
elif [ $? -eq 0 ]
then
    echo
    echo "Succesfully dumped the database."
fi