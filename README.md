# Jeugdbeweging Online
Jeugdbeweging Online is a data management system for youth movements. It allows leaders of the youth movement to store member's data, create groups of them, create checklists, write reports, bill users, optionally allow parents to administer members, etc.

# Deployment
Deployment has been made as easy as possible. Some basic knowledge of Ubuntu and the command line is required.

## Requirements
The application requires at least:
-  Ubuntu 18.04 LTS
-  Python 3.6.7
-  Docker 18.06
-  docker-compose 3.3

We expect the user to be running an instance of Ubuntu already. The above mentioned Ubuntu version includes Python3 by default.

### Docker
*NOTE: following section is based on the official Docker set up guide for Ubuntu. Kindly see [this page](https://docs.docker.com/install/linux/docker-ce/ubuntu/#os-requirements) for more information.*

#### Set up the repository

1. Update the `apt` package index:
```
$ sudo apt-get update
```

2. Install packages to allow apt to use a repository over HTTPS:
```
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```

3. Add Docker's official GPG key:
```
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

4. Set up the stable repository:
```
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
``` 

#### Install Docker
1. Update the `apt` package index:
```
$ sudo apt-get update
```

2. Install the latest version of Docker CE:
```
$ sudo apt-get install docker-ce
```

3. Verify that Docker CE is installed correctly by running the hello-world image:
```
$ sudo docker run hello-world
```

### docker-compose
Install docker-compose, which is part of the Docker PPA:
```
$ sudo apt-get install docker-compose
```

## Set up
A simple, interactive set up script is provided. After answering the script's questions, the application automatically deploys.

*NOTE: the application ships with example data. This data can be loaded by selecting the `test` option during the loading of the database mode, which is the very first question being asked.*

Run the script as follows:
```
$ sudo ./setup.sh
```

## Getting started
The application ships with a default administrative user. Both username and password of this user are `admin`. 

Use this user to log in on the application and start adding leaders and members, set leader's permissions, and simply use the application as needed.

*NOTE: it is highly recommended to change the administrator's password as soon as possible after deployment.*

# Acknowledgements
This application was developed for the subject Software Engineering at the UHasselt 2018-2019.

We would like to thank dr. Jan VAN DEN BERGH for assisting our team during the course of the project.

Our thank also goes out to Prof. dr. Kris LUYTEN and the rest of the Software Engineering's educational team for teaching us the agile development methodology. 