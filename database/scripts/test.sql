--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: gender_type; Type: TYPE; Schema: public; Owner: youthmovement
--

CREATE TYPE public.gender_type AS ENUM (
    'Male',
    'Female',
    'Other'
);


ALTER TYPE public.gender_type OWNER TO youthmovement;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activity; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.activity (
    id integer NOT NULL,
    title text NOT NULL,
    description text,
    registration_date timestamp(6) without time zone DEFAULT NULL::timestamp without time zone,
    start_date timestamp(6) without time zone DEFAULT LOCALTIMESTAMP NOT NULL,
    stop_date timestamp(6) without time zone DEFAULT LOCALTIMESTAMP NOT NULL
);


ALTER TABLE public.activity OWNER TO youthmovement;

--
-- Name: activity_id_seq; Type: SEQUENCE; Schema: public; Owner: youthmovement
--

CREATE SEQUENCE public.activity_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activity_id_seq OWNER TO youthmovement;

--
-- Name: activity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: youthmovement
--

ALTER SEQUENCE public.activity_id_seq OWNED BY public.activity.id;


--
-- Name: activity_person_relation; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.activity_person_relation (
    person_id integer NOT NULL,
    activity_id integer NOT NULL
);


ALTER TABLE public.activity_person_relation OWNER TO youthmovement;

--
-- Name: bill; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.bill (
    id integer NOT NULL,
    description text NOT NULL,
    created_on date NOT NULL,
    amount numeric(1000,2) NOT NULL
);


ALTER TABLE public.bill OWNER TO youthmovement;

--
-- Name: bill_id_seq; Type: SEQUENCE; Schema: public; Owner: youthmovement
--

CREATE SEQUENCE public.bill_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bill_id_seq OWNER TO youthmovement;

--
-- Name: bill_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: youthmovement
--

ALTER SEQUENCE public.bill_id_seq OWNED BY public.bill.id;


--
-- Name: bill_person_relation; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.bill_person_relation (
    paid boolean DEFAULT false NOT NULL,
    person_id integer NOT NULL,
    bill_id integer NOT NULL
);


ALTER TABLE public.bill_person_relation OWNER TO youthmovement;

--
-- Name: checklist; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.checklist (
    id integer NOT NULL,
    title text NOT NULL,
    description text
);


ALTER TABLE public.checklist OWNER TO youthmovement;

--
-- Name: checklist_id_seq; Type: SEQUENCE; Schema: public; Owner: youthmovement
--

CREATE SEQUENCE public.checklist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.checklist_id_seq OWNER TO youthmovement;

--
-- Name: checklist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: youthmovement
--

ALTER SEQUENCE public.checklist_id_seq OWNED BY public.checklist.id;


--
-- Name: checklist_person_relation; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.checklist_person_relation (
    checked boolean DEFAULT false NOT NULL,
    person_id integer NOT NULL,
    checklist_id integer NOT NULL
);


ALTER TABLE public.checklist_person_relation OWNER TO youthmovement;

--
-- Name: child; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.child (
    person_id integer NOT NULL,
    registration_pending boolean DEFAULT true NOT NULL
);


ALTER TABLE public.child OWNER TO youthmovement;

--
-- Name: group; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public."group" (
    id integer NOT NULL,
    title text NOT NULL,
    description text
);


ALTER TABLE public."group" OWNER TO youthmovement;

--
-- Name: group_id_seq; Type: SEQUENCE; Schema: public; Owner: youthmovement
--

CREATE SEQUENCE public.group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group_id_seq OWNER TO youthmovement;

--
-- Name: group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: youthmovement
--

ALTER SEQUENCE public.group_id_seq OWNED BY public."group".id;


--
-- Name: group_person_relation; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.group_person_relation (
    person_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.group_person_relation OWNER TO youthmovement;

--
-- Name: leader; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.leader (
    user_account_id integer NOT NULL,
    person_id integer NOT NULL
);


ALTER TABLE public.leader OWNER TO youthmovement;

--
-- Name: parent; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.parent (
    user_account_id integer NOT NULL
);


ALTER TABLE public.parent OWNER TO youthmovement;

--
-- Name: parent_child_relation; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.parent_child_relation (
    parent_id integer NOT NULL,
    child_id integer NOT NULL
);


ALTER TABLE public.parent_child_relation OWNER TO youthmovement;

--
-- Name: person; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.person (
    id integer NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    gender public.gender_type,
    birthplace character varying,
    birthdate date NOT NULL,
    phone_number character varying,
    address_street text,
    address_zip character varying,
    address_place text,
    comments text
);


ALTER TABLE public.person OWNER TO youthmovement;

--
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: youthmovement
--

CREATE SEQUENCE public.person_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_id_seq OWNER TO youthmovement;

--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: youthmovement
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- Name: registration_codes; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.registration_codes (
    personid integer NOT NULL,
    registration_code character varying NOT NULL
);


ALTER TABLE public.registration_codes OWNER TO youthmovement;

--
-- Name: report; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.report (
    id integer NOT NULL,
    title text NOT NULL,
    date timestamp(6) without time zone DEFAULT LOCALTIMESTAMP NOT NULL,
    content text NOT NULL,
    written_by_id integer
);


ALTER TABLE public.report OWNER TO youthmovement;

--
-- Name: report_id_seq; Type: SEQUENCE; Schema: public; Owner: youthmovement
--

CREATE SEQUENCE public.report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.report_id_seq OWNER TO youthmovement;

--
-- Name: report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: youthmovement
--

ALTER SEQUENCE public.report_id_seq OWNED BY public.report.id;


--
-- Name: user_account; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.user_account (
    id integer NOT NULL,
    email text NOT NULL,
    password_hash character varying NOT NULL,
    permission bit(32) DEFAULT (0)::bit(32) NOT NULL
);


ALTER TABLE public.user_account OWNER TO youthmovement;

--
-- Name: user_account_id_seq; Type: SEQUENCE; Schema: public; Owner: youthmovement
--

CREATE SEQUENCE public.user_account_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_account_id_seq OWNER TO youthmovement;

--
-- Name: user_account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: youthmovement
--

ALTER SEQUENCE public.user_account_id_seq OWNED BY public.user_account.id;


--
-- Name: user_session; Type: TABLE; Schema: public; Owner: youthmovement
--

CREATE TABLE public.user_session (
    sid character varying NOT NULL,
    sess json NOT NULL,
    expire timestamp(6) without time zone NOT NULL
);


ALTER TABLE public.user_session OWNER TO youthmovement;

--
-- Name: activity id; Type: DEFAULT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.activity ALTER COLUMN id SET DEFAULT nextval('public.activity_id_seq'::regclass);


--
-- Name: bill id; Type: DEFAULT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.bill ALTER COLUMN id SET DEFAULT nextval('public.bill_id_seq'::regclass);


--
-- Name: checklist id; Type: DEFAULT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.checklist ALTER COLUMN id SET DEFAULT nextval('public.checklist_id_seq'::regclass);


--
-- Name: group id; Type: DEFAULT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public."group" ALTER COLUMN id SET DEFAULT nextval('public.group_id_seq'::regclass);


--
-- Name: person id; Type: DEFAULT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- Name: report id; Type: DEFAULT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.report ALTER COLUMN id SET DEFAULT nextval('public.report_id_seq'::regclass);


--
-- Name: user_account id; Type: DEFAULT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.user_account ALTER COLUMN id SET DEFAULT nextval('public.user_account_id_seq'::regclass);


--
-- Data for Name: activity; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.activity (id, title, description, registration_date, start_date, stop_date) FROM stdin;
1	BBQ	Winter BBQ. Hmmmm...	2019-01-15 23:00:00	2019-01-30 23:00:00	2019-01-30 23:00:00
2	Dierentuin	Met alle kinderren.	2019-08-08 22:00:00	2019-08-14 22:00:00	2019-08-14 22:00:00
3	Spaghettiavond	10€ per persoon.	\N	2019-03-31 22:00:00	2019-03-31 22:00:00
\.


--
-- Data for Name: activity_person_relation; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.activity_person_relation (person_id, activity_id) FROM stdin;
2	1
3	1
34	1
35	1
46	1
47	1
48	1
49	1
1	1
21	1
5	1
12	1
13	1
6	1
27	1
28	1
29	1
31	1
53	1
57	1
56	1
58	1
14	1
54	1
30	1
20	1
15	1
110	1
11	1
37	2
38	2
39	2
67	2
107	2
106	2
110	2
109	2
113	2
114	2
116	2
118	2
117	2
121	2
119	2
66	2
64	2
63	2
65	2
61	2
62	2
40	2
44	2
89	2
90	2
102	2
47	2
46	2
45	2
1	3
2	3
3	3
4	3
5	3
6	3
7	3
9	3
8	3
11	3
10	3
13	3
12	3
14	3
15	3
16	3
19	3
20	3
21	3
22	3
23	3
26	3
17	3
18	3
25	3
27	3
28	3
29	3
30	3
24	3
33	3
32	3
31	3
34	3
35	3
36	3
37	3
38	3
39	3
41	3
40	3
43	3
44	3
45	3
46	3
42	3
47	3
48	3
49	3
50	3
51	3
52	3
53	3
54	3
55	3
56	3
58	3
59	3
60	3
61	3
57	3
62	3
63	3
64	3
65	3
67	3
66	3
68	3
69	3
70	3
71	3
72	3
73	3
77	3
74	3
75	3
76	3
78	3
79	3
80	3
81	3
82	3
83	3
84	3
85	3
86	3
87	3
88	3
89	3
90	3
92	3
93	3
94	3
95	3
91	3
96	3
97	3
98	3
99	3
100	3
101	3
102	3
103	3
104	3
105	3
106	3
107	3
108	3
109	3
110	3
111	3
112	3
113	3
114	3
115	3
116	3
121	3
120	3
119	3
118	3
117	3
\.


--
-- Data for Name: bill; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.bill (id, description, created_on, amount) FROM stdin;
2	Kerstfeest	2019-01-06	16.00
3	Lidgeld (jaarlijks)	2019-01-06	100.00
4	Verplaatsingsvergoeding	2019-01-06	-2.00
5	Registratie	2019-01-06	4.60
\.


--
-- Data for Name: bill_person_relation; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.bill_person_relation (paid, person_id, bill_id) FROM stdin;
f	43	2
f	44	2
f	26	2
f	34	2
f	35	2
f	72	2
f	42	2
f	93	2
f	91	2
f	94	2
f	95	2
f	116	2
f	115	2
f	111	2
f	110	2
f	62	2
f	61	2
f	47	2
f	74	2
f	73	2
t	25	2
f	10	3
f	8	3
f	3	3
f	9	3
f	11	3
f	7	3
f	12	3
f	13	3
f	15	3
f	16	3
f	17	3
f	18	3
f	19	3
f	20	3
f	21	3
f	22	3
f	23	3
f	24	3
f	27	3
f	28	3
f	26	3
f	25	3
f	29	3
f	30	3
f	31	3
f	32	3
f	33	3
f	34	3
f	40	3
f	37	3
f	38	3
f	39	3
f	48	3
f	50	3
f	49	3
f	51	3
f	52	3
f	53	3
f	54	3
f	55	3
f	56	3
f	57	3
f	58	3
f	59	3
f	60	3
f	62	3
f	63	3
f	65	3
f	66	3
f	67	3
f	68	3
f	69	3
f	71	3
f	73	3
f	74	3
f	75	3
f	76	3
f	77	3
f	78	3
f	79	3
f	80	3
f	82	3
f	83	3
f	84	3
f	86	3
f	88	3
f	89	3
f	90	3
f	91	3
f	92	3
f	94	3
f	95	3
f	96	3
f	98	3
f	99	3
f	100	3
f	101	3
f	102	3
f	107	3
f	108	3
f	109	3
f	110	3
f	111	3
f	112	3
f	113	3
f	114	3
f	115	3
f	116	3
f	117	3
f	118	3
f	119	3
f	120	3
f	121	3
t	64	3
t	5	3
f	110	5
t	70	3
t	72	3
t	81	3
t	85	3
t	87	3
t	93	3
t	97	3
t	42	3
t	44	3
t	6	3
t	14	3
t	105	3
t	2	3
t	4	3
t	41	3
t	45	3
t	104	3
t	46	3
t	103	3
t	106	3
t	43	3
t	47	3
f	1	4
t	35	3
t	36	3
t	61	3
f	2	4
f	3	4
f	4	4
f	5	4
f	6	4
f	7	4
f	8	4
f	10	4
f	11	4
f	12	4
f	16	4
f	13	4
f	15	4
f	14	4
f	17	4
f	9	4
f	18	4
f	19	4
f	20	4
f	28	4
f	21	4
f	22	4
f	23	4
f	24	4
f	25	4
f	26	4
f	27	4
f	31	4
f	40	4
f	49	4
f	59	4
f	69	4
f	79	4
f	89	4
f	99	4
f	109	4
f	119	4
f	71	5
f	29	4
f	47	4
f	56	4
f	66	4
f	76	4
f	86	4
f	96	4
f	106	4
f	116	4
f	30	4
f	39	4
f	48	4
f	58	4
f	68	4
f	78	4
f	88	4
f	98	4
f	108	4
f	118	4
f	32	4
f	41	4
f	50	4
f	60	4
f	70	4
f	80	4
f	90	4
f	100	4
f	110	4
f	120	4
f	33	4
f	42	4
f	51	4
f	61	4
f	71	4
f	81	4
f	91	4
f	101	4
f	111	4
f	121	4
f	34	4
f	43	4
f	52	4
f	62	4
f	72	4
f	82	4
f	92	4
f	102	4
f	112	4
f	35	4
f	44	4
f	53	4
f	63	4
f	73	4
f	83	4
f	93	4
f	103	4
f	113	4
f	36	4
f	45	4
f	54	4
f	64	4
f	74	4
f	84	4
f	94	4
f	104	4
f	114	4
f	37	4
f	46	4
f	55	4
f	65	4
f	75	4
f	85	4
f	95	4
f	105	4
f	115	4
f	38	4
f	57	4
f	67	4
f	77	4
f	87	4
f	97	4
f	107	4
f	117	4
\.


--
-- Data for Name: checklist; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.checklist (id, title, description) FROM stdin;
1	Aanwezigen BBQ	
2	Huisbezoeken	Om bij te houden bij wie de huisbezoeken al zijn gebeurd.
\.


--
-- Data for Name: checklist_person_relation; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.checklist_person_relation (checked, person_id, checklist_id) FROM stdin;
f	21	1
f	31	1
f	34	1
f	35	1
f	48	1
f	49	1
f	53	1
f	54	1
f	56	1
f	58	1
f	57	1
f	5	1
f	6	1
f	20	1
f	15	1
t	2	1
t	3	1
t	30	1
f	34	2
f	35	2
t	1	1
t	1	2
t	9	2
t	10	2
t	12	1
t	12	2
t	14	1
t	14	2
t	17	2
t	24	2
t	26	2
t	27	1
t	27	2
t	28	1
t	28	2
t	29	1
t	29	2
t	37	2
t	38	2
t	39	2
t	40	2
t	42	2
t	45	2
t	43	2
t	46	1
t	46	2
t	61	2
t	62	2
t	63	2
t	50	2
t	47	1
t	47	2
t	51	2
t	52	2
t	68	2
t	64	2
t	65	2
t	66	2
t	67	2
t	102	2
t	106	2
t	90	2
t	107	2
t	109	2
t	110	2
t	113	2
t	114	2
t	117	2
t	118	2
t	119	2
t	121	2
f	44	2
f	89	2
f	116	2
f	11	2
f	13	1
f	13	2
f	25	2
\.


--
-- Data for Name: child; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.child (person_id, registration_pending) FROM stdin;
2	t
3	t
4	t
5	t
6	t
7	t
8	t
9	t
10	t
11	t
12	t
13	t
14	t
15	t
16	t
17	t
18	t
19	t
20	t
21	t
22	t
23	t
24	t
25	t
26	t
27	t
28	t
29	t
30	t
31	t
32	t
33	t
34	t
35	t
36	t
37	t
38	t
39	t
40	t
41	t
42	t
43	t
44	t
45	t
46	t
47	t
48	t
49	t
50	t
51	t
52	t
53	t
54	t
55	t
56	t
57	t
58	t
59	t
60	t
61	t
62	t
63	t
64	t
65	t
66	t
67	t
68	t
69	t
70	t
71	t
72	t
73	t
74	t
75	t
76	t
77	t
78	t
79	t
80	t
81	t
82	t
83	t
84	t
85	t
86	t
87	t
88	t
89	t
90	t
91	t
92	t
93	t
94	t
95	t
96	t
97	t
98	t
99	t
100	t
101	t
\.


--
-- Data for Name: group; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public."group" (id, title, description) FROM stdin;
1	Zaterdaggroep	
2	Deugnieten	
3	Avonturiers	Voor degenen die draken bevechten.
\.


--
-- Data for Name: group_person_relation; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.group_person_relation (person_id, group_id) FROM stdin;
6	1
12	1
31	1
29	1
14	1
15	1
28	1
47	1
27	1
48	1
57	1
53	1
56	1
54	1
58	1
13	1
20	1
30	1
21	1
2	1
5	1
3	1
1	1
37	2
38	2
39	2
40	2
61	2
62	2
63	2
64	2
66	2
65	2
67	2
106	2
107	2
109	2
110	2
113	2
114	2
116	2
117	2
119	2
121	2
118	2
1	3
10	3
11	3
9	3
13	3
12	3
14	3
17	3
24	3
25	3
26	3
27	3
28	3
29	3
34	3
35	3
42	3
43	3
46	3
47	3
52	3
51	3
50	3
61	3
62	3
64	3
65	3
67	3
68	3
110	1
\.


--
-- Data for Name: leader; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.leader (user_account_id, person_id) FROM stdin;
1	1
2	102
3	103
4	104
5	105
6	106
7	107
8	108
9	109
10	110
11	111
12	112
13	113
14	114
15	115
16	116
17	117
18	118
19	119
20	120
21	121
\.


--
-- Data for Name: parent; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.parent (user_account_id) FROM stdin;
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
\.


--
-- Data for Name: parent_child_relation; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.parent_child_relation (parent_id, child_id) FROM stdin;
22	11
23	35
23	80
24	39
24	84
25	60
26	23
27	43
27	88
28	53
29	44
29	89
30	40
30	85
31	30
31	75
32	66
33	15
34	37
34	82
35	48
35	93
36	49
36	94
37	99
38	26
38	71
39	29
39	74
40	33
40	78
41	3
42	22
43	64
44	68
45	45
45	90
46	61
47	62
48	46
48	91
49	51
49	96
50	54
51	31
51	76
\.


--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.person (id, first_name, last_name, gender, birthplace, birthdate, phone_number, address_street, address_zip, address_place, comments) FROM stdin;
1	admin	admin	\N		2019-01-06					
2	Michael	Smith	\N		2005-02-01					
3	James	Johnson	\N		2006-05-27					
4	John	Williams	\N		2010-11-07					
5	Robert	Brown	\N		2005-05-24					
6	David	Jones	\N		2005-11-12					
7	William	Garcia	\N		2004-03-28					
8	Mary	Rodriguez	\N		2004-04-05					
9	Christopher	Miller	\N		2008-08-15					
10	Joseph	Martinez	\N		2009-01-04					
11	Richard	Davis	\N		2003-10-22					
12	Daniel	Hernandez	\N		2009-10-01					
13	Thomas	Lopez	\N		2004-06-24					
14	Matthew	Gonzalez	\N		2008-02-04					
15	Jennifer	Wilson	\N		2005-02-18					
16	Charles	Anderson	\N		2011-08-24					
17	Anthony	Thomas	\N		2003-01-26					
18	Patricia	Taylor	\N		2008-10-04					
19	Linda	Lee	\N		2003-03-20					
20	Mark	Moore	\N		2010-04-14					
21	Elizabeth	Jackson	\N		2010-03-08					
22	Joshua	Perez	\N		2012-04-13					
23	Steven	Martin	\N		2012-07-26					
24	Andrew	Thompson	\N		2007-07-19					
25	Kevin	White	\N		2010-06-13					
26	Brian	Sanchez	\N		2009-02-22					
27	Barbara	Harris	\N		2009-04-10					
28	Jessica	Ramirez	\N		2003-05-26					
29	Jason	Clark	\N		2003-04-04					
30	Susan	Lewis	\N		2003-10-24					
31	Timothy	Robinson	\N		2006-05-15					
32	Paul	Walker	\N		2010-08-07					
33	Kenneth	Young	\N		2007-11-17					
34	Lisa	Hall	\N		2012-05-25					
35	Ryan	Allen	\N		2011-03-11					
36	Sarah	Torres	\N		2005-01-25					
37	Karen	Nguyen	\N		2003-11-18					
38	Jeffrey	Wright	\N		2009-11-11					
39	Donald	Flores	\N		2011-07-17					
40	Ashley	King	\N		2006-02-19					
41	Eric	Scott	\N		2006-04-06					
42	Jacob	Rivera	\N		2011-05-18					
43	Nicholas	Green	\N		2010-08-23					
44	Jonathan	Hill	\N		2005-06-23					
45	Ronald	Adams	\N		2006-03-04					
46	Michelle	Baker	\N		2012-09-13					
47	Kimberly	Nelson	\N		2007-11-16					
48	Nancy	Mitchell	\N		2008-02-13					
49	Justin	Campbell	\N		2012-08-13					
50	Sandra	Gomez	\N		2003-11-21					
51	Amanda	Carter	\N		2005-05-18					
52	Brandon	Roberts	\N		2012-08-01					
53	Stephanie	Diaz	\N		2010-08-01					
54	Emily	Phillips	\N		2003-10-28					
55	Melissa	Evans	\N		2004-09-11					
56	Gary	Turner	\N		2004-07-26					
57	Edward	Reyes	\N		2004-09-25					
58	Stephen	Cruz	\N		2012-11-03					
59	Scott	Parker	\N		2012-02-18					
60	George	Edwards	\N		2008-11-20					
61	Donna	Collins	\N		2003-02-02					
62	Jose	Stewart	\N		2004-06-05					
63	Rebecca	Morris	\N		2011-07-27					
64	Deborah	Morales	\N		2010-10-18					
65	Laura	Ortiz	\N		2009-05-22					
66	Cynthia	Gutierrez	\N		2007-02-28					
67	Carol	Ruiz	\N		2006-06-23					
68	Amy	Brooks	\N		2007-11-11					
69	Margaret	Thompson	\N		2006-06-09					
70	Gregory	White	\N		2011-09-13					
71	Sharon	Sanchez	\N		2011-08-07					
72	Larry	Harris	\N		2006-04-13					
73	Angela	Ramirez	\N		2010-04-22					
74	Maria	Clark	\N		2004-06-28					
75	Alexander	Lewis	\N		2006-11-23					
76	Benjamin	Robinson	\N		2010-03-05					
77	Nicole	Walker	\N		2012-08-07					
78	Kathleen	Young	\N		2010-01-22					
79	Patrick	Hall	\N		2012-05-13					
80	Samantha	Allen	\N		2011-08-13					
81	Tyler	Torres	\N		2011-10-25					
82	Samuel	Nguyen	\N		2006-01-22					
83	Betty	Wright	\N		2004-10-17					
84	Brenda	Flores	\N		2012-09-01					
85	Pamela	King	\N		2010-11-02					
86	Aaron	Scott	\N		2011-07-10					
87	Kelly	Rivera	\N		2003-09-18					
88	Heather	Green	\N		2006-07-17					
89	Rachel	Hill	\N		2007-03-19					
90	Adam	Adams	\N		2012-02-14					
91	Christine	Baker	\N		2008-03-01					
92	Zachary	Nelson	\N		2008-10-21					
93	Debra	Mitchell	\N		2003-07-19					
94	Katherine	Campbell	\N		2008-01-04					
95	Dennis	Gomez	\N		2005-10-15					
96	Nathan	Carter	\N		2003-04-15					
97	Christina	Cook	\N		2004-05-24					
98	Julie	Kim	\N		2006-08-03					
99	Jordan	Morgan	\N		2003-11-14					
100	Kyle	Cooper	\N		2004-06-12					
101	Anna	Ramos	\N		2009-01-08					
102	Amanda	Peterson	\N		2000-10-01					
103	Brandon	Gonzales	\N		2001-09-09					
104	Stephanie	Bell	\N		2000-11-14					
105	Emily	Reed	\N		2001-04-26					
106	Melissa	Bailey	\N		1999-05-17					
107	Gary	Chavez	\N		2000-04-11					
108	Edward	Kelly	\N		2001-11-15					
109	Stephen	Howard	\N		2000-01-12					
110	Scott	Richardson	\N		1998-11-21					
111	George	Ward	\N		1998-03-10					
112	Donna	Cox	\N		1998-06-20					
113	Jose	Ruiz	\N		1998-10-18					
114	Rebecca	Brooks	\N		1999-07-07					
115	Deborah	Watson	\N		1998-08-21					
116	Laura	Wood	\N		2001-07-18					
117	Cynthia	James	\N		2001-08-26					
118	Carol	Gonzalez	\N		1999-01-25					
119	Amy	Wilson	\N		2001-03-21					
120	Margaret	Anderson	\N		1998-02-10					
121	Gregory	Adams	\N		1998-07-08					
\.


--
-- Data for Name: registration_codes; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.registration_codes (personid, registration_code) FROM stdin;
\.


--
-- Data for Name: report; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.report (id, title, date, content, written_by_id) FROM stdin;
1	Verslag vergadering januari	2019-01-06 06:57:15.948	6 januari 2019\nVogelsanck, Hasselt\n\n# Vergadering nieuw jaar\nWe verwachten *veel* nieuwe leden dit jaar.\n\nGelukkig nieuwjaar 🎉🎊🎉\nGlühwein in Köln!	1
2	Benodigdheden Kerstfeest	2019-01-06 10:57:50.102	1.  Kerstboom\n2.  Rekeningen\n3.  Cadeautjes\n4.  Muziek\n5.  Eten & drinken\n6. Reservatie zaal	1
\.


--
-- Data for Name: user_account; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.user_account (id, email, password_hash, permission) FROM stdin;
2	amanda.peterson@facebook.com		00000000000000000000000000000000
3	brandon.gonzales@twitter.com		00000000000000000000000000000000
4	stephanie.bell@google.com		00000000000000000000000000000000
5	emily.reed@youtube.com		00000000000000000000000000000000
6	melissa.bailey@instagram.com		00000000000000000000000000000000
7	gary.chavez@linkedin.com		00000000000000000000000000000000
8	edward.kelly@wordpress.org		00000000000000000000000000000000
9	stephen.howard@pinterest.com		00000000000000000000000000000000
11	george.ward@wordpress.com		00000000000000000000000000000000
12	donna.cox@blogspot.com		00000000000000000000000000000000
13	jose.ruiz@apple.com		00000000000000000000000000000000
14	rebecca.brooks@adobe.com		00000000000000000000000000000000
15	deborah.watson@tumblr.com		00000000000000000000000000000000
16	laura.wood@youtu.be		00000000000000000000000000000000
17	cynthia.james@amazon.com		00000000000000000000000000000000
18	carol.gonzalez@goo.gl		00000000000000000000000000000000
19	amy.wilson@vimeo.com		00000000000000000000000000000000
20	margaret.anderson@flickr.com		00000000000000000000000000000000
21	gregory.adams@microsoft.com		00000000000000000000000000000000
23	larry.allen@godaddy.com		00000000000000000000000000000000
24	angela.flores@qq.com		00000000000000000000000000000000
25	maria.edwards@bit.ly		00000000000000000000000000000000
26	alexander.martin@vk.com		00000000000000000000000000000000
27	benjamin.green@reddit.com		00000000000000000000000000000000
28	nicole.diaz@w3.org		00000000000000000000000000000000
29	kathleen.hill@baidu.com		00000000000000000000000000000000
30	patrick.king@nytimes.com		00000000000000000000000000000000
31	samantha.lewis@t.co.uk		00000000000000000000000000000000
32	tyler.gutierrez@europa.eu		00000000000000000000000000000000
33	samuel.wilson@buydomains.com		00000000000000000000000000000000
34	betty.nguyen@wp.com		00000000000000000000000000000000
35	brenda.mitchell@statcounter.com		00000000000000000000000000000000
36	pamela.campbell@miitbeian.gov		00000000000000000000000000000000
37	aaron.morgan@jimdo.com		00000000000000000000000000000000
38	kelly.sanchez@blogger.com		00000000000000000000000000000000
39	heather.clark@github.com		00000000000000000000000000000000
40	rachel.young@weebly.com		00000000000000000000000000000000
41	adam.johnson@soundcloud.com		00000000000000000000000000000000
42	christine.perez@mozilla.org		00000000000000000000000000000000
43	zachary.morales@bbc.co.uk		00000000000000000000000000000000
44	debra.brooks@yandex.ru		00000000000000000000000000000000
45	katherine.adams@myspace.com		00000000000000000000000000000000
46	dennis.collins@google.de		00000000000000000000000000000000
47	nathan.stewart@addthis.com		00000000000000000000000000000000
48	christina.baker@nih.gov		00000000000000000000000000000000
49	julie.carter@theguardian.com		00000000000000000000000000000000
50	jordan.phillips@google.co.uk		00000000000000000000000000000000
51	kyle.robinson@cnn.com		00000000000000000000000000000000
22	sharon.davis@yahoo.com	736372797074000e000000080000000178c986f331ba73c2d6a22e6e797f640caa6bac19620556e584c11bad7cdaa47cc47b6c3e384b9ff3d16821c60ea983d4b03d45c075b444ae5f81bfef8ab0aacc38bf68f410123d2df04e89982261b219	00000000000000000000000000000000
10	scott.richardson@wikipedia.org	736372797074000e000000080000000178c986f331ba73c2d6a22e6e797f640caa6bac19620556e584c11bad7cdaa47cc47b6c3e384b9ff3d16821c60ea983d4b03d45c075b444ae5f81bfef8ab0aacc38bf68f410123d2df04e89982261b219	00000000000000000000110000110000
1	admin	736372797074000e000000080000000178c986f331ba73c2d6a22e6e797f640caa6bac19620556e584c11bad7cdaa47cc47b6c3e384b9ff3d16821c60ea983d4b03d45c075b444ae5f81bfef8ab0aacc38bf68f410123d2df04e89982261b219	11111111111111111111111111111111
\.


--
-- Data for Name: user_session; Type: TABLE DATA; Schema: public; Owner: youthmovement
--

COPY public.user_session (sid, sess, expire) FROM stdin;
z4_cuEKbehEzuIsOLgx1IfRUf33mOaJY	{"cookie":{"originalMaxAge":7200000,"expires":"2019-01-06T11:18:29.750Z","secure":true,"httpOnly":true,"domain":"lab.wardsegers.be","path":"/"},"loggedInId":1}	2019-01-06 12:57:56
fXsXWBBYx30hZgA3AAIqxDmvRA6n0Vpx	{"cookie":{"originalMaxAge":7200000,"expires":"2019-01-06T11:33:28.503Z","secure":true,"httpOnly":true,"domain":"lab.wardsegers.be","path":"/"},"loggedInId":1}	2019-01-06 11:33:29
vAsR7jl2uH10-0FbEI3pFynPh9Yh9Zm_	{"cookie":{"originalMaxAge":7200000,"expires":"2019-01-06T10:27:56.536Z","secure":true,"httpOnly":true,"domain":"lab.wardsegers.be","path":"/"},"loggedInId":1}	2019-01-06 11:50:22
vvrN-hJH1Ggf5XQDR66tXPx3GiYeheNc	{"cookie":{"originalMaxAge":7200000,"expires":"2019-01-06T11:35:15.576Z","secure":true,"httpOnly":true,"domain":"lab.wardsegers.be","path":"/"},"loggedInId":1}	2019-01-06 11:37:50
\.


--
-- Name: activity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: youthmovement
--

SELECT pg_catalog.setval('public.activity_id_seq', 3, true);


--
-- Name: bill_id_seq; Type: SEQUENCE SET; Schema: public; Owner: youthmovement
--

SELECT pg_catalog.setval('public.bill_id_seq', 5, true);


--
-- Name: checklist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: youthmovement
--

SELECT pg_catalog.setval('public.checklist_id_seq', 2, true);


--
-- Name: group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: youthmovement
--

SELECT pg_catalog.setval('public.group_id_seq', 3, true);


--
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: youthmovement
--

SELECT pg_catalog.setval('public.person_id_seq', 121, true);


--
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: youthmovement
--

SELECT pg_catalog.setval('public.report_id_seq', 2, true);


--
-- Name: user_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: youthmovement
--

SELECT pg_catalog.setval('public.user_account_id_seq', 51, true);


--
-- Name: activity_person_relation activity_person_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.activity_person_relation
    ADD CONSTRAINT activity_person_relation_pkey PRIMARY KEY (person_id, activity_id);


--
-- Name: activity activity_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT activity_pkey PRIMARY KEY (id);


--
-- Name: bill_person_relation bill_person_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.bill_person_relation
    ADD CONSTRAINT bill_person_relation_pkey PRIMARY KEY (person_id, bill_id);


--
-- Name: bill bill_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.bill
    ADD CONSTRAINT bill_pkey PRIMARY KEY (id);


--
-- Name: checklist_person_relation checklist_person_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.checklist_person_relation
    ADD CONSTRAINT checklist_person_relation_pkey PRIMARY KEY (person_id, checklist_id);


--
-- Name: checklist checklist_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.checklist
    ADD CONSTRAINT checklist_pkey PRIMARY KEY (id);


--
-- Name: checklist checklist_title_key; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.checklist
    ADD CONSTRAINT checklist_title_key UNIQUE (title);


--
-- Name: child child_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.child
    ADD CONSTRAINT child_pkey PRIMARY KEY (person_id);


--
-- Name: group_person_relation group_person_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.group_person_relation
    ADD CONSTRAINT group_person_relation_pkey PRIMARY KEY (person_id, group_id);


--
-- Name: group group_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public."group"
    ADD CONSTRAINT group_pkey PRIMARY KEY (id);


--
-- Name: group group_title_key; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public."group"
    ADD CONSTRAINT group_title_key UNIQUE (title);


--
-- Name: leader leader_person_id_key; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.leader
    ADD CONSTRAINT leader_person_id_key UNIQUE (person_id);


--
-- Name: leader leader_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.leader
    ADD CONSTRAINT leader_pkey PRIMARY KEY (user_account_id, person_id);


--
-- Name: leader leader_user_account_id_key; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.leader
    ADD CONSTRAINT leader_user_account_id_key UNIQUE (user_account_id);


--
-- Name: parent_child_relation parent_child_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.parent_child_relation
    ADD CONSTRAINT parent_child_relation_pkey PRIMARY KEY (parent_id, child_id);


--
-- Name: parent parent_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.parent
    ADD CONSTRAINT parent_pkey PRIMARY KEY (user_account_id);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- Name: registration_codes registration_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.registration_codes
    ADD CONSTRAINT registration_codes_pkey PRIMARY KEY (registration_code);


--
-- Name: report report_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT report_pkey PRIMARY KEY (id);


--
-- Name: user_session session_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.user_session
    ADD CONSTRAINT session_pkey PRIMARY KEY (sid);


--
-- Name: user_account user_account_email_key; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.user_account
    ADD CONSTRAINT user_account_email_key UNIQUE (email);


--
-- Name: user_account user_account_pkey; Type: CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.user_account
    ADD CONSTRAINT user_account_pkey PRIMARY KEY (id);


--
-- Name: activity_person_relation activity_person_relation_activity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.activity_person_relation
    ADD CONSTRAINT activity_person_relation_activity_id_fkey FOREIGN KEY (activity_id) REFERENCES public.activity(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: activity_person_relation activity_person_relation_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.activity_person_relation
    ADD CONSTRAINT activity_person_relation_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: bill_person_relation bill_person_relation_bill_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.bill_person_relation
    ADD CONSTRAINT bill_person_relation_bill_id_fkey FOREIGN KEY (bill_id) REFERENCES public.bill(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: bill_person_relation bill_person_relation_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.bill_person_relation
    ADD CONSTRAINT bill_person_relation_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: checklist_person_relation checklist_person_relation_checklist_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.checklist_person_relation
    ADD CONSTRAINT checklist_person_relation_checklist_id_fkey FOREIGN KEY (checklist_id) REFERENCES public.checklist(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: checklist_person_relation checklist_person_relation_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.checklist_person_relation
    ADD CONSTRAINT checklist_person_relation_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: child child_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.child
    ADD CONSTRAINT child_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: group_person_relation group_person_relation_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.group_person_relation
    ADD CONSTRAINT group_person_relation_group_id_fkey FOREIGN KEY (group_id) REFERENCES public."group"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: group_person_relation group_person_relation_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.group_person_relation
    ADD CONSTRAINT group_person_relation_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: leader leader_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.leader
    ADD CONSTRAINT leader_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: leader leader_user_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.leader
    ADD CONSTRAINT leader_user_account_id_fkey FOREIGN KEY (user_account_id) REFERENCES public.user_account(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: parent_child_relation parent_child_relation_child_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.parent_child_relation
    ADD CONSTRAINT parent_child_relation_child_id_fkey FOREIGN KEY (child_id) REFERENCES public.child(person_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: parent_child_relation parent_child_relation_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.parent_child_relation
    ADD CONSTRAINT parent_child_relation_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.parent(user_account_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: parent parent_user_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.parent
    ADD CONSTRAINT parent_user_account_id_fkey FOREIGN KEY (user_account_id) REFERENCES public.user_account(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: registration_codes registration_codes_personid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.registration_codes
    ADD CONSTRAINT registration_codes_personid_fkey FOREIGN KEY (personid) REFERENCES public.child(person_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: report report_written_by_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: youthmovement
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT report_written_by_id_fkey FOREIGN KEY (written_by_id) REFERENCES public.leader(person_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--

