CREATE TABLE user_account (
    id SERIAL PRIMARY KEY,
    email TEXT NOT NULL UNIQUE,
    password_hash VARCHAR NOT NULL,
    permission bit(32) NOT NULL DEFAULT (0::bit(32))
);

CREATE TABLE parent (
    user_account_id INTEGER PRIMARY KEY REFERENCES user_account(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TYPE gender_type AS ENUM ('Male', 'Female', 'Other');

CREATE TABLE person (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    gender GENDER_TYPE,
    birthplace VARCHAR,
    birthdate DATE NOT NULL,
    phone_number VARCHAR,
    address_street TEXT,
    address_zip VARCHAR,
    address_place TEXT,
    comments TEXT
);

CREATE TABLE leader (
    user_account_id INTEGER UNIQUE REFERENCES user_account(id) ON UPDATE CASCADE ON DELETE CASCADE,
    person_id INTEGER UNIQUE REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY (user_account_id, person_id)
);

CREATE TABLE child (
    person_id INTEGER PRIMARY KEY REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE,
    registration_pending BOOLEAN NOT NULL DEFAULT true
);

CREATE TABLE parent_child_relation (
    parent_id INTEGER NOT NULL REFERENCES parent(user_account_id) ON UPDATE CASCADE ON DELETE CASCADE,
    child_id INTEGER NOT NULL REFERENCES child(person_id) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY (parent_id, child_id)
);

CREATE TABLE checklist (
    id SERIAL PRIMARY KEY,
    title TEXT NOT NULL UNIQUE,
    description TEXT
);

CREATE TABLE checklist_person_relation (
    checked BOOLEAN NOT NULL DEFAULT false,
    person_id INTEGER NOT NULL REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE,
    checklist_id INTEGER NOT NULL REFERENCES checklist(id) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY (person_id, checklist_id)
);

CREATE TABLE "group" (
    id SERIAL PRIMARY KEY,
    title TEXT NOT NULL UNIQUE,
    description TEXT
);

CREATE TABLE group_person_relation (
    person_id INTEGER NOT NULL REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE,
    group_id INTEGER NOT NULL REFERENCES "group"(id) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY (person_id, group_id)
);

CREATE TABLE activity (
    id SERIAL PRIMARY KEY,
    title TEXT NOT NULL,
    description TEXT,
    registration_date TIMESTAMP(6) DEFAULT null,
    start_date TIMESTAMP(6) NOT NULL DEFAULT localtimestamp,
    stop_date TIMESTAMP(6) NOT NULL DEFAULT localtimestamp
);

CREATE TABLE activity_person_relation (
    person_id INTEGER NOT NULL REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE,
    activity_id INTEGER NOT NULL REFERENCES activity(id) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY (person_id, activity_id)
);

CREATE TABLE report (
    id SERIAL PRIMARY KEY,
    title TEXT NOT NULL,
    date TIMESTAMP(6) NOT NULL DEFAULT localtimestamp,
    content TEXT NOT NULL,
    written_by_id INTEGER REFERENCES leader(person_id) ON UPDATE CASCADE ON DELETE SET NULL
);

CREATE TABLE bill (
    id SERIAL PRIMARY KEY,
    description TEXT NOT NULL,
    created_on DATE NOT NULL,
    amount NUMERIC(1000, 2) NOT NULL
);

CREATE TABLE bill_person_relation (
    paid BOOLEAN NOT NULL DEFAULT false,
    person_id INTEGER NOT NULL REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE,
    bill_id INTEGER NOT NULL REFERENCES bill(id) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY (person_id, bill_id)
);

CREATE TABLE registration_codes (
	personid INTEGER NOT NULL REFERENCES child(person_id) ON UPDATE CASCADE ON DELETE CASCADE,
	registration_code VARCHAR NOT NULL UNIQUE,
	PRIMARY KEY (registration_code)
);

-- From connect-pg-simple
CREATE TABLE user_session (
    "sid" varchar NOT NULL COLLATE "default",
    "sess" json NOT NULL,
    "expire" timestamp(6) NOT NULL
) WITH (OIDS=FALSE);
ALTER TABLE user_session ADD CONSTRAINT "session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE;


INSERT INTO
    user_account (
        password_hash,
        email,
        permission
    )
VALUES
    (
        -- password: admin
        '736372797074000e000000080000000178c986f331ba73c2d6a22e6e797f640caa6bac19620556e584c11bad7cdaa47cc47b6c3e384b9ff3d16821c60ea983d4b03d45c075b444ae5f81bfef8ab0aacc38bf68f410123d2df04e89982261b219',
        'admin',
        ~(0::bit(32))
    );
INSERT INTO
    person (
        first_name,
        last_name,
        gender,
        birthplace,
        birthdate,
        phone_number,
        address_street,
        address_zip,
        address_place,
        comments
    )
VALUES (
    'admin',
    'admin',
    NULL,
    '',
    localtimestamp,
    '',
    '',
    '',
    '',
    ''
);
INSERT INTO leader (user_account_id, person_id)
VALUES (1, 1);