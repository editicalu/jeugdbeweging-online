# Privacybeleid template

> **Opmerking voor de jeugdbeweging**
>
> Dit is een template, dat kan gebruikt worden om een privacybeleid te maken voor je jeugdbeweging. Mogelijks moet je nog een aantal dingen veranderen.
>
> Vergeet zeker niet alles in `{{ }}` te vervangen door de gegevens die voor jullie van toepassing zijn.
>

# Welke gegevens verzamelen we?

> **Opmerking voor de jeugdbeweging**
>
> Controleer dit zeker! Als je zelf gegevens verzamelt of aanvult, pas je dit hieronder best aan.
>

We verzamelen enkel de volgende gegevens:

Kinderen:
- naam
- geboorteplaats en -datum
- groepen waarin het kind zit
- geslacht*
- adres*
- telefoonnummer*

Data gemarkeerd met * is optioneel. Enkel als de ouder het wenst door te geven, verzamelen we deze gegevens.

Deze gegevens zijn nodig om onze werking te garanderen en (in geval van nood) ouders te kunnen contacteren.

## Ouders met een account

Voor ouders met een account verzamelen we de volgende gegevens:

- email
- wachtwoord
- wie je kinderen zijn

Deze gegevens gebruiken we om je te laten inloggen en (indien nodig) te kunnen e-mailen.

# Met wie delen we deze gegevens?

> **Opmerking voor de jeugdbeweging**
>
> Pas dit aan als je gegevens doorgeeft, bv. aan de verzekering.

We delen deze gegevens enkel met {{ hosting provider }}. We hebben met hen een verwerkersovereenkomst, die ons garandeert dat de data wettelijk wordt verwerkt.

# Hoe lang houden we deze gegevens bij?

We houden deze gegevens bij zolang dit nodig is om onze werking te garanderen. M.a.w. zolang je kind lid is van {{ jeugdbeweging }}, blijven de gegevens in ons systeem staan.

Gegevens van ouders (accounts) houden we bij zolang het account bestaat. Zodra je (zoals hieronder vermeld) vraagt om je account te verwijderen, worden het account gewist.

# Hoe kan je je rechten uitoefenen?

## Opvragen van data

Je hebt het recht om alle gegevens die we over je hebben op te vragen.

Als je een account hebt, kan je dit zelf doen via de applicatie. Log in en ga naar `Account Beheren`. Onderaan kan je klikken op `Download alle data gelinkt aan dit account`.

Als je geen account hebt, kan je een mail sturen naar {{ administrator }}. Hij zal je binnen 5 werkdagen een antwoord geven met alle data die we over je kinderen hebben.

## Aanpassing van data

Als je een account hebt, kan je alle data van jou en jouw kinderen bekijken en aanpassen, mocht deze niet juist zijn.

Als je geen account hebt, stuur je een mail naar {{ administrator }} met de aan te passen gegevens. Hij zal de gegevens aanpassen.

## Verwijdering van data

Je hebt het recht om al je gegevens expliciet uit ons systeem te laten verwijderen. Om deze rechten uit te oefenen, stuur je een mail naar {{ administrator }}. Hij zal je gegevens laten verwijderen.

Als je als ouder een account hebt, heb je ook de mogelijkheid om enkel je account te verwijderen, maar je kinderen in het systeem te laten staan. Laat weten aan de administrator of je dit wilt.

## Wijzigingen aan dit privacybeleid

> **Opmerking voor de jeugdbeweging**
>
> Pas dit aan als je op een andere manier je ouders op een andere manier wilt inlichten.

Bij wijzigingen aan dit privacybeleid zullen we je een mail sturen met het gewijzigde beleid (als je geregistreerd bent). Ook zullen we dit duidelijk aangeven op onze werking. We laten je dit 14 dagen op voorhand weten en je hebt altijd het recht je uit te schrijven indien je het niet eens bent met het gewijzigde privacybeleid.
