import 'mocha';
import '../src/lib/StringExtend';

describe('String prototype extension tests', () => {
  describe('insensitiveIncludes tests', () => {
    describe('#caseInsensitive', () => {
      it('should confirm that the lowercase substr is part of the uppercase main string.', () => {
        const baseStr: String = 'foo BAR';
        baseStr.insensitiveIncludes(' bar');
      });
    });

    describe('#localeInsensitive', () => {
      it('should confirm that the substr is part of the main string, even though the locale of the strings does not match.', () => {
        const baseStr: String = 'áßéíóúüñ';
        baseStr.insensitiveIncludes('iou');
      });
    });

    describe('#allInsensitive', () => {
      it('should confirm that the mixed case substr is part of the (differently) mixed case main string, even though the locale of the strings does not match.', () => {
        const baseStr: String = 'ßéíÁßéíÓúüñíóú';
        baseStr.insensitiveIncludes('aSSeIoUun');
      });
    });

    describe('#localeInsensitiveExtreme', () => {
      it('tests whether a non-latin alphabet substr is included in the non-latin alphabet mainstr.', () => {
        const baseStr: String = '官话';
        baseStr.insensitiveIncludes('话');
      });
    });
  });
});
