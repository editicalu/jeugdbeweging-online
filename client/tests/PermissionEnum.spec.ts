import assert from 'assert';
import PermissionEnum from '../src/lib/PermissionEnum';

describe('Permission', () => {
  describe('#readAllPermissions', () => {
    it('should say that all permissions still have the same id', () => {
      // This test is to ensure no-one accidently changes the values, as this could break an
      // existing database.
      // Add new permissions to this test.
      assert(PermissionEnum.ChildrenViewer === 0);
      assert(PermissionEnum.ChildrenAdministrator === 1);
      assert(PermissionEnum.LeaderAdministrator === 2);
      assert(PermissionEnum.ParentAdministrator === 3);
      assert(PermissionEnum.Financial === 4);
      assert(PermissionEnum.Reporter === 5);
      assert(PermissionEnum.Grouper === 6);
      assert(PermissionEnum.Checklister === 11);
    });
  });
});
