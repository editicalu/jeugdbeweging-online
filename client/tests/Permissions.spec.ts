import assert from 'assert';
import PermissionEnum from '../src/lib/PermissionEnum';
import Permissions from '../src/lib/Permissions';

describe('Permissions', () => {
  describe('#newPermissions', () => {
    it('should give no permissions at all', () => {
      const permissions = Permissions.empty();
      const keys: number[] = Object.keys(PermissionEnum)
        .filter(v => /^[0-9]+$/gi.test(v)).map(item => Number(item)) as number[];
      keys
        .forEach((item: number) => {
          assert(!permissions.hasPermission(item));
        });
      assert.deepEqual(permissions.allPermissions(), []);
      assert.equal(permissions.toHexString(), '0x00000000');
    });
  });

  describe('#onePermission', () => {
    it('should only assign the ChildrenViewer permission', () => {
      const permissions = Permissions.empty();
      permissions.addPermission(PermissionEnum.ChildrenViewer);

      assert(permissions.hasPermission(PermissionEnum.ChildrenViewer) === true);
      assert(permissions.hasPermission(PermissionEnum.ChildrenAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.LeaderAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.ParentAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.Financial) === false);
      assert(permissions.hasPermission(PermissionEnum.Reporter) === false);
      assert(permissions.hasPermission(PermissionEnum.Grouper) === false);
    });
    it('should only assign the ChildrenAdministrator permission', () => {
      const permissions = Permissions.empty();
      permissions.addPermission(PermissionEnum.ChildrenAdministrator);

      assert(permissions.hasPermission(PermissionEnum.ChildrenViewer) === false);
      assert(permissions.hasPermission(PermissionEnum.ChildrenAdministrator) === true);
      assert(permissions.hasPermission(PermissionEnum.LeaderAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.ParentAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.Financial) === false);
      assert(permissions.hasPermission(PermissionEnum.Reporter) === false);
      assert(permissions.hasPermission(PermissionEnum.Grouper) === false);
    });
    it('should only assign the LeaderAdministrator permission', () => {
      const permissions = Permissions.empty();
      permissions.addPermission(PermissionEnum.LeaderAdministrator);

      assert(permissions.hasPermission(PermissionEnum.ChildrenViewer) === false);
      assert(permissions.hasPermission(PermissionEnum.ChildrenAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.LeaderAdministrator) === true);
      assert(permissions.hasPermission(PermissionEnum.ParentAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.Financial) === false);
      assert(permissions.hasPermission(PermissionEnum.Reporter) === false);
      assert(permissions.hasPermission(PermissionEnum.Grouper) === false);
    });
    it('should only assign the ParentAdministrator permission', () => {
      const permissions = Permissions.empty();
      permissions.addPermission(PermissionEnum.ParentAdministrator);

      assert(permissions.hasPermission(PermissionEnum.ChildrenViewer) === false);
      assert(permissions.hasPermission(PermissionEnum.ChildrenAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.LeaderAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.ParentAdministrator) === true);
      assert(permissions.hasPermission(PermissionEnum.Financial) === false);
      assert(permissions.hasPermission(PermissionEnum.Reporter) === false);
      assert(permissions.hasPermission(PermissionEnum.Grouper) === false);
    });
    it('should only assign the Financial permission', () => {
      const permissions = Permissions.empty();
      permissions.addPermission(PermissionEnum.Financial);

      assert(permissions.hasPermission(PermissionEnum.ChildrenViewer) === false);
      assert(permissions.hasPermission(PermissionEnum.ChildrenAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.LeaderAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.ParentAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.Financial) === true);
      assert(permissions.hasPermission(PermissionEnum.Reporter) === false);
      assert(permissions.hasPermission(PermissionEnum.Grouper) === false);
    });
    it('should only assign the Reporter permission', () => {
      const permissions = Permissions.empty();
      permissions.addPermission(PermissionEnum.Reporter);

      assert(permissions.hasPermission(PermissionEnum.ChildrenViewer) === false);
      assert(permissions.hasPermission(PermissionEnum.ChildrenAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.LeaderAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.ParentAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.Financial) === false);
      assert(permissions.hasPermission(PermissionEnum.Reporter) === true);
      assert(permissions.hasPermission(PermissionEnum.Grouper) === false);
    });
    it('should only assign the Grouper permission', () => {
      const permissions = Permissions.empty();
      permissions.addPermission(PermissionEnum.Grouper);

      assert(permissions.hasPermission(PermissionEnum.ChildrenViewer) === false);
      assert(permissions.hasPermission(PermissionEnum.ChildrenAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.LeaderAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.ParentAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.Financial) === false);
      assert(permissions.hasPermission(PermissionEnum.Reporter) === false);
      assert(permissions.hasPermission(PermissionEnum.Grouper) === true);
    });
  });

  describe('#multiplePermissions', () => {
    it('should set some permissions', () => {
      const permissions = Permissions.empty();
      permissions.addPermission(PermissionEnum.ParentAdministrator);
      permissions.addPermission(PermissionEnum.Grouper);
      permissions.addPermission(PermissionEnum.ChildrenViewer);
      permissions.addPermission(PermissionEnum.ChildrenViewer);
      permissions.addPermission(PermissionEnum.ChildrenViewer);
      permissions.addPermission(PermissionEnum.ChildrenViewer);

      assert(permissions.hasPermission(PermissionEnum.ChildrenViewer) === true);
      assert(permissions.hasPermission(PermissionEnum.ChildrenAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.LeaderAdministrator) === false);
      assert(permissions.hasPermission(PermissionEnum.ParentAdministrator) === true);
      assert(permissions.hasPermission(PermissionEnum.Financial) === false);
      assert(permissions.hasPermission(PermissionEnum.Reporter) === false);
      assert(permissions.hasPermission(PermissionEnum.Grouper) === true);
    });
  });

  describe('#multiplePermissions', () => {
    it('deletes some permissions', () => {
      const permissions = Permissions.fromInteger(126);
      permissions.deletePermission(PermissionEnum.Financial);
      permissions.deletePermission(PermissionEnum.LeaderAdministrator);
      permissions.deletePermission(PermissionEnum.ChildrenViewer);

      assert.deepEqual(permissions, Permissions.fromBitString('1101010'));
    });
  });

  describe('#PermissionsConvertions', () => {
    it('should read the correct permissions from an integer', () => {
      const permissions = Permissions.fromInteger(126);

      assert(permissions.hasPermission(PermissionEnum.ChildrenViewer) === false);
      assert(permissions.hasPermission(PermissionEnum.ChildrenAdministrator) === true);
      assert(permissions.hasPermission(PermissionEnum.LeaderAdministrator) === true);
      assert(permissions.hasPermission(PermissionEnum.ParentAdministrator) === true);
      assert(permissions.hasPermission(PermissionEnum.Financial) === true);
      assert(permissions.hasPermission(PermissionEnum.Reporter) === true);
      assert(permissions.hasPermission(PermissionEnum.Grouper) === true);
    });

    it('should read the correct permissions from a bitstring', () => {
      assert.deepEqual(Permissions.fromInteger(126), Permissions.fromBitString('1111110'));
    });

    it('should return the correct bitstring.', () => {
      assert.deepEqual(
        Permissions.fromInteger(126).toBitString(),
        '00000000000000000000000001111110',
      );
    });
  });
});
