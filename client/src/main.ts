import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faCheck, faTimes, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import VueCookies from 'vue-cookies';
import store from '@/store/store';
import App from './App.vue';
import router from './router';
import './registerServiceWorker';

Vue.use(VueCookies);
Vue.use(Buefy);

library.add(faCheck, faTimes, faTrashAlt);
// Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
