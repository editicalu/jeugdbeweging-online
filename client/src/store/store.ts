import Vue from 'vue';
import Vuex from 'vuex';
import sessionModule from '@/store/SessionModule';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  modules: {
    sessionModule,
  },
  plugins: [createPersistedState()],
});
