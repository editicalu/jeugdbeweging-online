import { Module, VuexModule, Mutation } from 'vuex-module-decorators';
import SessionData from '@/lib/SessionData';
import { Parent, Account } from '@/lib/UsersPack';
import Child from '@/lib/Child';
import PermissionEnum from '@/lib/PermissionEnum';
import Permissions from '@/lib/Permissions';
import Leader from '@/lib/Leader';
import Vue from 'vue';

function isParent(person: Parent | Leader): person is Parent {
  return (person as Parent).children !== undefined;
}


// WARNING: Persistency of global state is handled by saving & loading of a
// JSON on client-side.
// This means that every data being accessed here has NO methods! If you need
// to access a class' methods, take the JSON and create a new object of the
// desired class with it.
@Module
export default class SessionModule extends VuexModule {
  // state
  sessionData = new SessionData()

  // getters
  get getSessionData(): SessionData {
    return this.sessionData;
  }

  get account(): Account | null {
    if (this.sessionData.account !== null) {
      const account: Account = new Account(
        this.sessionData.account.id,
        this.sessionData.account.permissions !== undefined
          ? Permissions.fromInteger(this.sessionData.account.permissions.permissionField)
          : Permissions.empty(),
        this.sessionData.account.email,
      );
      return account;
    }
    return null;
  }

  get accountID(): number {
    return this.sessionData.account !== null ? this.sessionData.account.id || -1 : -1;
  }

  get accountEmail(): string {
    return this.sessionData.account !== null ? this.sessionData.account.email || '' : '';
  }

  get isLoggedIn(): boolean {
    const sessionExpireDate =
      new Date(Number(Vue.cookies.get('session_expire')));
    return this.sessionData.account !== null
      && new Date() < sessionExpireDate;
  }

  get userIsParent(): boolean {
    return this.sessionData.account !== null && !this.sessionData.isLeader;
  }

  get userIsLeader(): boolean {
    return this.sessionData.account !== null && this.sessionData.isLeader;
  }

  get children(): Child[] {
    return this.sessionData.children.map(child => new Child(
      child.id,
      child.firstName,
      child.lastName,
      child.placeOfBirth,
      child.dateOfBirth,
      child.gender,
      child.phoneNumber,
      child.address,
      child.groups,
      child.comments,
      child.bills,
    ));
  }

  get hasPermission(): Function {
    return (permission: PermissionEnum): boolean => {
      if (this.sessionData.account === null || this.sessionData.account.permissions === undefined) {
        return false;
      }
      return Permissions
        .fromInteger(this.sessionData.account.permissions.permissionField)
        .hasPermission(permission);
    };
  }

  get permissionsNumber(): number {
    if (this.sessionData.account === null || this.sessionData.account.permissions === undefined) {
      return 0;
    }
    return this.sessionData.account.permissions.permissionField;
  }

  get personID(): number | null {
    return this.sessionData.personID;
  }

  @Mutation
  setAccount(account: Account) {
    this.sessionData.account = account;
  }

  @Mutation
  setIsLeader(isLeader: boolean) {
    this.sessionData.isLeader = isLeader;
  }

  @Mutation
  setChildren(children: Child[]) {
    this.sessionData.children = children;
  }

  @Mutation
  logout(): void {
    this.sessionData = new SessionData();
  }

  @Mutation
  setPersonID(personID: number | null) {
    this.sessionData.personID = personID;
  }
}
