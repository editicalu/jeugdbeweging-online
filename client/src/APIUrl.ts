const APIUrl =
  `http${
    process.env.VUE_APP_SSL === 'true' ? 's' : ''
  }://${
    process.env.VUE_APP_DOMAIN_NAME
  }:${
    process.env.VUE_APP_PORT
  }`;

export default APIUrl;
