import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: () => import('./views/Login.vue'),
    },
    {
      path: '/person/:id(\\d+)',
      name: 'personPage',
      component: () => import('./views/PersonPage.vue'),
    },
    {
      path: '/users',
      name: 'users',
      component: () => import('./views/Users.vue'),
    },
    {
      path: '/groups',
      name: 'groupExplorer',
      component: () => import('./views/GroupExplorer.vue'),
    },
    {
      path: '/checklists',
      name: 'ChecklistExplorer',
      component: () => import('./views/ChecklistExplorer.vue'),
    },
    {
      path: '/home',
      name: 'Home',
      component: () => import('./views/Home.vue'),
    },
    {
      path: '/group/:id(\\d+)',
      name: 'group',
      component: () => import('./views/Group.vue'),
    },
    {
      path: '/checklist/:id(\\d+)',
      name: 'checklist',
      component: () => import('./views/Checklist.vue'),
    },
    {
      path: '/account/:accountId(\\d+)',
      name: 'accountPage',
      component: () => import('./views/AccountPage.vue'),
      props: true,
    },
    {
      path: '/register/child',
      name: 'registerChild',
      component: () => import('./views/registerChildPage.vue'),
    },
    {
      path: '/register/leader',
      name: 'registerLeader',
      component: () => import('./views/RegisterLeaderPage.vue'),
    },
    {
      path: '/reports',
      name: 'reports',
      component: () => import('./views/ReportsOverviewPage.vue'),
    },
    {
      path: '/report/edit',
      name: 'writeNewReport',
      component: () => import('./views/WriteReportPage.vue'),
    },
    {
      path: '/report/edit/:id(\\d+)',
      name: 'editReport',
      component: () => import('./views/WriteReportPage.vue'),
    },
    {
      path: '/report/view/:id(\\d+)',
      name: 'viewReportPage',
      component: () => import('./views/ViewReportPage.vue'),
    },
    {
      path: '/add_child_to_parent',
      name: 'addChildToParent',
      component: () => import('./views/AddChildPage.vue'),
    },
    {
      path: '/bills/:id(\\d+)',
      name: 'billsOverview',
      component: () => import('./views/BillOverviewPage.vue'),
    },
    {
      path: '/bills/null',
      name: 'billsOverview',
      component: () => import('./views/BillOverviewPage.vue'),
    },
    {
      path: '/bill/:id(\\d+)',
      name: 'bill',
      component: () => import('./views/BillPage.vue'),
    },
    {
      path: '/parents',
      name: 'Parents',
      component: () => import('./views/Parents.vue'),
    }, {
      path: '/billmanagement',
      name: 'Bill Management Page',
      component: () => import('./views/AllBillsPage.vue'),
    },
    {
      path: '/events',
      name: 'events',
      component: () => import('./views/ActivityExplorer.vue'),
    },
    {
      path: '/event/:id(\\d+)',
      name: 'event',
      component: () => import('./views/Activity.vue'),
    },
    {
      path: '*',
      name: '404',
      component: () => import('./views/Page404.vue'),
    },
  ],
});
