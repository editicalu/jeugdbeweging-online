import PermissionEnum from './PermissionEnum';

const MAX_PERMISSIONS = 32;
const HEXSTRING_LENGTH = Math.ceil(MAX_PERMISSIONS / 4);

const KEYS = Object.keys(PermissionEnum)
  .filter(v => /^[0-9]+$/gi.test(v)).map(item => Number(item)) as number[];

export default class Permissions {
  public static fromInteger(integer: number): Permissions {
    return new Permissions(integer);
  }

  /**
     * Builds a Permissions object from a bitstring. Returns null if the bitstring is invalid.
     * @param bitstring The bitstring to be parsed.
     */
  public static fromBitString(bitstring: string): Permissions | null {
    try {
      const integer = parseInt(bitstring, 2);
      return new Permissions(integer);
    } catch (err) {
      return null;
    }
  }

  /**
     * Returns a new Permissions object with no permissions at all.
     */
  public static empty(): Permissions {
    return new Permissions(0);
  }

  public permissionField: number;

  private constructor(integer: number) {
    this.permissionField = integer;
  }

  /**
   * Checks whether the given permission is included.
   * @param permission the permission to check for
   */
  public hasPermission(permission: PermissionEnum): boolean {
    return (this.permissionField & (1 << permission)) !== 0;
  }

  /**
   * Adds a Permission. Does nothing if the Permission is already included
   * @param permission Permission to add.
   */
  public addPermission(permission: PermissionEnum) {
    this.permissionField = (this.permissionField | (1 << permission)) >>> 0;
  }

  /**
   * Deletes the given permission. Does nothing if the Permission is already omitted.
   * @param permission Permission to delete
   */
  public deletePermission(permission: PermissionEnum) {
    this.permissionField = (this.permissionField & ~(1 << permission)) >>> 0;
  }

  public allPermissions(): PermissionEnum[] {
    return KEYS.filter(permissionId => this.hasPermission(permissionId));
  }

  public getRawNumber(): number {
    return this.permissionField;
  }

  public toBitString(): string {
    let bitstring = this.permissionField.toString(2);
    const addLength = MAX_PERMISSIONS - bitstring.length;
    for (let i = 0; i < addLength; i += 1) {
      bitstring = `0${bitstring}`;
    }
    return bitstring;
  }

  public toHexString(): string {
    let bitstring = this.permissionField.toString(16);
    while (bitstring.length < HEXSTRING_LENGTH) {
      bitstring = `0${bitstring}`;
    }
    return `0x${bitstring}`;
  }
}
