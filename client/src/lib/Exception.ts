export default class Exception {
  constructor(public message: string = '') {
    this.message = message;
  }
}
