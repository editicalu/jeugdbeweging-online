import ResponseBill from '@/api/responseInterfaces/ResponseBill';

export default class Bill {
  public creationDateString: string|undefined = undefined;

  public constructor(
    public id: number|undefined = undefined,
    public description: string|undefined = undefined,
    public amount: number|undefined = undefined,
    public creationDate: Date|undefined = undefined,
  ) {
    this.id = id;
    this.description = description;
    this.amount = Number(amount);
    this.creationDate = creationDate;

    if (creationDate === undefined) {
      this.creationDate = undefined;
    } else {
      this.creationDateString =
        `${creationDate.getDate()}-${creationDate.getMonth() + 1}-${creationDate.getFullYear()}`;
    }
  }


  /**
   * Make a deepcopy of a bill
   * @param bill the bill to copy
   */
  static deepCopy(bill: Bill): Bill {
    return new Bill(
      bill.id,
      bill.description,
      bill.amount,
      bill.creationDate,
    );
  }


  /**
   * Generate a bill-object, based on a json from the server
   * @param resp the json in the server response
   * @returns the bill-object
   */
  static fromServerResponse(resp: ResponseBill): Bill {
    return new Bill(
      resp.id,
      resp.description,
      resp.amount,
      resp.creationDate !== undefined ?
        new Date(resp.creationDate) :
        undefined,
    );
  }


  /**
   * Generate e json for server-communication, based on a bill
   * @param bill the bill you want to send over the network
   * @returns the json formatted bill
   */
  static toServerRequest(bill: Bill): ResponseBill {
    return {
      id: bill.id,
      description: bill.description,
      amount: bill.amount,
      creationDate: bill.creationDate !== undefined ?
        bill.creationDate.toISOString() :
        undefined,
    };
  }


  /**
   * Make da deep-copy of an array of bills
   * @param bills the bills to copy
   * @returns the array of bills
   */
  static deepCopyList(bills: Bill[]|undefined): Bill[]|undefined {
    if (bills === undefined) {
      return undefined;
    }
    return bills.map(bill => Bill.deepCopy(bill));
  }


  /**
   * Calculate the total sum of an array of bills
   * @param bills the array of bills
   * @returns The sum of all bills which as an amount that is defined
   */
  static sum(bills: Bill[]|undefined): number {
    if (bills === undefined) {
      return 0;
    }
    let sum: number = 0;
    bills.forEach((bill) => {
      if (bill.amount !== undefined) {
        sum += Number(bill.amount);
      }
    });
    return sum;
  }
}
