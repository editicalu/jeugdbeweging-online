import ResponsePerson from '@/api/responseInterfaces/ResponsePerson';
import { Gender, Group } from './UsersPack';
import Address from './Address';
import Bill from './Bill';
import Person from './Person';


export default class Child extends Person {
  constructor(
    public id: number = 0,
    public firstName: string = '',
    public lastName: string = '',
    public placeOfBirth: string|undefined = undefined,
    public dateOfBirth: Date|undefined = undefined,
    public gender: Gender|undefined = undefined,
    public phoneNumber: string|undefined = undefined,
    public address: Address|undefined = undefined,
    public groups: Group[]|undefined = undefined,
    public comments: string|undefined = undefined,
    public bills: Bill[]|undefined = undefined,
  ) {
    super(
      id, firstName, lastName, placeOfBirth, dateOfBirth,
      gender, phoneNumber, address, groups, comments, bills,
    );
  }

  static toServerRequest(child: Child): ResponsePerson {
    return super.toServerRequest(child as Person);
  }
}
