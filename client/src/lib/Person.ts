import '@/lib/ArrayExtend';
import ResponsePerson from '@/api/responseInterfaces/ResponsePerson';

import { Gender, Group, PersonMin } from './UsersPack';
import Address from './Address';
import Bill from './Bill';


export default class Person extends PersonMin {
  constructor(
    public id: number = 0,
    public firstName: string = '',
    public lastName: string = '',
    public placeOfBirth: string | undefined = undefined,
    public dateOfBirth: Date | undefined = undefined,
    public gender: Gender | undefined = undefined,
    public phoneNumber: string | undefined = undefined,
    public address: Address | undefined = undefined,
    public groups: Group[] | undefined = undefined,
    public comments: string | undefined = undefined,
    public bills: Bill[] | undefined = undefined,
    public type: string | undefined = undefined,
  ) {
    super(id, firstName, lastName, groups);
    this.placeOfBirth = placeOfBirth;
    this.dateOfBirth = dateOfBirth;
    this.gender = gender;
    this.phoneNumber = phoneNumber;
    this.address = address;
    this.comments = comments;
    this.bills = bills;
    this.type = type;
  }


  /**
   * Generate a Person-object based on the response from the server
   * @param resp the server response
   * @returns Person from server-response
   */
  static fromServerResponse(resp: ResponsePerson): Person {
    return new Person(
      resp.id,
      resp.firstName !== undefined ? resp.firstName : '',
      resp.lastName !== undefined ? resp.lastName : '',
      resp.birthplace,
      resp.birthdate !== undefined ? new Date(resp.birthdate) : undefined,
      Gender[resp.gender as keyof typeof Gender] || Gender.Blank,
      resp.phoneNumber,
      (resp.address !== undefined && resp.address !== null) ? Address.fromServerResponse(resp.address) : new Address('', '', ''),
      undefined,
      resp.comment,
      undefined,
      resp.type,
    );
  }


  /**
   * format a person to a format you can send
   * @param person the person to format
   * @returns the formatted person
   */
  static toServerRequest(person: Person): ResponsePerson {
    const req: ResponsePerson = {
      id: person.id,
      firstName: person.firstName,
      lastName: person.lastName,
      birthplace: person.placeOfBirth,
      birthdate: person.dateOfBirth !== undefined
        ? person.dateOfBirth.toISOString()
        : undefined,
      gender: person.gender,
      phoneNumber: person.phoneNumber,
      address: person.address !== undefined ?
        Address.toServerRequest(person.address) :
        undefined,
      comment: person.comments,
      registrationPending: undefined,
      type: person.type,
    };
    return req;
  }


  /**
   * Make a deepcopy of a person object
   * @param person the person-object you want to deep-copy
   */
  static deepCopy(person: Person): Person {
    return new Person(
      person.id,
      person.firstName,
      person.lastName,
      person.placeOfBirth,
      person.dateOfBirth,
      person.gender,
      person.phoneNumber,
      person.address !== undefined && person.address !== null
        ? Address.deepCopy(person.address)
        : undefined,
      this.deepCopyGroups(person.groups),
      person.comments,
      person.bills !== undefined ?
        person.bills.map(bill => Bill.deepCopy(bill)) :
        undefined,
      person.type,
    );
  }

  /**
   * Add a group to this person, if this group isn't assigned
   * yet to this person. Else do nothing.
   * @param group the group to add
   * @post This person will be part of the given group
   */
  addGroup(group: Group): void {
    if (this.groups === undefined) {
      this.groups = [];
    }

    let contains: boolean = false;
    this.groups.forEach((existingGroup) => {
      if (existingGroup.id === group.id) {
        contains = true;
      }
    });

    if (!contains) {
      this.groups.push(group);
    }
  }


  /**
   * Add multiple groups to a person
   * @param groups the groups you want to add
   */
  addGroups(groups: Group[]): void {
    groups.forEach((group) => {
      this.addGroup(group);
    });
  }

  /**
   * Remove a group from this person's groups
   * @param group The group you want to remove
   */
  removeGroup(group: Group): void {
    if (this.groups === undefined) {
      return;
    }

    const newGroups: Group[] = [];

    this.groups.forEach((containedGroup) => {
      if (containedGroup.id !== group.id) {
        newGroups.push(containedGroup);
      }
    });

    this.groups = Person.deepCopyGroups(newGroups);
  }


  /**
   * Remove multiple gropus from this person
   * @param groups the groups you want to remove
   */
  removeGroups(groups: Group[]): void {
    groups.forEach((group) => {
      this.removeGroup(group);
    });
  }

  // getters with safety check
  // Returns the value if set and throw an error when not set

  getId(): number {
    if (this.id === undefined) {
      throw Error('Id is undefined');
    }
    return this.id;
  }

  // setters
  setBills(bills: Bill[]): void {
    this.bills = bills.map(bill => Bill.deepCopy(bill));
  }

  setDateOfBirth(localDate: Date) {
    this.dateOfBirth =
      new Date(localDate.getTime() - (localDate.getTimezoneOffset() * 60 * 1000));
  }

  private static deepCopyGroups(groups: Group[] | undefined): Group[] | undefined {
    return groups !== undefined ?
      groups.map(group => Group.deepCopy(group)) :
      undefined;
  }
}
