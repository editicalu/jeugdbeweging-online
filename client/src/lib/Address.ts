import ResponseAddress from '@/api/responseInterfaces/ResponseAddress';

export default class Address {
  constructor(
    public streetAndNumber: string|undefined = undefined,
    public zipCode: string|undefined = undefined,
    public city: string|undefined = undefined,
  ) {
    this.streetAndNumber = streetAndNumber;
    this.zipCode = zipCode;
    this.city = city;
  }

  static fromServerResponse(resp: ResponseAddress): Address {
    return new Address(resp.streetAddress, resp.zipcode, resp.city);
  }


  /**
   * format given address to a format you can send
   * @param address the address to send
   * @returns server-format
   */
  static toServerRequest(address: Address): ResponseAddress {
    const req: ResponseAddress = {
      streetAddress: address.streetAndNumber,
      zipcode: address.zipCode,
      city: address.city,
    };
    return req;
  }

  static deepCopy(addr: Address|undefined): Address {
    if (addr === undefined) {
      return new Address();
    }
    return new Address(
      addr.streetAndNumber,
      addr.zipCode,
      addr.city,
    );
  }
}
