export class ReportMin {
  dateString: string = '';
  constructor(
    public id: number = 0,
    public reportName: string = '',
    public writtenById: number = 0,
    public writtenByName: string = '',
    public date: Date = new Date(1980, 0, 1),
  ) {
    this.id = id;
    this.reportName = reportName;
    this.writtenById = writtenById;
    this.writtenByName = writtenByName;
    this.date = date;
    this.dateString = date.toISOString().split('T')[0];
  }
}

export class Report extends ReportMin {
  constructor(
    public id: number = 0,
    public reportName: string = '',
    public writtenById: number = 0,
    public writtenByName: string = '',
    public date: Date = new Date(1980, 0, 1),
    public content: string = '',
  ) {
    super(id, reportName, writtenById, writtenByName, date);
    this.content = content;
  }
}
