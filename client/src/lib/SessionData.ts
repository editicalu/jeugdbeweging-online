import { Account } from '@/lib/UsersPack';
import Child from '@/lib/Child';

export default class SessionData {
  account: Account|null = null;
  isLeader: boolean = false;
  children: Child[] = [];
  personID: number|null = null;
}
