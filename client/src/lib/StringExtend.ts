declare interface String {
    insensitiveIncludes(this: String, substr: string): boolean;
}

// Extend String with the `insensitiveIncludes()` method.
// This method checks whether a substr is included in the base string, but does
// this case in-sensitively.
Object.defineProperty(String.prototype, 'insensitiveIncludes', {
  value(this: String, substr: string): boolean {
    return this.toLocaleUpperCase().includes(substr.toLocaleUpperCase());
  },
});

