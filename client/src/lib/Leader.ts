import Person from './Person';
import { Gender, Account, Group } from './UsersPack';
import Address from './Address';
import Bill from './Bill';

export default class Leader extends Person {
  constructor(
    public id: number = 0,
    public firstName: string = '',
    public lastName: string = '',
    public placeOfBirth: string|undefined = undefined,
    public dateOfBirth: Date|undefined = undefined,
    public gender: Gender|undefined = undefined,
    public phoneNumber: string|undefined = undefined,
    public address: Address|undefined = undefined,
    public groups: Group[]|undefined = undefined,
    public comments: string|undefined = undefined,
    public bills: Bill[]|undefined = undefined,
    public account: Account|undefined = undefined,
  ) {
    super(
      id, firstName, lastName, placeOfBirth, dateOfBirth,
      gender, phoneNumber, address, groups, comments, bills,
    );
    this.account = account;
  }

  /**
   * Generate a Leader-object from a given person and account
   * @param person the given person
   * @param account the given account
   * @returns a leader
   */
  static fromPersonAndAccount(person: Person, account: Account): Leader {
    return new Leader(
      person.id,
      person.firstName,
      person.lastName,
      person.placeOfBirth,
      person.dateOfBirth,
      person.gender,
      person.phoneNumber,
      Address.deepCopy(person.address),
      person.groups !== undefined ?
        Group.deepCopyList(person.groups) :
        undefined,
      person.comments,
      Bill.deepCopyList(person.bills),
      account,
    );
  }

  static deepCopy(leader: Leader): Leader {
    return new Leader(
      leader.id,
      leader.firstName,
      leader.lastName,
      leader.placeOfBirth,
      leader.dateOfBirth,
      leader.gender,
      leader.phoneNumber,
      Address.deepCopy(leader.address),
      leader.groups !== undefined ?
        Group.deepCopyList(leader.groups) :
        undefined,
      leader.comments,
      Bill.deepCopyList(leader.bills),
      leader.account !== undefined ?
        leader.account.deepCopy() :
        undefined,
    );
  }
}
