import Permissions from '@/lib/Permissions';
import Child from '@/lib/Child';
import Grouping from '@/lib/Grouping';
import ResponseAccount from '@/api/responseInterfaces/ResponseAccount';

export enum Gender {
  Male = 'Male',
  Female = 'Female',
  Other = 'Other',
  Blank = ''
}

export class Account {
  constructor(
    public id: number = -1,
    public permissions: Permissions = Permissions.empty(),
    public email: string = '',
    public password: string | undefined = undefined,
    public accountType: string = 'leader',
  ) {
    this.id = id;
    this.permissions = permissions;
    this.email = email;
    this.password = password;
    this.accountType = accountType;
  }

  deepCopy(): Account {
    return new Account(
      this.id,
      this.permissions !== undefined ?
        Permissions.fromInteger(this.permissions.getRawNumber()) :
        undefined,
      this.email,
      this.password,
    );
  }

  static fromServerResponse(resp: ResponseAccount): Account {
    return new Account(
      resp.id,
      resp.permissions !== undefined ?
        Permissions.fromInteger(resp.permissions) :
        undefined,
      resp.email,
      resp.password,
    );
  }

  static toServerRequest(account: Account): ResponseAccount {
    return {
      id: account.id,
      permissions: account.permissions !== undefined ?
        account.permissions.getRawNumber() :
        undefined,
      email: account.email,
      password: account.password,
    };
  }
}

export interface AccountMin {
  id: number,
  email: string,
  isParent: boolean,
}

export class Parent {
  constructor(public children: Child[], public account: Account) {
    this.account = account;
    this.children = children;
  }
}

export class Container<T> {
  v: T;

  constructor(val: T) {
    this.v = val;
  }

  set(val: T) {
    this.v = val;
  }
}

export class Group extends Grouping {
  /**
   * Make a deep-copy of a given group
   * @param group the given group
   * @returns a deep-copy of the given group
   */
  public static deepCopy(group: Group): Group {
    return new Group(group.id, group.title, group.description);
  }


  /**
   * Make a deep-copy of an array of groups
   * @param groups the array of groups
   * @returns a deep-copy of the given array
   */
  public static deepCopyList(groups: Group[]): Group[] {
    return groups.map(group => Group.deepCopy(group));
  }
}

export class PersonMin {
  constructor(
    public id: number = 0,
    public firstName: string = '',
    public lastName: string = '',
    public groups: Group[] | undefined = undefined,
  ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.groups = groups;
  }

  public static fromJson(person: { id: number, firstName: string, lastName: string }): PersonMin {
    return new PersonMin(person.id, person.firstName, person.lastName);
  }
}
