import Grouping from '@/lib/Grouping';

export default class Activity extends Grouping {
  public static fromJson<T extends Grouping>(
    this: { new(id: number, title: string, description: string,
                preRegisterDate: Date | null, startDate: Date, stopDate: Date): T; },
    query: { id: number, title: string, description: string | null,
      preRegisterDate: string | null, startDate: string, stopDate: string},
  ): T {
    if (query.preRegisterDate === null) {
      return new this(
        query.id, query.title, query.description || '',
        null, new Date(query.startDate), new Date(query.stopDate),
      );
    }
    return new this(
      query.id, query.title, query.description || '',
      new Date(query.preRegisterDate), new Date(query.startDate), new Date(query.stopDate),
    );
  }

  constructor(
        public id: number = -1,
        public title: string = '',
        public description: string = '',
        public preRegisterDate: Date | null = null,
        public startDate: Date = new Date(),
        public stopDate: Date = new Date(),
  ) {
    super(id, title, description);
    this.preRegisterDate = preRegisterDate;
    this.startDate = startDate;
    this.stopDate = stopDate;
  }
}
