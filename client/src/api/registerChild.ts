import Axios, { AxiosResponse } from 'axios';
import Child from '@/lib/Child';
import APIUrl from '@/APIUrl';


/**
 * Register a child on the server
 * @param child containing the data of the child you want to register
 * @throws exception when connection error or return value isn't valid
 */
export default async function registerChild(child: Child): Promise<{ valid: boolean, id: number }> {
  // initialize Axios headers
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  // send request
  const response: AxiosResponse =
    await Axios.put(
      `${APIUrl}/api/child`,
      Child.toServerRequest(child),
      { withCredentials: true },
    );

  // return response if it's valid
  return response.data;
}
