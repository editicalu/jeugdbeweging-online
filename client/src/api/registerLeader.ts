import Axios, { AxiosResponse } from 'axios';
import Leader from '@/lib/Leader';
import APIUrl from '@/APIUrl';

/**
 * Register a leader on the server
 * @param leader containing the data of the child you want to register
 * @throws exception when connection error or return value isn't valid
 */
export default async function register(leader: Leader)
  : Promise<{ valid: boolean, accId: number, perId: number }> {
  // initialize Axios headers
  Axios.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';
  const sendingLeader = {
    firstName: leader.firstName,
    lastName: leader.lastName,
    address: leader.address ? {
      streetAddress: leader.address.streetAndNumber,
      zipCode: leader.address.zipCode,
      city: leader.address.city,
    } : null,
    phoneNumber: leader.phoneNumber,
    birthplace: leader.placeOfBirth,
    birthdate: leader.dateOfBirth,
    permissions: leader.account!.permissions.getRawNumber(),
    username: leader.account!.email,
    password: leader.account!.password,
  };
  // send request
  const response: AxiosResponse =
    await Axios.put(
      `${APIUrl}/api/leader`,
      sendingLeader,
      { withCredentials: true },
    );

  // return response if it's valid
  return response.data;
}
