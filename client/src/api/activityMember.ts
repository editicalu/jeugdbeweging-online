import Axios from 'axios';
import APIUrl from '@/APIUrl';

const baseUrl = `${APIUrl}/api/activity_member/`;
const axiosOptions = { withCredentials: true };

async function addGroupMembersToActivity(
  activityId: number,
  addedGroupId: number,
): Promise<number[] | null> {
  Axios.defaults.headers.post['Content-Type'] =
    'application/json; charset=utf-8';
  try {
    const { notAddedIds } =
      (await Axios.post(`${baseUrl}${activityId}/group/${addedGroupId}`, {}, axiosOptions)).data;
    return notAddedIds;
  } catch (e) {
    if (e.response.data === { error: 'alreadyAdded' }) {
      return [];
    }
    return null;
  }
}

async function postMemberToActivity(
  personId: number,
  activityId: number,
): Promise<boolean> {
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  try {
    await Axios.post(`${baseUrl}${activityId}/${personId}`, {}, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

async function deleteMemberFromActivity(personId: number, activityId: number): Promise<boolean> {
  try {
    await Axios.delete(`${baseUrl}${activityId}/${personId}`, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

export {
  addGroupMembersToActivity,
  postMemberToActivity,
  deleteMemberFromActivity,
};
