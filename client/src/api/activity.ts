import Axios, { AxiosPromise, AxiosResponse } from 'axios';
import Activity from '@/lib/Activity';
import { PersonMin } from '@/lib/UsersPack';
import APIUrl from '@/APIUrl';

const baseUrl = `${APIUrl}/api/activity/`;
const axiosOptions = { withCredentials: true };

interface Response {
  valid: boolean;
  error: string;
  id: number;
}

interface ServerPersonMin {
  id: number,
  firstName: string,
  lastName: string
}

async function saveNewActivity(activity: Activity): Promise<number> {
  // initialize Axios headers
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  const axiosData = {
    title: activity.title,
    description: activity.description,
    preRegister: activity.preRegisterDate,
    startDate: activity.startDate,
    stopDate: activity.stopDate,
  };

  try {
    const answer: AxiosResponse = await Axios.post(`${baseUrl}`, axiosData, axiosOptions);
    const answerData: Response = answer.data;
    return answerData.id;
  } catch (e) {
    throw Error(e);
  }
}

async function getFullActivity(activityId: string)
  : Promise<{ activity: Activity, members: PersonMin[] } | null> {
  try {
    let members: PersonMin[] = [];
    const url = `${baseUrl}${activityId}?page=`;
    const pageNumber = 0;

    const resp = await Axios.get(`${url}${pageNumber}`, axiosOptions);
    const activity = Activity.fromJson(resp.data.activity);

    members =
      members.concat(resp.data.persons.map((m: ServerPersonMin) => PersonMin.fromJson(m)));
    const leftOverPages =
      Math.ceil((resp.data.amtPersons / resp.data.persons.length) - 1) || 0;

    const serverResponsesOtherPages = await Promise
      .all(Array(leftOverPages)
        .fill(null)
        .map((_: any, i: number): AxiosPromise<any> =>
          Axios.get(`${url}${i + 1}`, axiosOptions)));

    const otherMembers: PersonMin[] =
      serverResponsesOtherPages
        .filter(r => r.status === 200)
        .flatMap(r => r.data.persons.map((m: ServerPersonMin) => PersonMin.fromJson(m)));
    members = members.concat(otherMembers);

    return { activity, members };
  } catch (_) {
    return null;
  }
}

async function updateActivity(activity: Activity): Promise<boolean> {
  // initialize Axios headers
  Axios.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';
  const url = `${baseUrl}${activity.id}`;

  const axiosData = {
    title: activity.title,
    description: activity.description,
    preRegister: activity.preRegisterDate,
    startDate: activity.startDate,
    stopDate: activity.stopDate,
  };

  try {
    await Axios.put(`${url}`, axiosData, axiosOptions);
    return true;
  } catch (e) {
    throw Error(e);
  }
}

async function deleteActivity(activityId: number): Promise<boolean> {
  // initialize Axios headers
  Axios.defaults.headers.delete['Content-Type'] = 'application/json; charset=utf-8';
  const url = `${baseUrl}${activityId}`;

  try {
    await Axios.delete(`${url}`, axiosOptions);
    return true;
  } catch (e) {
    throw Error(e);
  }
}

export { saveNewActivity, getFullActivity, updateActivity, deleteActivity };
