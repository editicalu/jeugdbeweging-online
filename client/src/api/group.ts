import { Group, PersonMin } from '@/lib/UsersPack';
import Axios, { AxiosPromise } from 'axios';
import APIUrl from '@/APIUrl';

const baseUrl = `${APIUrl}/api/group/`;
const axiosOptions = { withCredentials: true };

async function postNewGroup(group: Group): Promise<boolean> {
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  const message =
    JSON.stringify({ title: group.title, description: group.description });

  try {
    await Axios.post(baseUrl, message, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

async function updateGroup(group: Group): Promise<boolean> {
  Axios.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';
  const message =
    JSON.stringify({ title: group.title, description: group.description });

  try {
    await Axios.put(`${baseUrl}${group.id}`, message, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

async function deleteGroup(groupId: string): Promise<boolean> {
  try {
    await Axios.delete(`${baseUrl}${groupId}`, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

interface ServerPersonMin {
  id: number,
  firstName: string,
  lastName: string
}

// TODO: we should use pagination for listing the members. This requires some
// hacking with Vue tho. At least we're not requesting 1 HUGE JSON, but
// multiples.
async function getFullGroup(groupId: string)
  : Promise<{ group: Group, members: PersonMin[] } | null> {
  try {
    let members: PersonMin[] = [];
    const url = `${baseUrl}${groupId}?page=`;
    const pageNumber = 0;

    const resp = await Axios.get(`${url}${pageNumber}`, axiosOptions);
    const group = Group.fromJson(resp.data.group);

    members =
      members.concat(resp.data.persons.map((m: ServerPersonMin) => PersonMin.fromJson(m)));
    const leftOverPages =
      Math.ceil((resp.data.amtPersons / resp.data.persons.length) - 1) || 0;

    const serverResponsesOtherPages = await Promise
      .all(Array(leftOverPages)
        .fill(null)
        .map((_: any, i: number): AxiosPromise<any> =>
          Axios.get(`${url}${i + 1}`, axiosOptions)));

    const otherMembers: PersonMin[] =
      serverResponsesOtherPages
        .filter(r => r.status === 200)
        .flatMap(r => r.data.persons.map((m: ServerPersonMin) => PersonMin.fromJson(m)));
    members = members.concat(otherMembers);

    return { group, members };
  } catch (_) {
    return null;
  }
}

export { postNewGroup, getFullGroup, deleteGroup, updateGroup };
