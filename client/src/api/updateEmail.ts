import Axios, { AxiosResponse } from 'axios';
import APIUrl from '@/APIUrl';

interface RequestFormat {
  email: string,
}

function generateRequest(email: string): RequestFormat {
  return { email };
}

export default async function updateEmail(
  email: string,
  id: number,
): Promise<number> {
  // initialize Axios headers
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  // send request to back-end server
  const response: AxiosResponse =
    await Axios.post(
      `${APIUrl}/api/account/${id}/change_email/`,
      generateRequest(email),
      { withCredentials: true },
    );
  return 0;
}
