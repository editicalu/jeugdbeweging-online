import { PersonMin } from '@/lib/UsersPack';
import Checklist from '@/lib/Checklist';
import Axios, { AxiosPromise } from 'axios';
import APIUrl from '@/APIUrl';

const baseUrl = `${APIUrl}/api/checklist/`;
const axiosOptions = { withCredentials: true };

async function postNewChecklist(checklist: Checklist): Promise<boolean> {
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  const message =
    JSON.stringify({ title: checklist.title, description: checklist.description });

  try {
    await Axios.post(baseUrl, message, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

async function updateChecklist(checklist: Checklist): Promise<boolean> {
  Axios.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';
  const message =
    JSON.stringify({ title: checklist.title, description: checklist.description });

  try {
    await Axios.put(`${baseUrl}${checklist.id}`, message, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

async function deleteChecklist(checklistId: string): Promise<boolean> {
  try {
    await Axios.delete(`${baseUrl}${checklistId}`, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

interface ServerPersonMin {
  id: number,
  checked: boolean,
  firstName: string,
  lastName: string
}

// TODO: we should use pagination for listing the members. This requires some
// hacking with Vue tho. At least we're not requesting 1 HUGE JSON, but
// multiples.
async function getFullChecklist(checklistId: string)
  : Promise<{
    checklist: Checklist,
    members: PersonMin[],
    checkedMembers: PersonMin[],
  } | null> {
  try {
    let members: PersonMin[] = [];
    let checkedMembers: PersonMin[] = [];
    const url = `${baseUrl}${checklistId}?page=`;
    const pageNumber = 0;

    const resp = await Axios.get(`${url}${pageNumber}`, axiosOptions);
    const checklist = Checklist.fromJson(resp.data.checklist);

    const persons =
      resp
        .data
        .persons
        .map((m: ServerPersonMin) => [m.checked, PersonMin.fromJson(m)]);
    members = members.concat(persons.map((p: [boolean, PersonMin]) => p[1]));
    checkedMembers =
      checkedMembers.concat(persons
        .filter((p: [boolean, PersonMin]) => p[0])
        .map((p: [boolean, PersonMin]) => p[1]));

    const leftOverPages =
      Math.ceil((resp.data.amtPersons / resp.data.persons.length) - 1) || 0;

    const serverResponsesOtherPages = await Promise
      .all(Array(leftOverPages)
        .fill(null)
        .map((_: any, i: number): AxiosPromise<any> =>
          Axios.get(`${url}${i + 1}`, axiosOptions)));

    const otherPersons =
      serverResponsesOtherPages
        .filter(r => r.status === 200)
        .flatMap(r =>
          r
            .data
            .persons
            .map((m: ServerPersonMin) => [m.checked, PersonMin.fromJson(m)]));
    members =
      members.concat(otherPersons.map((p: [boolean, PersonMin]) => p[1]));
    checkedMembers =
      checkedMembers.concat(otherPersons
        .filter((p: [boolean, PersonMin]) => p[0])
        .map((p: [boolean, PersonMin]) => p[1]));

    return { checklist, members, checkedMembers };
  } catch (_) {
    return null;
  }
}

export { postNewChecklist, getFullChecklist, deleteChecklist, updateChecklist };
