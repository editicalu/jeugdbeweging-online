import Axios, { AxiosResponse } from 'axios';
import APIUrl from '@/APIUrl';

interface SReport {
  valid: boolean,
  error: string,
  accountID: number,
}

export default async function translatePersonToAccountId(personId: number): Promise<number> {
  Axios.defaults.headers.get['Content-Type'] = 'application/json; charset=utf-8';

  const answer: AxiosResponse =
    await Axios.get(
      `${APIUrl}/api/translate/person/${personId}`,
      { withCredentials: true },
    );

  const answerData: SReport = answer.data;

  // Check if something went wrong
  if (answerData.valid === false) {
    throw Error(answerData.error);
  }

  return answerData.accountID;
}
