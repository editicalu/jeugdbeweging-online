import Axios from 'axios';
import APIUrl from '@/APIUrl';

const baseUrl = `${APIUrl}/api/checklist_member/`;
const axiosOptions = { withCredentials: true };

async function addActivityMembersToChecklist(
  checklistId: number,
  addedActivityId: number,
): Promise<number[] | null> {
  Axios.defaults.headers.post['Content-Type'] =
    'application/json; charset=utf-8';
  try {
    const { notAddedIds } = (await Axios.post(`${baseUrl}${checklistId}/activity/${addedActivityId}`, {}, axiosOptions)).data;
    return notAddedIds;
  } catch (e) {
    if (e.response.data === { error: 'alreadyAdded' }) {
      return [];
    }
    return null;
  }
}

async function addGroupMembersToChecklist(
  checklistId: number,
  addedGroupId: number,
): Promise<number[] | null> {
  Axios.defaults.headers.post['Content-Type'] =
    'application/json; charset=utf-8';
  try {
    const { notAddedIds } = (await Axios.post(`${baseUrl}${checklistId}/group/${addedGroupId}`, {}, axiosOptions)).data;
    return notAddedIds;
  } catch (e) {
    if (e.response.data === { error: 'alreadyAdded' }) {
      return [];
    }
    return null;
  }
}

async function postMemberToChecklist(
  personId: number,
  checklistId: number,
): Promise<boolean> {
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  try {
    await Axios.post(`${baseUrl}${checklistId}/${personId}`, {}, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

async function deleteMemberFromChecklist(personId: number, checklistId: number): Promise<boolean> {
  try {
    await Axios.delete(`${baseUrl}${checklistId}/${personId}`, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

async function checkMemberOnChecklist(
  personId: number,
  checklistId: number,
  checked: boolean = true,
): Promise<boolean> {
  Axios.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';
  try {
    const resp = await Axios.put(
      `${baseUrl}${checklistId}/${personId}`,
      { checked },
      axiosOptions,
    );
    return resp.data.checked;
  } catch (_) {
    return false;
  }
}

export {
  addActivityMembersToChecklist,
  addGroupMembersToChecklist,
  postMemberToChecklist,
  deleteMemberFromChecklist,
  checkMemberOnChecklist,
};
