import Axios, { AxiosResponse } from 'axios';
import Permissions from '@/lib/Permissions';
import PermissionEnum from '@/lib/PermissionEnum';
import APIUrl from '@/APIUrl';

interface RequestFormat {
  permissions: PermissionEnum[],
}

function generateRequest(permissions: Permissions): RequestFormat {
  return { permissions: permissions.allPermissions() };
}

export default async function updatePermissions(
  permissions: Permissions,
  userId: number,
): Promise<number> {
  // initialize Axios headers
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  // send request to back-end server
  const response: AxiosResponse =
    await Axios.post(
      `${APIUrl}/api/account/${userId}/change_permissions/`,
      generateRequest(permissions),
      { withCredentials: true },
    );
  return 0;
}
