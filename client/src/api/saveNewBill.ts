import Axios, { AxiosResponse } from 'axios';
import Bill from '@/lib/Bill';
import APIUrl from '@/APIUrl';

interface Response {
  valid: boolean;
  error: string | undefined;
  billId: number;
}

export default async function saveNewBill(bill: Bill): Promise<number> {
  // initialize Axios headers
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';

  const baseUrl = `${APIUrl}/api/bill`;
  const body = { bill: Bill.toServerRequest(bill), personIds: [] };
  const axiosOptions = { withCredentials: true };

  const answer: AxiosResponse = await Axios.post(`${baseUrl}`, body, axiosOptions);

  const answerData: Response = answer.data;

  if (!answerData.valid) {
    throw Error(answerData.error);
  }

  return answerData.billId;
}
