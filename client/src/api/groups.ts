import { Group } from '@/lib/UsersPack';
import Axios, { AxiosPromise } from 'axios';
import APIUrl from '@/APIUrl';

interface ServerGroup {
  id: number,
  title: string,
  description: string | null
}

// TODO: we should use pagination for listing the groups. This requires some
// hacking with Vue tho. At least we're not requesting 1 HUGE JSON, but
// multiples.
async function getGroupsOfPerson(personId: string = ''): Promise<Group[]> {
  let groups: Group[] = [];
  const baseUrl = `${APIUrl}/api/groups/${personId}?page=`;
  const axiosOptions = { withCredentials: true };
  const pageNumber = 0;

  try {
    const resp = await Axios.get(`${baseUrl}${pageNumber}`, axiosOptions);

    groups =
      groups.concat(resp.data.groups.map((g: ServerGroup) => Group.fromJson(g)));
    const leftOverPages =
      Math.ceil((resp.data.amtGroups / resp.data.groups.length) - 1) || 0;

    const serverResponsesOtherPages = await Promise
      .all(Array(leftOverPages)
        .fill(null)
        .map((_: any, i: number): AxiosPromise<any> =>
          Axios.get(`${baseUrl}${i + 1}`, axiosOptions)));

    const otherGroups: Group[] =
      serverResponsesOtherPages
        .filter(r => r.status === 200)
        .flatMap(r => r.data.groups.map((g: ServerGroup) => Group.fromJson(g)));
    groups = groups.concat(otherGroups);

    return groups;
  } catch (_) {
    return groups;
  }
}

export default getGroupsOfPerson;
