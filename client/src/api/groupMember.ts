import Axios from 'axios';
import APIUrl from '@/APIUrl';

const baseUrl = `${APIUrl}/api/group_member/`;
const axiosOptions = { withCredentials: true };

async function postMemberToGroup(
  personId: number,
  groupId: number,
): Promise<boolean> {
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  try {
    await Axios.post(`${baseUrl}${groupId}/${personId}`, {}, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

async function deleteMemberFromGroup(personId: number, groupId: number): Promise<boolean> {
  try {
    await Axios.delete(`${baseUrl}${groupId}/${personId}`, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

export { postMemberToGroup, deleteMemberFromGroup };
