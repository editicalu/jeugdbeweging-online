import Axios, { AxiosResponse } from 'axios';
import Bill from '@/lib/Bill';
import APIUrl from '@/APIUrl';

interface Response {
  valid: boolean,
  error: string,
  bills: responseBill[]
}

interface responseBill {
  id: number,
  description: string,
  amount: number,
  creationDate: string,
}

export default async function getbills(personId: number, paid: boolean): Promise<Bill[]> {
  // initialize Axios headers
  Axios.defaults.headers.get['Content-Type'] = 'application/json; charset=utf-8';

  const baseUrl = `${APIUrl}/api/bills/${personId}/${paid}`;
  const axiosOptions = { withCredentials: true };

  const answer: AxiosResponse = await Axios.get(`${baseUrl}`, axiosOptions);

  const answerData: Response = answer.data;

  if (answerData.valid) {
    return answerData.bills.map(bill =>
      new Bill(bill.id, bill.description, bill.amount, new Date(bill.creationDate)));
  }
  throw Error(answerData.error);
}
