import Axios from 'axios';
import APIUrl from '@/APIUrl';

/**
 * Get the information of a certain person
 * @param id the personID
 * @throws exception if connection went wrong
 */
export default async function deleteAccount(id: number): Promise<void> {
  try {
    await Axios.delete(
      `${APIUrl}/api/account/${id}`,
      { withCredentials: true },
    );
  } catch (_) {
    //
  }
}
