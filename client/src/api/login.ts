import Axios, { AxiosResponse } from 'axios';
import Permissions from '@/lib/Permissions';
import { Account } from '@/lib/UsersPack';
import APIUrl from '@/APIUrl';

interface SLoginAnswer {
  valid: boolean,
  error: string,
  account: {
    uid: number
    email: string,
    // eslint-disable-next-line
    parent: boolean,
    permissions: number,
  }
  personID: number | null,
}

/**
 * This function wil log you in and will initialize the sessionData
 * If the given username and password match
 * @param username the email-adress of the user account
 * @param password the password of the user account
 * @throws exception if connection to server went wrong
 */
export default async function login(
  username: string,
  password: string,
): Promise<{ account: Account | null, isLeader: boolean, personId: number | null }> {
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  // Generate message to send to server
  const message: string = JSON.stringify({ username, password });
  // Send message to server and catch response
  const response: AxiosResponse =
    await Axios.post(
      `${APIUrl}/api/login`, message,
      { withCredentials: true },
    );
  // Check if response was valid and noting went wrong
  if (response.data.valid) {
    const responseParsed: SLoginAnswer = response.data;
    // Check if username and password matched
    if (responseParsed.valid) {
      const permissions = Permissions.fromInteger(responseParsed.account.permissions);
      const account: Account =
        new Account(responseParsed.account.uid, permissions, responseParsed.account.email);
      const isLeader: boolean = !responseParsed.account.parent;
      const personId = isLeader ? responseParsed.personID : null;
      return { account, isLeader, personId };
    }
    // Else if they didn't match

    return { account: null, isLeader: false, personId: null };
  }
  // If something went wrong contacting the server
  throw response.status;
}
