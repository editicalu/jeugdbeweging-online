import Activity from '@/lib/Activity';
import Axios, { AxiosPromise } from 'axios';
import APIUrl from '@/APIUrl';

interface ServerActivity {
  id: number,
  title: string,
  description: string | null,
  preRegisterDate: string | null,
  startDate: string,
  stopDate: string
}

// TODO: we should use pagination for listing the activities. This requires some
// hacking with Vue tho. At least we're not requesting 1 HUGE JSON, but
// multiples.
async function getActivitiesOfPerson(personId: string = ''): Promise<Activity[]> {
  let activities: Activity[] = [];
  const baseUrl = `${APIUrl}/api/activities/${personId}?page=`;
  const axiosOptions = { withCredentials: true };
  const pageNumber = 0;

  try {
    const resp = await Axios.get(`${baseUrl}${pageNumber}`, axiosOptions);

    activities =
      activities.concat(resp.data.activities.map((g: ServerActivity) => Activity.fromJson(g)));
    const leftOverPages =
      Math.ceil((resp.data.amtActivities / resp.data.activities.length) - 1) || 0;

    const serverResponsesOtherPages = await Promise
      .all(Array(leftOverPages)
        .fill(null)
        .map((_: any, i: number): AxiosPromise<any> =>
          Axios.get(`${baseUrl}${i + 1}`, axiosOptions)));

    const otherActivities: Activity[] =
      serverResponsesOtherPages
        .filter(r => r.status === 200)
        .flatMap(r => r.data.activities.map((g: ServerActivity) => Activity.fromJson(g)));
    activities = activities.concat(otherActivities);

    return activities;
  } catch (_) {
    return activities;
  }
}

export default getActivitiesOfPerson;
