import Axios, { AxiosResponse } from 'axios';
import APIUrl from '@/APIUrl';

/**
 * Logout from server
 * @throws exception when connection to server went wrong
 */
export default async function logOut(): Promise<Boolean> {
  const answer: AxiosResponse =
    await Axios.post(
      `${APIUrl}/api/logout`,
      {},
      { withCredentials: true },
    );
  return answer.data.valid;
}
