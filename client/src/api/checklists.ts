import Checklist from '@/lib/Checklist';
import Axios, { AxiosPromise } from 'axios';
import APIUrl from '@/APIUrl';

interface ServerChecklist {
  id: number,
  title: string,
  description: string | null
}

// TODO: we should use pagination for listing the checklists. This requires some
// hacking with Vue tho. At least we're not requesting 1 HUGE JSON, but
// multiples.
async function getChecklistsOfPerson(personId: string = ''): Promise<Checklist[]> {
  let checklists: Checklist[] = [];
  const baseUrl = `${APIUrl}/api/checklists/${personId}?page=`;
  const axiosOptions = { withCredentials: true };
  const pageNumber = 0;

  try {
    const resp = await Axios.get(`${baseUrl}${pageNumber}`, axiosOptions);

    checklists =
      checklists.concat(resp.data.checklists.map((g: ServerChecklist) => Checklist.fromJson(g)));
    const leftOverPages =
      Math.ceil((resp.data.amtChecklists / resp.data.checklists.length) - 1) || 0;

    const serverResponsesOtherPages = await Promise
      .all(Array(leftOverPages)
        .fill(null)
        .map((_: any, i: number): AxiosPromise<any> =>
          Axios.get(`${baseUrl}${i + 1}`, axiosOptions)));

    const otherChecklists: Checklist[] =
      serverResponsesOtherPages
        .filter(r => r.status === 200)
        .flatMap(r => r.data.groups.map((g: ServerChecklist) => Checklist.fromJson(g)));
    checklists = checklists.concat(otherChecklists);

    return checklists;
  } catch (_) {
    return checklists;
  }
}

export default getChecklistsOfPerson;
