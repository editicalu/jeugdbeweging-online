import Axios, { AxiosResponse } from 'axios';
import Child from '@/lib/Child';
import APIUrl from '@/APIUrl';

interface Response {
  valid: boolean,
  error: string,
  children: Child[]
}

export default async function getChildren(parentId: number): Promise<Child[]> {
  // initialize Axios headers
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  const baseUrl = `${APIUrl}/api/parent/get_children/${parentId}`;
  const axiosOptions = { withCredentials: true };

  const answer: AxiosResponse = await Axios.get(`${baseUrl}`, axiosOptions);

  const aswerData: Response = answer.data;

  if (aswerData.valid) {
    return aswerData.children;
  }
  throw Error(aswerData.error);
}
