import { PersonMin } from '@/lib/UsersPack';
import Axios from 'axios';
import APIUrl from '@/APIUrl';

const baseUrl = `${APIUrl}/api/allpersons`;

const axiosOptions = { withCredentials: true };

interface ServerPersonMin {
  id: number,
  firstName: string,
  lastName: string
}

async function getAllPersons()
: Promise<{ totalAmount: number, persons: PersonMin[] } | null> {
  try {
    let persons: PersonMin[] = [];
    const url = `${baseUrl}`;

    const resp = await Axios.get(`${url}`, axiosOptions);
    const { totalAmount } = resp.data;

    persons =
      persons.concat(resp.data.persons.map((m: ServerPersonMin) => PersonMin.fromJson(m)));

    return { totalAmount, persons };
  } catch (_) {
    return null;
  }
}

export default getAllPersons;
