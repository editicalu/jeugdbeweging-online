import Axios, { AxiosResponse } from 'axios';
import { Report } from '@/lib/Report';
import APIUrl from '@/APIUrl';

interface SReport {
  valid: boolean,
  error: string,
  report: {
    id: number,
    reportName: string,
    writtenById: number,
    writtenByName: string,
    date: string,
    content: string,
  },
}

export default async function getReport(id: number): Promise<Report> {
  Axios.defaults.headers.get['Content-Type'] = 'application/json; charset=utf-8';

  const answer: AxiosResponse =
    await Axios.get(
      `${APIUrl}/api/report/${id}`,
      { withCredentials: true },
    );

  const answerData: SReport = answer.data;
  if (!answerData.valid) {
    throw Error(answerData.error);
  }

  const ansRep = answerData.report;

  return new Report(
    ansRep.id,
    ansRep.reportName,
    ansRep.writtenById,
    ansRep.writtenByName,
    new Date(ansRep.date),
    ansRep.content,
  );
}
