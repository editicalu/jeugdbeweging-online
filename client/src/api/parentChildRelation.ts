import Axios from 'axios';
import APIUrl from '@/APIUrl';

export default async function postParentChildRelation(parentId: number, childId: number)
  : Promise<boolean> {
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  const url = `${APIUrl}/api/parent_child/`;
  const message =
    JSON.stringify({ parentId, childId });
  const axiosOptions = { withCredentials: true };

  try {
    await Axios.post(url, message, axiosOptions);
    return true;
  } catch (_) {
    return false;
  }
}

// export { postParentChildRelation };
