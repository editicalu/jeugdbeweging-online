import Axios from 'axios';
import APIUrl from '@/APIUrl';

interface ResponseFormat {
  valid: boolean,
  error: string,
}

export default async function addChildToParentByRegistrationCode(regCode: string):
  Promise<boolean> {
  // initialize header
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';

  try {
    // Send request
    const answer =
      await Axios.post(
        `${APIUrl}/api/parent/add_child/`,
        { regCode },
        { withCredentials: true },
      );

    const answerData: ResponseFormat = answer.data;

    // Check if response-data is valid
    if (!answerData.valid) {
      return false;
    }

    return true;
  } catch (e) {
    return false;
  }
}
