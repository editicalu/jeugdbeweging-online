import { AccountMin } from '@/lib/UsersPack';
import Axios, { AxiosResponse } from 'axios';
import APIUrl from '@/APIUrl';

const baseUrl = `${APIUrl}/api/parent/?page=`;
const axiosOptions = { withCredentials: true };

interface ServerAccountMin {
  amountOfParents: number,
  parents: AccountMin[],
}

const REQUEST_SIZE = 20;

async function getAllParents(): Promise<{ totalAmount: number, parents: AccountMin[] } | null> {
  try {
    let parents: AccountMin[] = [];
    const url = `${baseUrl}`;

    const resp = await Axios.get(`${url}0`, axiosOptions);
    const { data } = resp;

    parents = parents.concat(data.parents);

    const { amountOfParents } = data;
    const amountOfRequests: number = Math.ceil(amountOfParents / REQUEST_SIZE);

    const promises: Array<Promise<AxiosResponse>> = [];
    for (let x = 1; x < amountOfRequests; x += 1) {
      promises.concat(Axios.get(`${url}${x}`, axiosOptions));
    }

    const otherData = await Promise.all(promises);

    otherData.forEach((response) => {
      const respData: ServerAccountMin = response.data;
      parents = parents.concat(respData.parents);
    });

    return { totalAmount: amountOfParents, parents };
  } catch (_) {
    return null;
  }
}

export default getAllParents;
