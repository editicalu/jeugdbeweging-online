import Axios, { AxiosResponse } from 'axios';
import Bill from '@/lib/Bill';
import APIUrl from '@/APIUrl';

interface Response {
  valid: boolean;
  error: string | undefined;
}

export default async function saveBill(bill: Bill): Promise<void> {
  // initialize Axios headers
  Axios.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';

  const baseUrl = `${APIUrl}/api/bill/${bill.id}`;
  const body = { bill: Bill.toServerRequest(bill) };
  const axiosOptions = { withCredentials: true };

  const answer: AxiosResponse = await Axios.put(`${baseUrl}`, body, axiosOptions);

  const answerData: Response = answer.data;

  if (!answerData.valid) {
    throw Error(answerData.error);
  }
}
