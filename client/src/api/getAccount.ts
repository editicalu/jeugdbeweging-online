import Axios, { AxiosResponse } from 'axios';
import { Account } from '@/lib/UsersPack';
import Permissions from '@/lib/Permissions';
import APIUrl from '@/APIUrl';

interface ServerResponse {
  error: string,

  email: string,
  parent: boolean,
  permissions: number,
  uid: number,
}

/**
 * Get the information of a certain person
 * @param id the personID
 * @throws exception if connection went wrong
 */
export default async function getAccount(id: number): Promise<Account> {
  Axios.defaults.headers.get['Content-Type'] = 'application/json; charset=utf-8';
  try {
    const answer: AxiosResponse =
      await Axios.get(
        `${APIUrl}/api/account/${id}`,
        { withCredentials: true },
      );

    const answerData: ServerResponse = answer.data;

    if (answerData.error !== undefined) {
      // Something went wrong
      return Promise.reject();
    }

    const account: Account = new Account(
      answerData.uid,
      Permissions.fromInteger(answerData.permissions),
      answerData.email,
      '',
      answerData.parent ? 'parent' : 'leader',
    );
    return account;
  } catch (_) {
    return Promise.reject();
  }
}
