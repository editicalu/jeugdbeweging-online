import Bill from '@/lib/Bill';
import Axios, { AxiosResponse } from 'axios';
import APIUrl from '@/APIUrl';

interface serverResponse {
  valid: boolean,
  error: string,
  bill: {
    id: number,
    description: string,
    amount: number,
    creationDate: string,
  },
}

/**
 * Get a bill by it's id
 * @param billId
 */
export default async function getBill(billId: number): Promise<Bill> {
  Axios.defaults.headers.get['Content-Type'] = 'application/json; charset=utf-8';

  const answer: AxiosResponse = await Axios.get(
    `${APIUrl}/api/bills/${billId}`,
    { withCredentials: true },
  );

  const answerData: serverResponse = answer.data;

  return new Bill(
    answerData.bill.id,
    answerData.bill.description,
    answerData.bill.amount,
    new Date(answerData.bill.creationDate),
  );
}
