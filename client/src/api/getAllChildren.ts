import { PersonMin } from '@/lib/UsersPack';
import Axios from 'axios';
import APIUrl from '@/APIUrl';

const baseUrl = `${APIUrl}/api/allchildren/`;
const axiosOptions = { withCredentials: true };

interface ServerPersonMin {
  id: number,
  firstName: string,
  lastName: string
}

// TODO: pagination
async function getAllChildren()
: Promise<{ totalAmount: number, members: PersonMin[] } | null> {
  try {
    let members: PersonMin[] = [];
    const url = `${baseUrl}`;

    const resp = await Axios.get(url, axiosOptions);
    const amountOfChildren: number = resp.data.totalAmount;

    members =
      members.concat(resp.data.persons.map((m: ServerPersonMin) => PersonMin.fromJson(m)));

    return { totalAmount: amountOfChildren, members };
  } catch (_) {
    return null;
  }
}

export default getAllChildren;
