import Axios, { AxiosResponse } from 'axios';
import APIUrl from '@/APIUrl';

interface serverResponse {
  valid: boolean,
  error: string,
}

export default async function editBilled(billId: number, personId: number, paid: boolean):
  Promise<void> {
  // initialize Axios headers
  Axios.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';

  try {
    const answer: AxiosResponse =
      await Axios.put(
        `${APIUrl}/api/billed/${billId}/${personId}`,
        { paid },
        { withCredentials: true },
      );

    const answerData: serverResponse = answer.data;

    if (answerData.valid) {
      return;
    }
    throw Error(answerData.error);
  } catch (e) {
    throw e;
  }
}
