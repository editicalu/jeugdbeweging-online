import Axios, { AxiosResponse } from 'axios';
import Person from '@/lib/Person';
import ResponsePerson from '@/api/responseInterfaces/ResponsePerson';
import APIUrl from '@/APIUrl';
import { Gender } from '@/lib/UsersPack';
import Address from '@/lib/Address';

interface ServerResponse {
  valid: boolean,
  error: string | undefined,
  person: ResponsePerson
}

/**
 * Get the information of a certain person
 * @param id the personID
 * @throws exception if connection went wrong
 */
export default async function getPerson(id: number): Promise<Person | null> {
  Axios.defaults.headers.get['Content-Type'] = 'application/json; charset=utf-8';
  try {
    const answer: AxiosResponse =
      await Axios.get(
        `${APIUrl}/api/person/${id}`,
        { withCredentials: true },
      );

    const answerData: ServerResponse = answer.data;

    const person: Person = Person.fromServerResponse(answerData.person);

    return person;
  } catch (_) {
    return null;
  }
}
