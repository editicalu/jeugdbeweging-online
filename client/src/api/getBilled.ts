import Axios, { AxiosResponse } from 'axios';
import Person from '@/lib/Person';
import APIUrl from '@/APIUrl';
import ResponsePerson from './responseInterfaces/ResponsePerson';

interface Response {
  valid: boolean,
  error: string,
  paid: ResponsePerson[],
  unPaid: ResponsePerson[],
}

export default async function getbilled(billId: number):
  Promise<{ paid: Person[], unpaid: Person[] }> {
  // initialize Axios headers
  Axios.defaults.headers.get['Content-Type'] = 'application/json; charset=utf-8';

  const baseUrl = `${APIUrl}/api/billed/${billId}`;
  const axiosOptions = { withCredentials: true };

  const answer: AxiosResponse = await Axios.get(`${baseUrl}`, axiosOptions);

  const answerData: Response = answer.data;

  const paid: Person[] =
    answerData.paid.map(paidPerson => Person.fromServerResponse(paidPerson));
  const unpaid: Person[] =
    answerData.unPaid.map(unPaidPerson => Person.fromServerResponse(unPaidPerson));

  return { paid, unpaid };
}
