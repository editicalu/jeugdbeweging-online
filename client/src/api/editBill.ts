import Axios, { AxiosResponse } from 'axios';
import Bill from '@/lib/Bill';
import APIUrl from '@/APIUrl';

interface serverResponse {
  valid: boolean,
  error: string,
}

/**
 * Edit a bill
 * @param bill the bill and it's new data
 * @throws on error
 */
export default async function editBill(bill: Bill):
  Promise<void> {
  // initialize Axios headers
  Axios.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';

  const answer: AxiosResponse =
    await Axios.put(
      `${APIUrl}/api/bill/${bill.id}`,
      { bill: Bill.toServerRequest(bill) },
      { withCredentials: true },
    );

  const answerData: serverResponse = answer.data;

  if (answerData.valid) {
    return;
  }
  throw Error(answerData.error);
}
