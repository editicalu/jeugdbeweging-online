import Axios, { AxiosResponse } from 'axios';
import APIUrl from '@/APIUrl';

interface SReport {
  valid: boolean,
  error: string,
  reportID: number,
}

export default async function saveReport(name: string, content: string, id: number = 0):
  Promise<number> {
  Axios.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';

  let extraUrl: string = '';
  if (id !== 0) {
    extraUrl = `${id}`;
  }

  const answer: AxiosResponse =
    await Axios.put(
      `${APIUrl}/api/report/${extraUrl}`,
      { reportName: name, content },
      { withCredentials: true },
    );

  const answerData: SReport = answer.data;

  // Check if something went wrong
  if (answerData.valid === false) {
    throw Error(answerData.error);
  }

  return answerData.reportID;
}
