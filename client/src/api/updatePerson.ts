import Axios, { AxiosResponse } from 'axios';
import Person from '@/lib/Person';
import APIUrl from '@/APIUrl';

interface Answer {
  valid: boolean,
  error: string,
}

export default async function updatePerson(person: Person): Promise<boolean> {
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  let url;
  if (person.type === 'child') {
    url = `${APIUrl}/api/child/${person.id}`;
  } else {
    url = `${APIUrl}/api/leader/${person.id}`;
  }
  const response: AxiosResponse =
    await Axios.put(
      url,
      { ...Person.toServerRequest(person) },
      { withCredentials: true },
    );
  // Check if response was valid and nothing went wrong
  const responseParsed: Answer = response.data;
  return responseParsed.valid;
}
