import Axios, { AxiosResponse } from 'axios';
import APIUrl from '@/APIUrl';

interface serverResponse {
  valid: boolean,
  error: string,
}

/**
 * Get a bill by it's id
 * @param billId
 */
export default async function addBilled(billId: number, personIds: number[]): Promise<void> {
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';

  const answer: AxiosResponse = await Axios.post(
    `${APIUrl}/api/billed/${billId}`,
    { personIds },
    { withCredentials: true },
  );

  const answerData: serverResponse = answer.data;

  if (!answerData.valid) {
    throw new Error(answerData.error);
  }
}
