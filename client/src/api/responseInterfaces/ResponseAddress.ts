interface ResponseAddress {
  streetAddress: string|undefined;
  zipcode: string|undefined;
  city: string|undefined;
}

export default ResponseAddress;

