interface ResponseAccount {
  id: number|undefined;
  permissions: number|undefined;
  email: string|undefined;
  password: string|undefined;
}

export default ResponseAccount;
