import ResponseAddress from './ResponseAddress';

interface ResponsePerson {
  id: number | undefined;
  firstName: string | undefined;
  lastName: string | undefined;
  birthplace: string | undefined;
  birthdate: string | undefined;
  gender: string | undefined | null;
  phoneNumber: string | undefined;
  address: ResponseAddress | undefined;
  comment: string | undefined;
  registrationPending: boolean | undefined;
  type: string | undefined;
}

export default ResponsePerson;
