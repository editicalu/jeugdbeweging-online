import Axios, { AxiosResponse } from 'axios';
import APIUrl from '@/APIUrl';

interface RequestFormat {
  originalPassword: string,
  password: string,
}

function generateRequest(originalPassword: string, password: string): RequestFormat {
  return { originalPassword, password };
}

export default async function updatePassword(
  originalPassword: string,
  password: string,
): Promise<number> {
  // initialize Axios headers
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
  // send request to back-end server
  const response: AxiosResponse =
    await Axios.post(
      `${APIUrl}/api/account/change_password`,
      generateRequest(originalPassword, password),
      { withCredentials: true },
    );
  return 0;
}
