import Axios, { AxiosResponse } from 'axios';
import Permissions from '@/lib/Permissions';
import { Account } from '@/lib/UsersPack';
import APIUrl from '@/APIUrl';

interface SRegisterAnswer {
  valid: boolean,
  account: {
    uid: number
    email: string,
    // eslint-disable-next-line
    parent: boolean,
    permissions: number
  }
}

/**
 * This function wil log you in and will initialize the sessionData
 * If the given username and password match
 * @param username the email-adress of the user account
 * @param password the password of the user account
 * @throws exception if connection to server went wrong
 */
export default async function registerParent(username: string, password: string):
  Promise<{ account: Account, isLeader: boolean }> {
  Axios.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';
  // Generate message to send to server
  const message: string = JSON.stringify({ username, password });
  // Send message to server and response
  const response: AxiosResponse =
    await Axios.put(
      `${APIUrl}/api/parent/`, message,
      { withCredentials: true },
    );
  // Check if response was valid and noting went wrong
  const responseParsed: SRegisterAnswer = response.data;
  // Check if username and password match
  const permissions = Permissions.fromInteger(responseParsed.account.permissions);
  const account: Account =
    new Account(responseParsed.account.uid, permissions, responseParsed.account.email);
  const isLeader: boolean = !responseParsed.account.parent;
  return { account, isLeader };
}
