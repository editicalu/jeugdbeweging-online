import Axios from 'axios';
import APIUrl from '@/APIUrl';

interface ResponseFormat {
  valid: boolean,
  registrationCode: string,
}

export default async function generateRegistrationCode(childId: number): Promise<string | null> {
  // initialize header
  Axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';

  try {
    // Send request
    const answer =
      await Axios.get(
        `${APIUrl}/api/child/${childId}/code/`,
        { withCredentials: true },
      );

    const answerData: ResponseFormat = answer.data;

    return answerData.registrationCode;
  } catch (e) {
    return null;
  }
}
