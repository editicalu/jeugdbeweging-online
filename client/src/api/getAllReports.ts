import Axios, { AxiosResponse } from 'axios';
import { ReportMin } from '@/lib/Report';
import APIUrl from '@/APIUrl';

interface SReportsMin {
  valid: boolean,
  error: string,
  reports: {
    id: number,
    reportName: string,
    writtenById: number,
    writtenByName: string,
    date: string,
  }[],
}

export default async function getAllReports(): Promise<ReportMin[]> {
  Axios.defaults.headers.get['Content-Type'] = 'application/json; charset=utf-8';

  const answer: AxiosResponse =
    await Axios.get(
      `${APIUrl}/api/report`,
      { withCredentials: true },
    );
  const answerData: SReportsMin = answer.data;
  if (!answerData.valid) {
    throw Error(answerData.error);
  }

  const ret: ReportMin[] =
    answerData.reports.map(r =>
      new ReportMin(r.id, r.reportName, r.writtenById, r.writtenByName, new Date(r.date)));


  return ret;
}
