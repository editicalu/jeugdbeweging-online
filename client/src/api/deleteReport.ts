import Axios, { AxiosResponse } from 'axios';
import APIUrl from '@/APIUrl';

interface SResponse {
  valid: boolean,
  error: string,
}

export default async function getReport(id: number): Promise<void> {
  Axios.defaults.headers.delete['Content-Type'] = 'application/json; charset=utf-8';

  const answer: AxiosResponse =
    await Axios.delete(
      `${APIUrl}/api/report/${id}`,
      { withCredentials: true },
    );

  const answerData: SResponse = answer.data;
  if (!answerData.valid) {
    throw Error(answerData.error);
  }
}
