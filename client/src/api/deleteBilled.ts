import Axios from 'axios';
import APIUrl from '@/APIUrl';


export default async function deleteBilled(billId: number, personId: number): Promise<void> {
  // initialize Axios headers
  Axios.defaults.headers.delete['Content-Type'] = 'application/json; charset=utf-8';
  await Axios.delete(
    `${APIUrl}/api/billed/${billId}/${personId}`,
    { withCredentials: true },
  );
}
