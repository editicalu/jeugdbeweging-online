module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'linebreak-style': 0,
    // Extending primitive type properties is often not a good idea.
    // We know what we're doing though, thus we only want a warning.
    'no-extend-native': 'warn',
    // We set this to warn as Vue object methods are often times event
    // handlers used in our templates. These do not use `this`, which makes
    // this error invalid in .vue files.
    'class-methods-use-this': 'warn',
    // We want bitwise operations for permissions.
    'no-bitwise': 'off',
    // This is usually what you want.
    // However, the global state store should not be destructured as it gives
    // errors.
    'prefer-destructuring': 'warn',
  },
  parserOptions: {
    parser: 'typescript-eslint-parser',
  },
};
